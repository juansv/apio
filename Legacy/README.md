Estimado/a usuario/a:

He subido mi proyecto Apio a gitlab en su forma de paquete debian. En el repositorio tienes tanto el paquete construido (apio-1.1_all.deb) como "deconstruido", es decir, el conjunto de archivos y carpetas necesarios para hacer el paquete debian.

Estos archivos y carpetas te permitirán adaptar Apio a cualquier sistema gnu-linux. Además, te permitirán estudiar el código, ya que Apio está escrito en Python.

Pues ahí lo tienes. Ya uses directamente el .deb o adaptes las fuentes de Apio en tu sistema, espero que mi programa te sea de utilidad. Ya me contarás...

Por último, recuerda que Apio es un proyecto de software y hardware libre. La parte de software es completamente autónoma, no es necesario el hardware para que Apio funcione. Pero si implementas la parte hardware, Apio se convierte en una herramienta mucho más cómoda para el usuario. En /usr/share/doc/apio/hardware_apio.pdf tienes información detallada para construir la parte de hardware.
