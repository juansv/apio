##############################################
###             APIO (las recetas)         ###
##############################################


# Xapio (puré de apio con Xcfe: rápido, ligero y visualmente atractivo)

- Una cebolla.
- Un apio (entero, hojas incluidas si las tuvieses).
- Agua, sal y un poco de aceite de oliva.

Pochar una cebolla en aceite de oliva. Cuando esté pochadina, añadir un
ramillete de apio (hojas incluidas si las tuviese) troceado y rehogarlo
un poco. Cubrir de agua, salar y dejar cocer hasta que el apio esté
tierno (unos 30 minutos). Triturar con la batidora.

Este puré admite ser congelado, se conserva bien y no cambia el sabor.
Admite bien otros condimentos como el curry, cilantro o la pimienta.
También admite una zanahoria.


# Apio_v2.2.deb (Debian Potato)

Lo mismo de antes, pero añade una patata grande o un par de patatas
pequeñas a la cocción.


# Salmorejo con apio

- Cuatro tomates pequeños maduros.
- Un tallo de apio.
- Media cebolla.
- Una rebanada (gorda) de pan integral.
- Medio diente de ajo (o un diente pequeño).
- Aceite de oliva.

Pelar el tomate. Echarlo todos los ingredientes a un bol (los tomates
pelados) y batirlo  todo junto. Dejar reposar para que se mezclen los
sabores.


