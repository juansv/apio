Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: apio
Upstream-Contact: Juan Antonio Silva Villar <juaninsv@gmail.com>

Files: *
Copyright: 2018 Juan Antonio Silva Villar <juaninsv@gmail.com>
License: GPL-3+

Files: /usr/share/pixmaps/*
Copyright: https://pixabay.com/
License: Creative Commons CC0

Files: /usr/share/doc/apio/hardware_apio.pdf
Copyright: Juan Antonio Silva Villar <juaninsv@gmail.com>
License: Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional

License: GPL-3+
 Este programa está licenciado como software libre. Puedes redistribuirlo
 y/o modificarlo según los términos de la GNU General Public License
 versión 3. En Debian GNU/Linux el texto completo de la licencia GNU
 General Public License se puede encontrar en:
 /usr/share/common-licenses
 
 Respecto al código fuente: Apio está programado en Python, que es un
 lenguaje interpretado. El propio código son las fuentes. Puedes ver las
 fuentes en:
 /usr/share/apio

License: Creative Commons CC0
 El icono que utilizo en el programa Apio lo obtuve en la web:
 https://pixabay.com/
 
 El dibujo original se llama “celery-311953”. El dibujo está liberado para
 el dominio público con una licencia Creative Commons CC0. El contenido de
 la licencia puede leerse en:
 https://creativecommons.org/publicdomain/zero/1.0/legalcode
 
 Las condiciones de uso del contenido de Pixabay está disponible en:
 https://pixabay.com/es/service/terms/#usage 
 
 La web Pixabay no menciona el autor/a del dibujo, motivo por el que no se
 menciona en este documento. No obstante, le expreso mi gratitud por ceder
 su arte para el uso público.

License: Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional
 El circuito electrónico compatible con el programa Apio y que permite que
 Apio opere de manera automática, está documentado en:
 /usr/share/doc/apio/hardware_apio.pdf
 
 Cualquier persona con suficientes conocimientos en electrónica puede
 fabricar el circuito y hacerlo funcionar con la simple ayuda de este
 documento.
 
 Este documento puede distribuirse y modificarse bajo la licencia Creative
 Commons "Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional". El
 contenido de esta licencia puede leerse en:
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 
 La idea que sustenta el circuito es tan simple que no considero que pueda
 ser registrada. Sería como registrar el uso de una tijera de pescado para
 cortar papel o el uso de un candado para sustituir el cierre de una
 taquilla.
 
 El circuito que he diseñado y que soporta la idea de funcionamiento no es
 más que la conjunción de circuitos y técnicas estándar usadas en
 electrónica. No puedo licenciarlo de ninguna forma, sería como registrar
 la resolución de un problema de matemáticas o una pipe de comandos del
 sistema operativo.
