#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo tiene la función con la ventana de ayuda

import tkinter


################################################################
################ FUNCIÓN ayuda() ###############################
################################################################
## Genera una ventana hija (Toplevel -> wait_window) donde cargo
## un widget de texto con una barra de scroll. A su vez, dentro
## del widget de texto cargo el contenido de un archivo de texto
## que contiene la información de la ayuda. Así, cada modificación
## del texto de la ayuda, sólo tengo que hacerlo en ese archivo de
## texto, que puedo editar libremente.

def ayuda():
    # VENTANA DE LA AYUDA
    ventana_ayuda = tkinter.Toplevel()

    # icono de la ventana
    icono_ayuda = tkinter.PhotoImage(file='/usr/share/pixmaps/ayuda.png')
    ventana_ayuda.tk.call('wm', 'iconphoto', ventana_ayuda._w, icono_ayuda)

    # tamaño de partida de la ventana de ayuda
    # ventana_ayuda.geometry("550x300")

    # Hacer la ventana de tamaño fijo. Añadiendo esta línea de código, el tamaño
    # de la ventana será fijo, el usuario no la podrá cambiar pinchando y
    # arrastrando en las esquinas de la ventana
    ventana_ayuda.resizable(width='FALSE', height='FALSE')

    # título en el marco de la ventana
    ventana_ayuda.title('Ayuda de Apio')

    # barra de scroll. Se aplica a la ventana de ayuda y ocupa todo el
    # borde derecho
    barrascroll = tkinter.Scrollbar(ventana_ayuda)
    barrascroll.pack(side='right', fill='y')

    # TEXTO DE LA AYUDA

    # abro el archivo de texto con el texto de la ayuda
    archivo_ayuda = open("/usr/share/doc/apio/ayuda_ayuda.txt", "r")

    # leo el archivo
    contenido_ayuda = archivo_ayuda.read()
    # cierro el archivo
    archivo_ayuda.close()

    # inserto el texto de la ayuda en la ventana usando un widget 'texto'
    # al que llamo 'texto_ayuda'. Este widget tiene que llevar la referencia
    # a la ventana a la que pertenece (obvio) y la opción de la barra de
    # scroll.
    # Luego hay que indicarle el texto que se inserta en el widget y posicionar
    # el widget (pack).
    # Una vez posicionado el texto tengo que indicar la configuración de la
    # barra de scroll, que es mover el texto de ayuda por el eje de las y.
    texto_ayuda = tkinter.Text(ventana_ayuda, yscrollcommand=barrascroll.set)
    texto_ayuda.insert('insert', contenido_ayuda)
    texto_ayuda.pack()
    barrascroll.config(command=texto_ayuda.yview)
    ventana_ayuda.wait_window()
    return


################################################################
################ FUNCIÓN fundamento() ##########################
################################################################
## Genera una ventana hija (Toplevel -> wait_window) donde cargo
## un widget de texto con una barra de scroll. A su vez, dentro
## del widget de texto cargo el contenido de un archivo de texto
## que contiene la información que explica el fundamento del programa.
## Así, cada modificación del texto de la ayuda, sólo tengo que
## hacerlo en ese archivo de texto, que puedo editar libremente.

def fundamento():
    # VENTANA QUE EXPLICA EL FUNDAMENTO DE APIO
    ventana_fundamento = tkinter.Toplevel()

    # icono de la ventana
    icono_ayuda = tkinter.PhotoImage(file='/usr/share/pixmaps/ayuda.png')
    ventana_fundamento.tk.call('wm', 'iconphoto', ventana_fundamento._w,
        icono_ayuda)

    # tamaño de partida de la ventana de fundamento
    # ventana_fundamento.geometry("550x300")

    # Hacer la ventana de tamaño fijo. Añadiendo esta línea de código, el tamaño
    # de la ventana será fijo, el usuario no la podrá cambiar pinchando y
    # arrastrando en las esquinas de la ventana
    ventana_fundamento.resizable(width='FALSE', height='FALSE')

    # título en el marco de la ventana
    ventana_fundamento.title('Fundamento de Apio')

    # barra de scroll. Se aplica a la ventana de fundamento y ocupa todo el
    # borde derecho
    barrascroll = tkinter.Scrollbar(ventana_fundamento)
    barrascroll.pack(side='right', fill='y')

    # TEXTO DEL FUNDAMENTO

    # abro el archivo de texto con el texto del fundamento
    archivo_fundamento = open("/usr/share/doc/apio/ayuda_fundamento.txt",
        "r")
    # leo el archivo
    contenido_fundamento = archivo_fundamento.read()
    # cierro el archivo
    archivo_fundamento.close()

    # inserto el texto de la fundamento en la ventana usando un widget 'texto'
    # al que llamo 'texto_fundamento'. Este widget tiene que llevar la
    # referencia a la ventana a la que pertenece (obvio) y la opción de la
    # barra de scroll.
    # Luego hay que indicarle el texto que se inserta en el widget y posicionar
    # el widget (pack).
    # Una vez posicionado el texto tengo que indicar la configuración de la
    # barra de scroll, que es mover el texto de fundamento por el eje de las y.
    texto_fundamento = tkinter.Text(ventana_fundamento,
        yscrollcommand=barrascroll.set)
    texto_fundamento.insert('insert', contenido_fundamento)
    texto_fundamento.pack()
    barrascroll.config(command=texto_fundamento.yview)
    ventana_fundamento.wait_window()
    return


################################################################
################ FUNCIÓN licencia() ############################
################################################################
## Genera una ventana hija (Toplevel -> wait_window) donde cargo
## un widget de texto con una barra de scroll. A su vez, dentro
## del widget de texto cargo el contenido de un archivo de texto
## que contiene la información de la licencia del programa.
## Así, cada modificación del texto de la ayuda, sólo tengo que
## hacerlo en ese archivo de texto, que puedo editar libremente.

def recetas():
    # VENTANA CON LAS RECETAS DE APIO
    ventana_recetas = tkinter.Toplevel()

    # icono de la ventana
    icono_ayuda = tkinter.PhotoImage(file='/usr/share/pixmaps/ayuda.png')
    ventana_recetas.tk.call('wm', 'iconphoto', ventana_recetas._w, icono_ayuda)

    # tamaño de partida de la ventana de recetas
    # ventana_recetas.geometry("550x300")

    # Hacer la ventana de tamaño fijo. Añadiendo esta línea de código, el tamaño
    # de la ventana será fijo, el usuario no la podrá cambiar pinchando y
    # arrastrando en las esquinas de la ventana
    ventana_recetas.resizable(width='FALSE', height='FALSE')

    # título en el marco de la ventana
    ventana_recetas.title('Recetas de Apio')

    # barra de scroll. Se aplica a la ventana de recetas y ocupa todo el
    # borde derecho
    barrascroll = tkinter.Scrollbar(ventana_recetas)
    barrascroll.pack(side='right', fill='y')

    # TEXTO DE LAS RECETAS

    # abro el archivo de texto con el texto de las recetas
    archivo_recetas = open("/usr/share/doc/apio/ayuda_recetas.txt", "r")
    # leo el archivo
    contenido_recetas = archivo_recetas.read()
    # cierro el archivo
    archivo_recetas.close()

    # inserto el texto de las recetas en la ventana usando un widget 'texto'
    # al que llamo 'texto_recetas'. Este widget tiene que llevar la
    # referencia a la ventana a la que pertenece (obvio) y la opción de la
    # barra de scroll.
    # Luego hay que indicarle el texto que se inserta en el widget y posicionar
    # el widget (pack).
    # Una vez posicionado el texto tengo que indicar la configuración de la
    # barra de scroll, que es mover el texto de las recetas por el eje de las y
    texto_recetas = tkinter.Text(ventana_recetas,
        yscrollcommand=barrascroll.set)
    texto_recetas.insert('insert', contenido_recetas)
    texto_recetas.pack()
    barrascroll.config(command=texto_recetas.yview)
    ventana_recetas.wait_window()
    return


################################################################
################ FUNCIÓN acerca() ##############################
################################################################
## Genera una ventana hija (Toplevel -> wait_window) donde cargo
## un widget de texto con una barra de scroll. A su vez, dentro
## del widget de texto cargo el contenido de un archivo de texto
## que contiene la información del acerca del programa (autor y licencia).
## Así, cada modificación del texto de "acerca de", sólo tengo que
## hacerlo en ese archivo de texto, que puedo editar libremente.

def acerca():
    # VENTANA "ACERCA DE" DE APIO
    ventana_acerca = tkinter.Toplevel()

    # icono de la ventana
    icono_ayuda = tkinter.PhotoImage(file='/usr/share/pixmaps/ayuda.png')
    ventana_acerca.tk.call('wm', 'iconphoto', ventana_acerca._w, icono_ayuda)

    # tamaño de partida de la ventana de acerca
    # ventana_acerca.geometry("550x300")

    # Hacer la ventana de tamaño fijo. Añadiendo esta línea de código, el tamaño
    # de la ventana será fijo, el usuario no la podrá cambiar pinchando y
    # arrastrando en las esquinas de la ventana
    ventana_acerca.resizable(width='FALSE', height='FALSE')

    # título en el marco de la ventana
    ventana_acerca.title('acerca de Apio')

    # barra de scroll. Se aplica a la ventana "acerca de" y ocupa todo el
    # borde derecho
    barrascroll = tkinter.Scrollbar(ventana_acerca)
    barrascroll.pack(side='right', fill='y')

    # TEXTO DEL "ACERCA DE"

    # abro el archivo de texto con el texto del "acerca de"
    archivo_acerca = open("/usr/share/doc/apio/ayuda_acerca.txt", "r")
    # leo el archivo
    contenido_acerca = archivo_acerca.read()
    # cierro el archivo
    archivo_acerca.close()

    # inserto el texto de "acerca de" en la ventana usando un widget 'texto'
    # al que llamo 'texto_acerca'. Este widget tiene que llevar la
    # referencia a la ventana a la que pertenece (obvio) y la opción de la
    # barra de scroll.
    # Luego hay que indicarle el texto que se inserta en el widget y posicionar
    # el widget (pack).
    # Una vez posicionado el texto tengo que indicar la configuración de la
    # barra de scroll, que es mover el texto de acerca por el eje de las y.
    texto_acerca = tkinter.Text(ventana_acerca, yscrollcommand=barrascroll.set)
    texto_acerca.insert('insert', contenido_acerca)
    texto_acerca.pack()
    barrascroll.config(command=texto_acerca.yview)
    ventana_acerca.wait_window()
    return