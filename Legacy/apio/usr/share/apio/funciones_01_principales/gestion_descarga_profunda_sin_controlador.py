#! /usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import messagebox
import funciones_02_auxiliares.bateria_acpi as bateria


###############################################################
##### FUNCIÓN gestion_descarga_profunda_sin_controlador() #####
###############################################################
# Si se invoca esta función es porque se está en pleno ciclo de
# descarga profunda de la batería y se detectó que el adaptador
# USB-RS232 no está conectado.
# Lo primero es verificar si el ciclo de descarga llegó a su fin
# y setear como corresponda la variable de cambio de ciclo
# Lo segundo es verificar el estado del cargador y avisar al
# usuario para que lo enchufe o desenchufe

def gestion_descarga_profunda_sin_controlador(carga_min_min):
    carga_min_min = int(carga_min_min)

# Decisión sobre si toca cambiar el ciclo o no:
    if bateria.carga() > carga_min_min:
        cambio_ciclo = 0
    else:
        cambio_ciclo = 1

# Decisión sobre avisar al usuario. Sólo hay que considerar dos casos:
# 1) Si no acabó el ciclo (el ciclo de descarga profunda continúa), el cargador
# debe estar desenchufado. Lo verifico aviso al usuario si procede
# 2) Si acabó el ciclo (el ciclo de descarga profunda terminó), el cargador
# debe estar enchufado. Lo verifico y aviso al usuario si procede

# Si no cambia el ciclo => el cargador debe seguir desenchufado. Si está
# enchufado, hay que avisar al usuario
    if cambio_ciclo == 0 and bateria.enchufada() == 1:
        messagebox.showwarning("Apio informa",
        "Por favor, desenchufa el cargador")

# Si cambia el ciclo => el cargador debe enchufarse, hay que avisar al usuario
    elif cambio_ciclo == 1 and bateria.enchufada() == 0:
        messagebox.showwarning("Apio informa",
        "Por favor, enchufa el cargador")

# No hay más casos que valorar
    else:
        pass

# Fin de función
    return cambio_ciclo