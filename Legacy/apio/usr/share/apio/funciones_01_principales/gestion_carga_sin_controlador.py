#! /usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import messagebox
import funciones_02_auxiliares.bateria_acpi as bateria


###############################################################
############ FUNCIÓN gestion_carga_sin_controlador() ##########
###############################################################
# Si se invoca esta función es porque se está en pleno ciclo de
# carga de la batería y se detectó que el adaptador USB-RS232
# no está conectado.
# Lo primero es verificar si el ciclo de carga llegó a su fin
# y setear como corresponda la variable de cambio de ciclo
# Lo segundo es verificar el estado del cargador y avisar al
# usuario para que lo enchufe o desenchufe

def gestion_carga_sin_controlador(carga_max):
    carga_max = int(carga_max)

# Decisión sobre si toca cambiar el ciclo o no:
    if bateria.carga() < carga_max:
        cambio_ciclo = 0
    else:
        cambio_ciclo = 1

# Decisión sobre el control del cargador. Sólo hay que considerar dos casos:
# 1) Si no acabó el ciclo (el ciclo de carga continúa), el cargador
# debe seguir enchufado. Lo verifico aviso al usuario si procede
# 2) Si acabó el ciclo (el ciclo de carga terminó), el cargador debe pasar
# a estar desenchufado. Lo verifico aviso al usuario si procede

# Si no cambia el ciclo => el cargador debe seguir enchufado. Si está
# desenchufado hay que avisar al usuario
    if cambio_ciclo == 0 and bateria.enchufada() == 0:
        messagebox.showerror("Apio informa",
        "Por favor, enchufa el cargador")

# Si cambia el ciclo => el cargador debe desenchufarse. Si está enchufado hay
# que avisar al usuario
    elif cambio_ciclo == 1 and bateria.enchufada() == 1:
        messagebox.showerror("Apio informa",
        "Por favor, desenchufa el cargador")

# No hay más casos que valorar
    else:
        pass

# Fin de función
    return cambio_ciclo