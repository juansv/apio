#! /usr/bin/python3
# -*- coding: utf-8 -*-
import tkinter
from tkinter import messagebox

from funciones_02_auxiliares.bateria_acpi import enchufada
from funciones_02_auxiliares.bateria_conf import descarga_profunda, \
    estado_ciclo, nivel_minimo, nivel_maximo, nivel_minimo_minimo, \
    frecuencia_descarga_profunda, actualiza_nivel_minimo, \
    actualiza_nivel_maximo, actualiza_nivel_minimo_minimo, \
    actualiza_frecuencia_descarga_profunda
from funciones_02_auxiliares.bateria_data import exportar
from funciones_02_auxiliares.control_cargador import conectado, conmuta
from funciones_02_auxiliares.control_crontab import onoff_apio, existe_job, \
    arranca_apio
from funciones_03_gui.ventana_ayuda import ayuda, fundamento, recetas, acerca


###############################################################################
#                                                                             #
#                          GUI de APIO                                        #
#                                                                             #
###############################################################################

# El GUI consta de una ventana sobre la que se sitúan varios widgets.
# Cada widget es un elemento de un interface gráfico: un botón, un texto,
# una caja para introducir datos, etc.
# Cada widget tiene sus peculiaridades a la hora de escribir el código,lo
# que hace que el código se estructure en tres partes:
# En la primera figuran una serie de funciones que son necesarias para
# representar ciertos widgets que contienen información que se actualiza
# en tiempo real.
# En la segunda, se escribe el código general de la ventana del GUI, donde se
# definen el título de la ventana, el tamaño, los iconos, la barra de menú,etc.
# Por último, en la tercera parte, se escribe el código de los distintos
# widgets cuyo estado es "estático", es decir, no contienen información que
# haya que actualizar en tiempo real, como los widget que contienen las
# funciones de la primera parte.
# Dentro del código, todos los widgets están organizados por las coordenadas
# que ocuparán dentro del grid de la ventana. El grid es un método que
# representa los widget situados en coordenadas fila-columna. Es decir,
# el grid es la parrilla, la rejilla, la cuadrícula sobre la que se colocan
# todos los widgets (botones, cajas de entrada de texto, gráficos, etc.).

# Grid del GUI de Apio quedó, finalmente, configurado en 6 columnas y 11
# filas. Inicialmente tenía 16 filas. Las últimas filas contenían el código
# de los widgets que representaban gráficamente la vida de la batería y
# permitían estimar su envejecimiento. Finalmente esta parte del código
# se desestimó y esas filas se eliminaron.
# El mapa del grid de la ventana es:

#    BARRA DE MENÚS
#
#    |   C1    |   C2    |   C3    |   C4    |   C5    |   C6    |
# ---+---------+---------+---------+---------+---------+---------+
# F1 |                        FILA LIBRE                         |
# ---+         +-------------------+                             |
# F2 |         |        Título     |                             |
# ---+    C    |-------------------+    C    +---------+    C    |
# F3 |    O    |    Apio on/off    |    O    | Botón   |    O    |
# ---+    L    +-------------------+    L    | Start/  |    L    |
# F4 |    U    |     Manual/auto   |    U    | Stop    |    U    |
# ---+    M    +---------+---------+    M    +---------+    M    |
# F5 |    N    |   Min   |  Dato   |    N    |         |    N    |
# ---+    A    +---------+---------+    A    |         |    A    |
# F6 |         |   Max   |  Dato   |         | Botón   |         |
# ---+    L    +---------+---------+    L    | Actua-  |    L    |
# F7 |    I    | Min-min |  Dato   |    I    | lizar   |    I    |
# ---+    B    +---------+---------+    B    |         |    B    |
# F8 |    R    |Desc.Prof|  Dato   |    R    |         |    R    |
# ---+    E    +---------+---------+    E    +---------+    E    |
# F9 |         |     Tipo ciclo    |         |Exportar |         |
# ---+         +-------------------+         +---------+         |
# F10|         |  Estado cargador  |         |Conmutar |         |
# ---+         +-------------------+         +---------+         |
# F11|                        FILA LIBRE                         |
# ---+-----------------------------------------------------------+

# Algunos widgets ocupan más de una coordenada. Por ejemplo, el título está
# ubicado en la coordenada (C2,F2) pero ocupa las columnas C2 y C3 de ancho.
# Esto está hecho así por cuestiones estéticas, para que los textos tengan
# la misma alineación, los botones abarquen a los textos con los que están
# relacionados, etc.


####################################################################
############# Función estado_apio() (C2,F3) a (C3,F3) ##############
####################################################################

# Esta función es necesaria para mostrar el estado de ejecución de apio
# (en marcha o parado) en un widget Label.
# Para que el contenido del label se actualice en tiempo real, es
# necesario usar el método after. Y el método after requiere de una
# función a la que llamar. Esta función debe actualizar el dato que
# se quiere mostrar en el label y crear el label en la posición que
# se le indique dentro del grid de la ventana
def estado_apio():
    # Determinamos el estado de ejecución de apio con la función existe_job()
    # que está en el módulo crontrol_crontab y cargo una variable de texto
    # con la información que quiero mostrar en el label.
    if existe_job() == 2:
        texto_estado_apio = "Apio en marcha"
        color = "green"
    else:
        texto_estado_apio = "     Apio parado"
        color = "red"
    # Ahora creo el label en la ventana principal del programa con el texto
    # que acabo de definir en función del estado de ejecución de apio
    estado = tkinter.Label(ventana, text=texto_estado_apio)
    estado.grid(row=3, column=2, columnspan=2, sticky='e')
    estado.config(fg=color)
    # Utilizo el método after para que actualice la información cada 250ms
    ventana.after(250, estado_apio)
    return


####################################################################
############## Función modo_apio() (C2,F4) a (C3,F4) ###############
####################################################################

# Esta función es necesaria para mostrar el modo de ejecución de apio
# (manual o automático) en un widget label.
# Para que el contenido del label se actualice en tiempo real, es
# necesario usar el método after. Y el método after requiere de una
# función a la que llamar. Esta función debe actualizar el dato que
# se quiere mostrar en el label y crear el label en la posición que
# se le indique dentro del grid de la ventana
def modo_apio():
    # Determinamos el modo de ejecución de apio con la función conectado()
    # que está en el módulo control_cargador y cargo una variable de texto
    # con la información que quiero mostrar en el label.
    if conectado() == 1:
        texto_modo_apio = "Apio en modo automático"
        color = "green"
    else:
        texto_modo_apio = "      Apio en modo manual"
        color = "black"
    # Si apio no está en marcha, pongo el texto en un color que indique
    # que el modo de funcionamiento es irrelevante
    if existe_job() != 2:
        color = "grey"
    else:
        pass
    # Ahora creo el label en la ventana principal del programa con el texto
    # que acabo de definir en función del estado de ejecución de apio
    estado = tkinter.Label(ventana, text=texto_modo_apio)
    estado.grid(row=4, column=2, columnspan=2, sticky='e')
    estado.config(fg=color)
    # Utilizo el método after para que actualice la información cada 250ms
    ventana.after(250, modo_apio)
    return


####################################################################
############## Función ciclo_apio() (C2,F9) a (C3,F9) ##############
####################################################################

# Esta función es necesaria para mostrar si el apio está en un ciclo de
# carga o de descarga en un widget label.
# Para que el contenido del label se actualice en tiempo real, es
# necesario usar el método after. Y el método after requiere de una
# función a la que llamar. Esta función debe actualizar el dato que
# se quiere mostrar en el label y crear el label en la posición que
# se le indique dentro del grid de la ventana
def ciclo_apio():
    # Determinamos el ciclo con las funciones estado_ciclo() y
    # descarga_profunda() que están en el módulo bateria_conf y cargo una
    # variable de texto con la información que quiero mostrar en el label.
    if descarga_profunda() == 1:
        texto_ciclo_apio = "En ciclo de descarga profunda"
        color = "blue"
    else:
        if estado_ciclo() == 1:
            texto_ciclo_apio = "                    En ciclo de carga"
            color = "black"
        else:
            texto_ciclo_apio = "                 En ciclo de descarga"
            color = "black"
    # Ahora creo el label en la ventana principal del programa con el texto
    # que acabo de definir en función del ciclo de trabajo
    estado = tkinter.Label(ventana, text=texto_ciclo_apio)
    estado.grid(row=9, column=2, columnspan=2, sticky='e')
    estado.config(fg=color)
    # Utilizo el método after para que actualice la información cada 250ms
    ventana.after(250, ciclo_apio)
    return


####################################################################
######### Función estado_cargador() (C2,F10) a (C3,F10) ############
####################################################################

# Esta función es necesaria para mostrar si el cargador está o no
# conectado en un widget label
# Para que el contenido del label se actualice en tiempo real, es
# necesario usar el método after. Y el método after requiere de una
# función a la que llamar. Esta función debe actualizar el dato que
# se quiere mostrar en el label y crear el label en la posición que
# se le indique dentro del grid de la ventana
def estado_cargador():
    # Determinamos el estado de ejecución de apio con la función existe_job()
    # que está en el módulo crontrol_crontab y cargo una variable de texto
    # con la información que quiero mostrar en el label.
    if enchufada() == 1:
        texto_estado_cargador = "Cargador on"
        color = "green"
    else:
        texto_estado_cargador = "Cargador off"
        color = "red"
    # Ahora creo el label en la ventana principal del programa con el texto
    # que acabo de definir en función del estado de ejecución de apio
    estado = tkinter.Label(ventana, text=texto_estado_cargador)
    estado.grid(row=10, column=2, columnspan=2, sticky='e')
    estado.config(fg=color)
    # Utilizo el método after para que actualice la información cada 250ms
    ventana.after(250, estado_cargador)
    return


####################################################################
############# Función new_datos() (C5,F5) a (C5,F8) ################
####################################################################

# Esta función es necesaria para actualizar los nuevos datos que se
# introduzcan en las cajas de datos en el archivo de configuración
# "bateria.conf". La función verifica que los datos son coherentes.
# Si no lo son, devuelve mensajes de error. Si lo son, actualiza los
# datos y devuelve mensaje de OK.
def new_datos():
    # El primer paso es capturar los nuevos datos que el/la usuario/a haya
    # metido en las cajas:
    new_minimo = minimo.get()
    new_maximo = maximo.get()
    new_min_min = min_min.get()
    new_ciclos = ciclos.get()
    # Ahora se verifica la coherencia de los datos
    # Primero se comprueba que todos los datos representan números
    if not new_minimo.isdigit()\
    or not new_maximo.isdigit()\
    or not new_min_min.isdigit()\
    or not new_ciclos.isdigit():
        messagebox.showerror("Datos incorrectos",
        "Por favor, algún dato no es un número")
    # Los valores inferiores no pueden ser mayores que los superiores
    elif int(new_minimo) >= int(new_maximo)\
    or int(new_min_min) >= int(new_minimo):
        messagebox.showerror("Datos incorrectos",
        "Por favor, revisa los datos, no tienen sentido")
    # Los datos tienen unos rangos máximos y mínimos.
    elif int(new_maximo) > 100 or int(new_ciclos) < 2:
        messagebox.showerror("Datos incorrectos",
        "Por favor, revisa los datos, alguno está fuera de rango")
    # Si todo esta bien, se actualizan los datos
    else:
        actualiza_nivel_minimo(new_minimo)
        actualiza_nivel_maximo(new_maximo)
        actualiza_nivel_minimo_minimo(new_min_min)
        actualiza_frecuencia_descarga_profunda(new_ciclos)
        messagebox.showinfo("Datos actualizados",
        "Actualización de datos correcta")
    return


####################################################################
################### Función conmutador() (C5,F10) ##################
####################################################################

# Esta función se utiliza para conmutar el estado del cargador si se
# dispone del hardware de control. Si no se dispone de dicho hardware,
# la función no tiene sentido. Por eso mismo, esta función evalua
# si está conectado el controlador del cargador y, en caso contrario,
# advierte que no hará nada. Si no fuese por esta ventana de advertencia
# no sería necesaria esta función, se podría invocar a la función
# conmuta() del módulo control_cargador.py directamente desde el botón
def conmutador():
    if conectado() == 1:
        conmuta()
    elif conectado() == 0:
        messagebox.showerror("Modo manual",
        "Este botón no está operativo en modo manual")
    else:
        pass
    return


####################################################################
################## Ventana, configuración general ##################
####################################################################

# Aquí viene el código básico de la ventana del GUI de Apio. Es el código
# que define el tamaño de la ventana, el icono, etc.

# Creamos la ventana
ventana = tkinter.Tk()

# Icono de la ventana. Estas líneas utilizan el archivo 'apio_blanco.png' como
# icono del marco de la ventana y en la barra de programas del sistema operativo
icono_apio = tkinter.PhotoImage(file='/usr/share/pixmaps/apio_blanco.png')
ventana.tk.call('wm', 'iconphoto', ventana._w, icono_apio)

# color fondo de la ventana
#ventana.config(bg="white")

# Tamaño de la ventana. Esta línea define el tamaño que tendrá la ventana
# cuando se lance el GUI de apio.
#ventana.geometry("500x222")

# Hacer la ventana de tamaño fijo. Añadiendo esta línea de código, el tamaño
# de la ventana será fijo, el usuario no la podrá cambiar pinchando y
# arrastrando en las esquinas de la ventana
ventana.resizable(width='FALSE', height='FALSE')

# Título en el marco de la ventana
ventana.title('Configurador de Apio')


####################################################################
######################### Barra de menú ############################
####################################################################

# Aquí viene las líneas que programan la barra de menús
barramenu = tkinter.Menu(ventana)

# Menú Archivo
menuarchivo = tkinter.Menu(barramenu, tearoff=0)
# Comandos del menú Archivo
menuarchivo.add_command(label='Start', command=lambda: arranca_apio(1))
menuarchivo.add_command(label='Stop', command=lambda: arranca_apio(0))
menuarchivo.add_command(label='Actualizar', command=new_datos)
menuarchivo.add_command(label='Exporta datos', command=exportar)
menuarchivo.add_command(label='Conmutar', command=lambda: conmuta())
menuarchivo.add_command(label='Salir', command=ventana.quit)
# Fin del menú Archivo
barramenu.add_cascade(label='Archivo', menu=menuarchivo)

# Menú Ayuda
menuayuda = tkinter.Menu(barramenu, tearoff=0)
# Comandos del menú Ayuda
menuayuda.add_command(label='Ayuda rápida', command=ayuda)
menuayuda.add_command(label='Fundamento de apio', command=fundamento)
menuayuda.add_command(label='Recetas de apio', command=recetas)
menuayuda.add_command(label='Acerca de', command=acerca)
# Fin del menú Ayuda
barramenu.add_cascade(label='Ayuda', menu=menuayuda)

# Fin de la barra de menú
ventana.config(menu=barramenu)


####################################################################
##################### Filas y columnas libres ######################
####################################################################

# Añado las filas 1 y 11 y las columnas 1, 4 y 6 como espacios libres
# Para ello les pongo el widget Label sin ningún contenido
# Si simplemente omito colocar elementos en las filas que quiero dejar
# vacías, no sirve. Por ejemplo, si omito la fila 2 en mi programación,
# parecerá que los elementos que haya dispuesto en la fila 3 pasan a
# la fila 2, los de la fila 4 a la 3, etc. Es decir, omitir una fila
# o columna no significa que el programa representará una fila o
# columna vacía, significa que el programa representará una fila
# o columna de alto cero, o ancho cero, según corresponda. La única
# forma que la fila o columna se represente pero sin contenido, es
# usando un widget que no represente ningún elemento en pantalla,
# como un widget Label con texto ' '
espacio_01 = tkinter.Label(ventana, text=' ')
espacio_01.grid(row=1, column=1)
espacio_02 = tkinter.Label(ventana, text=' ')
espacio_02.grid(row=1, column=4)
espacio_03 = tkinter.Label(ventana, text=' ')
espacio_03.grid(row=1, column=6)
espacio_04 = tkinter.Label(ventana, text=' ')
espacio_04.grid(row=11, column=1)


####################################################################
################ Título del panel (C2,F2) a (C3,F2) ################
####################################################################

# Título de la parte del panel de control y configuración de apio
etiqueta_panel = tkinter.Label(ventana, text='CONFIGURACIÓN Y CONTROL DE APIO')
etiqueta_panel.config(fg='blue')
# La opción columnspan (y su equivalente filespan) permite al widget
# que se trate ocupar dos o más columnas
etiqueta_panel.grid(row=2, column=2, columnspan=2, sticky='e')


####################################################################
################## Valor mínimo (C2,F5) y (C3,F5) ##################
####################################################################

# Cuadro texto para valor mínimo (C2,F5)
etiqueta_1 = tkinter.Label(ventana, text='Valor mínimo de descarga (%)')
etiqueta_1.grid(row=5, column=2, sticky='e')
# Caja de entrada de datos (C3,F5)
minimo = tkinter.StringVar()
caja_1 = tkinter.Entry(ventana, textvariable=minimo, width=3)
caja_1.insert(0, nivel_minimo())
caja_1.grid(row=5, column=3, sticky='w')


####################################################################
################## Valor máximo (C2,F6) y (C3,F6) ##################
####################################################################

# Cuadro de texto para valor máximo (C2,F6)
etiqueta_2 = tkinter.Label(ventana, text='Valor máximo de carga (%)')
etiqueta_2.grid(row=6, column=2, sticky='e')
# Caja de entrada de datos (C3,F6)
maximo = tkinter.StringVar()
caja_2 = tkinter.Entry(ventana, textvariable=maximo, width=3)
caja_2.insert(0, nivel_maximo())
caja_2.grid(row=6, column=3, sticky='w')


####################################################################
############### Valor mínimo-mínimo (C2,F7) y (C3,F7) ##############
####################################################################

# Cuadro de texto para valor mínimo_mínimo (C2,F7)
etiqueta_3 = tkinter.Label(ventana,
    text='Valor mínimo en descarga profunda (%)')
etiqueta_3.grid(row=7, column=2, sticky='e')
# Caja de entrada de datos (C3,F7)
min_min = tkinter.StringVar()
caja_3 = tkinter.Entry(ventana, textvariable=min_min, width=3)
caja_3.insert(0, nivel_minimo_minimo())
caja_3.grid(row=7, column=3, sticky='w')


####################################################################
############# Valor descarga profunda (C2,F8) y (C3,F8) ############
####################################################################

# Cuadro de texto para el número de ciclos para descarga profunda (C2,F8)
etiqueta_4 = tkinter.Label(ventana, text='Ciclos para desgarda profunda')
etiqueta_4.grid(row=8, column=2, sticky='e')
# Caja de entrada de datos (C3,F8)
ciclos = tkinter.StringVar()
caja_4 = tkinter.Entry(ventana, textvariable=ciclos, width=3)
caja_4.insert(0, frecuencia_descarga_profunda())
caja_4.grid(row=8, column=3, sticky='w')


####################################################################
############### Botón "Actualizar" (C5,F5) a (C5,F8) ###############
####################################################################

# Botón de carga de nuevos datos. Recoge los datos de las cajas de
# entrada de datos y actualiza los valores en el archivo de configuración
# Para ello usamos una función que se encarga de este trabajo. En
# el botón sólo tengo que invocar esta función. La función se llama
# new_datos y se escribe en la parte superior del código
actualizar = tkinter.Button(ventana, text='Actualizar', command=new_datos)
# Para que el botón se "estire" y ocupe todo el espacio disponible de las
# filas y columnas donde está ubicado, hay que usar la opción sticky en
# todas las direcciones
actualizar.grid(row=5, column=5, rowspan=4, sticky='wens')


####################################################################
############### Botón "Start/Stop" (C5,F3) a (C5,F4) ###############
####################################################################

# Botón parada/arranque del programa. Habilita o deshabilita el
# programa en el CRON. Hace uso de la función onoff_apio() que está
# en el módulo control_crontab.py
detener = tkinter.Button(ventana,
    text='Start/Stop', command=lambda: onoff_apio())
# Para que el botón se "estire" y ocupe todo el espacio disponible de las
# filas y columnas donde está ubicado, hay que usar la opción sticky en
# todas las direcciones
detener.grid(row=3, column=5, rowspan=2, sticky='wens')


####################################################################
##################### Botón "Exportar" (C5,F9) #####################
####################################################################

# Botón para exportar los datos de la batería. Toma los datos del archivo
# bateria.data y los exporta a un csv o un txt. Hace uso de la función
# exportar() que está en el módulo bateria_data.py
detener = tkinter.Button(ventana, text='Exportar', command=lambda: exportar())
detener.grid(row=9, column=5, sticky='we')


####################################################################
##################### Botón "Conmutar" (C5,F10) ####################
####################################################################

# Botón para conmutar el cargador (solo modo automático). Envía orden
# a través del USB al circuito de control del cargador para que cambie
# de estado. Hace uso de la función conmuta() del módulo
# control_cargador.py
# El código que se encarga de realizar este trabajo se envió a la función
# conmutador() que está más arriba. La función evalua si está el
# hardware de control conectado (modo automático) y realiza la
# conmutación del estado del cargador. Si no estuviese conectado,
# devuelve un mensaje de error.
conmutar = tkinter.Button(ventana, text='Conmutar', command=conmutador)
conmutar.grid(row=10, column=5, sticky='we')
# Si no hay un hardware de control conectado, el botón se pone gris
# para indicar que está deshabilitado. Si se conecta o desconecta en
# caliente no hace nada. Fue necesario dejarlo así, sin que se actualice
# el color del botón en tiempo real, porque cuando se usa el método
# after el widget queda inactivo durante los instantes en que se está
# refrescando. Eso hace la sensación de manejo del botón desagradable,
# ya que sólo funciona cuando está activo el widget, lo que obliga a
# hacer varias veces clic hasta que el botón obedece, que es cuando
# pillas al widget en un momento de actividad.
# Para saber si está conectado el hardware de control, hace uso de
# la función conectado() del módulo control_cargador.py
if conectado() == 1:
    color = "black"
else:
    color = "grey"
conmutar.config(fg=color)


####################################################################
################# Estado de Apio (C2,F3) a (C3,F3) #################
####################################################################

# Muestra el estado de ejecución de Apio (marcha o parado) en un label
# colocado en las coordenadas indicadas.
# Para que el dato se refresque en tiempo real durante la ejecución
# del GUI, es necesario utilizar el método "after" junto con una llamada
# a una función en donde se configura el label.
# La función debe escribirse en la parte superior del programa, justo
# después de los import. El método after también obliga a que se indique
# cada cuanto se actualiza el dato. En este caso es cada 250ms
ventana.after(250, estado_apio)


####################################################################
################## Modo de Apio (C2,F4) a (C3,F4) ##################
####################################################################

# Muestra el modo de ejecución de Apio (manual o automático) en un label
# colocado en las coordenadas indicadas.
# Para que el dato se refresque en tiempo real durante la ejecución
# del GUI, es necesario utilizar el método "after" junto con una llamada
# a una función en donde se configura el label.
# La función debe escribirse en la parte superior del programa, justo
# después de los import. El método after también obliga a que se indique
# cada cuanto se actualiza el dato. En este caso es cada 250ms
ventana.after(250, modo_apio)


####################################################################
################## Ciclo de Apio (C2,F9) a (C3,F9) #################
####################################################################

# Muestra el ciclo de ejecución de Apio (carga o descarga) en un label
# colocado en las coordenadas indicadas.
# Para que el dato se refresque en tiempo real durante la ejecución
# del GUI, es necesario utilizar el método "after" junto con una llamada
# a una función en donde se configura el label.
# La función debe escribirse en la parte superior del programa, justo
# después de los import. El método after también obliga a que se indique
# cada cuanto se actualiza el dato. En este caso es cada 250ms
ventana.after(250, ciclo_apio)


####################################################################
############ Estado del cargador (C2,F10) a (C3,F10) ###############
####################################################################

# Muestra el estado del cargador (on u off) en un label
# colocado en las coordenadas indicadas.
# Para que el dato se refresque en tiempo real durante la ejecución
# del GUI, es necesario utilizar el método "after" junto con una llamada
# a una función en donde se configura el label.
# La función debe escribirse en la parte superior del programa, justo
# después de los import. El método after también obliga a que se indique
# cada cuanto se actualiza el dato. En este caso es cada 250ms
ventana.after(250, estado_cargador)

# Cierre de la ventana principal
ventana.mainloop()
