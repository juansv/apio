#! /usr/bin/python3
# -*- coding: utf-8 -*-

###############################################################################
#                                                                             #
#                      Configurador de APIO                                   #
#                                                                             #
###############################################################################

# El configurador de apio es el equivalente al apio_gui pero desde línea de
# comandos. Las utilidades previstas desde apio-conf son:
#
# -a --arranca: arranca apio y lo habilita en el cron.
#
# -p --para: para la ejecución de apio y lo inhabilita en el cron.
#
# -e --estado: verifica el estado de apio y devuelve la info por terminal de
# texto. El estado se refiere a si apio está on u off, si está en modo
# manual o automático y si está en ciclo de carga, de descarga o de descarga
# profunda.
#
# -c --conmutar: da orden de conmutar el cargador del portátil de encendido a
# apagado y viceversa. Sólo tiene sentido si apio está en modo automático.
#
# -min VALOR --minimo VALOR: indica el valor mínimo de carga que alcanzará la
# batería durante la descarga.
#
# -max VALOR --maximo VALOR: indica el valor máximo de carga que alcanzará la
# batería durante la carga.
#
# -prf VALOR --profundo VALOR: indica el valor mínimo de carga que alcanzará la
# batería durante una descarga profunda.
#
# -cls VALOR --ciclos VALOR: indica el número de ciclos de funcionamiento
# normal antes de proceder a una descarga profunda.
#
# -info {fundamento, recetas, datos}: muestra los distintos
# archivos con información.
#
# La sintaxis general prevista para apio-conf sería como en los siguientes
# ejemplos:
#    Arrancar apio: $ apio-conf -a
#    Fijar el nivel mínimo de carga en 40%: $ apio-conf -min 40
#    Ver el estado de apio: $ apio-conf --estado
#    Fijar el nivel máximo de carga en 80% y el de descarga profunda en
#    5%: $ apio-conf --maximo 80 -prf 5
#    Ver las recetas de apio: $ apio-conf --ayuda recetas

##################################################
#                  MÓDULOS                       #
##################################################

# Carga del módulo de gestión de argumentos
import argparse

# Carga de las funciones locales
from funciones_02_auxiliares.bateria_acpi import enchufada
from funciones_02_auxiliares.bateria_conf import descarga_profunda, \
    estado_ciclo, nivel_minimo, nivel_maximo, nivel_minimo_minimo, \
    frecuencia_descarga_profunda, actualiza_nivel_minimo, \
    actualiza_nivel_maximo, actualiza_nivel_minimo_minimo, \
    actualiza_frecuencia_descarga_profunda
from funciones_02_auxiliares.control_cargador import conectado, conmuta
from funciones_02_auxiliares.control_crontab import existe_job, arranca_apio


##################################################
#                  PROGRAMA                      #
##################################################

# Iniciar una lista de argumentos para los argumentos normales
# y una sub-lista para los argumentos incompatibles entre sí
lista_argumentos = argparse.ArgumentParser(description=
    "Configurador de los parámetros de apio")
excluyente = lista_argumentos.add_mutually_exclusive_group()

# Carga de los argumentos en la lista_argumentos
# Arranca, argumento booleano
excluyente.add_argument("-a", "--arranca", action="store_true",
     help="carga apio en el cron y arranca apio")
# Para, argumento booleano
excluyente.add_argument("-p", "--para", action="store_true",
     help="deshabilita apio en el cron, lo que detiene apio")
# Estado, argumento booleano
lista_argumentos.add_argument("-e", "--estado", action="store_true",
     help="muestra el estado actual de apio")
# Conmutar, argumento booleano
excluyente.add_argument("-c", "--conmutar", action="store_true",
     help="conmuta el cargador (si existe el hardware apio)")
# Mínimo, entero
lista_argumentos.add_argument("-min", "--minimo", type=int,
     help="valor mínimo de descarga en ciclo de trabajo normal")
# Máximo, entero
lista_argumentos.add_argument("-max", "--maximo", type=int,
     help="valor máximo de carga en ciclo de trabajo normal")
# Profundo, entero
lista_argumentos.add_argument("-prf", "--profundo", type=int,
     help="valor mínimo de descarga en ciclo de descarga profunda")
# Ciclos, entero
lista_argumentos.add_argument("-cls", "--ciclos", type=int,
     help="número de ciclos entre descargas profundas")
# Información, cadena de caracteres
lista_argumentos.add_argument("-info", "--informacion", type=str,
     choices=["fundamento", "recetas", "datos"],
     help="información detallada y datos almacenados")

# Inicializamos el uso de argumentos
argumento = lista_argumentos.parse_args()

#######################################
# Argumentos para arrancar y parar apio
if argumento.arranca:
    # Si ya está en marcha no hay que hacer nada
    if existe_job() == 2:
        print("Apio ya está en marcha.")
    else:
        print("Programando Apio en el CRON...")
        arranca_apio(1)
        print("Apio está en marcha.")
if argumento.para:
    if existe_job() != 2:
        print("Apio ya está parado.")
    else:
        print("Parando Apio...")
        arranca_apio(0)
        print("Apio está parado.")

########################################
# Argumento para saber el estado de apio
if argumento.estado:
    # Primero compruebo si Apio está en marcha y en qué modo de funcionamiento
    # Y si está en marcha (existe_job() == 2), hay que verificar también si
    # está en modo manual o automático (conectado()) y si está en descarga
    # profunda o en carga/descarga (funciones descarga_profunda()
    # y estado_ciclo())
    if existe_job() == 2:
        if conectado() == 1:
            print("Apio está en marcha y en modo automático")
        else:
            print("Apio está en marcha y en modo manual")
        if descarga_profunda() == 1:
            print("En ciclo de descarga profunda")
        else:
            if estado_ciclo() == 1:
                print("En ciclo de carga")
            else:
                print("En ciclo de descarga")
    # Y si no está en marcha... pues nada
    else:
        print("Apio está parado")
    # Para terminar muestro los valores de configuración
    print("Apio está configurado para funcionar con los siguientes parámetros:")
    print("Nivel máximo de carga: ", nivel_maximo(), "%")
    print("Nivel mínimo de carga: ", nivel_minimo(), "%")
    print("Nivel de descarga profunda: ", nivel_minimo_minimo(), "%")
    print("Ciclos entre descargas profundas: ", frecuencia_descarga_profunda())

#####################################################################
# Argumento para conmutar el cargador. Este argumento es incompatible
# con el arranque o parada de apio.
if argumento.conmutar:
    # Se comprueba que el hardware apio está conectado y, si lo está, ordena
    # conmutar el cargador e informa del estado
    if conectado() == 1:
        print("Lanzado orden para conmutar el cargador...")
        conmuta()
        if enchufada() == 1:
            print("Ahora el cargador está conectado.")
        else:
            print("Ahora el cargador está desconectado.")
    # Si no se detecta el hardware de apio... pues nada
    else:
        print("No se detecta ningún dispositivo de hardware...")
        print("Lo siento, no puedo ordenar la conmutación del cargador.")

#####################################################################
# Argumento para fijar el valor mínimo de descarga en ciclos normales
if argumento.minimo:
    # El valor de carga mínimo no puede ser mayor que el valor de carga
    # máximo, ni menor que el de descarga profunda, lo verifico
    if argumento.minimo >= nivel_maximo()\
    or argumento.minimo <= nivel_minimo_minimo()\
    or argumento.minimo >> 100\
    or argumento.minimo <= 0:
        print("Por favor, revisa el dato de carga mínima.")
        print("Debe ser un valor entre 1 y 100")
        print("Y menor que el actual valor de carga máximo")
    else:
        actualiza_nivel_minimo(argumento.minimo)
        print("Nuevo porcentaje de carga mínima en ciclos normales: ",
             nivel_minimo(), "%")

##################################################################
# Argumento para fijar el valor máximo de carga en ciclos normales
if argumento.maximo:
    # El valor de carga máximo no puede ser menor que el valor de carga
    # mínimo, ni menor que el de descarga profunda, lo verifico
    if argumento.maximo <= nivel_minimo()\
    or argumento.maximo <= nivel_minimo_minimo()\
    or argumento.maximo >> 100\
    or argumento.maximo <= 0:
        print("Por favor, revisa el dato de carga máximo.")
        print("Debe ser un valor entre 1 y 100")
        print("Y mayor que el actual valor de carga mínimo")
    else:
        actualiza_nivel_maximo(argumento.maximo)
        print("Nuevo porcentaje de carga máxima en ciclos normales: ",
             nivel_maximo(), "%")

#######################################################################
# Argumento para fijar el valor mínimo de descarga en ciclo de descarga
# profunda
if argumento.profundo:
    # El valor de descarga profunda no puede ser mayor que el valor de carga
    # máximo ni que el mínimo, lo verifico
    if argumento.profundo >= nivel_maximo()\
    or argumento.profundo >= nivel_minimo()\
    or argumento.profundo >> 100\
    or argumento.profundo <= 0:
        print("Por favor, revisa el dato de descarga profunda.")
        print("Debe ser un valor entre 1 y 100")
        print("Y menor que los valores actuales del ciclo normal de carga")
    else:
        actualiza_nivel_minimo_minimo(argumento.profundo)
        print("Nuevo porcentaje de carga en ciclos de descarga profunda:",
             nivel_minimo_minimo(), "%")

####################################################################
# Argumento para fijar el número de ciclos entre descargas profundas
if argumento.ciclos:
    # El valor del número de ciclos no puede ser cero ni uno, ya que
    # todos los ciclos serían de descarga profunda
    if argumento.ciclos <= 1:
        print("Por favor, revisa el número de ciclos entre descargas profundas")
        print("Debe ser un entero positivo distinto de cero y uno.")
    else:
        actualiza_frecuencia_descarga_profunda(argumento.ciclos)
        print("Nuevo intervalo de ciclos entre descargas profundas:",
             frecuencia_descarga_profunda(), " ciclos")

#################################################
# Argumento para mostrar la información detallada
if argumento.informacion == "fundamento":
    # abro el archivo de texto con el texto informativo
    archivo_info = open("/usr/share/doc/apio/ayuda_fundamento.txt", "r")
    # leo el archivo
    contenido_info = archivo_info.read()
    # cierro el archivo
    archivo_info.close()
    # muestro la información
    print(contenido_info)
if argumento.informacion == "recetas":
    # abro el archivo de texto con el texto informativo
    archivo_info = open("/usr/share/doc/apio/ayuda_recetas.txt", "r")
    # leo el archivo
    contenido_info = archivo_info.read()
    # cierro el archivo
    archivo_info.close()
    # muestro la información
    print(contenido_info)
if argumento.informacion == "datos":
    # abro el archivo de texto con el texto informativo
    archivo_info = open("/var/lib/apio/bateria.data", "r")
    # leo el archivo
    contenido_info = archivo_info.read()
    # cierro el archivo
    archivo_info.close()
    # muestro la información
    print(contenido_info)