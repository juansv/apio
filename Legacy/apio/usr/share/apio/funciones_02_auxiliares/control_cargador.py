#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo tiene dos funciones: una es conocer si existe
# un circuito controlador del cargador conectado en el USB.
# La otra, es sacar por el USB-RS232 un pulso (flanco de
# bajada) para que el controlador del cargador cambie de
# estado
# En debian, para python3, el módulo serial está en el paquete "python3-serial"
import serial
# Además de importar serial, hay que importar explícitamente el módulo
# serial.tools, que me permitirá detectar si el circuito controlador está
# conectado
from serial.tools import list_ports
# Necesito importar el módulo time para temporizar después de conmutar el
# cargador, y permitirle al sistema operativo enterarse que la conmutación
# se produjo. De esta forma se evitan molestos mensajes emergentes
import time


###############################################################
############### FUNCIÓN conectado() ###########################
###############################################################
# Comprueba si hay algún puerto serie disponible. Parte de una premisa
# que puede ser errónea: El portátil no tiene nigún puerto serie "real"
# Si se trata de un portátil viejo, que tenga un puerto serie, la función
# no está preparada y no funcionará.
# Pero si es un portátil más o menos moderno, sólo tendrá puertos USB.
# Por lo tanto, si detecta un puerto serie significa que se conectó un
# emulador RS-232 por el USB. Se interpreta que ese emulador RS-232
# corresponde al circuito de control del cargador.
# En caso de detectar que el circuito de control está conectado, devuelve 1
# y en el caso contrario devuelve 0

def conectado():
    # Genera una lista con los puertos "serie" disponibles. La genera tanto
    # si hay puertos reales como si son emulados con un hardware USB-RS232
    lista = list_ports.comports()
    # Si no hay puertos serie, genera una lista vacía. Python asigna un valor
    # booleano a las listas según estén vacías (si vacía => False) o no lo
    # estén (si no vacía => True). Luego se puede consultar si la lista está
    # vacía (lista vacía = no hay puerto serie) con una simple instrucción
    if lista:
        # Si es True, hay lista, luego hay puerto, luego está conectado
        conectado = 1
    else:
        # En caso contrario es False y no hay puerto ni nada conectado
        conectado = 0
    return conectado


###############################################################
############### FUNCIÓN conmuta() #############################
###############################################################
# Esta función manda conmutar el estado del cargador haciendo
# uso del adaptador USB-RS232 y el hardware de control. En
# primer lugar verifica que el hardware de control está
# conectado. Para ello replica la función anterior. Si el
# adaptador está conectado, realiza lo siguiente:
# Envía un tren de ceros (0x00) por el adaptador
# USB-RS232. Esto genera un flanco de bajada que detecta el
# hardware de control del cargador.
# Después espera 3 segundos antes de devolver el control al
# programa principal, con objeto de darle tiempo a reaccionar
# al cambio y evitar que salte la ventana de aviso de que el
# controlador "no funciona".
# Si el hardware no está conectado no hace nada.

def conmuta():
    # Genera una lista con los puertos "serie" disponibles. La genera tanto
    # si hay puertos reales como si son emulados con un hardware USB-RS232
    lista = list_ports.comports()
    # Si no hay puertos serie, genera una lista vacía. Python asigna un valor
    # booleano a las listas según estén vacías (si vacía => False) o no lo
    # estén (si no vacía => True). Luego se puede consultar si la lista está
    # vacía (lista vacía = no hay puerto serie) con una simple instrucción
    if lista:
        # Si es True, hay lista, luego está conectado el circuito de control
        # Por lo tanto puedo enviar una orden (todo ceros "0x00") para
        # conmutar el estado del cargador
        s = serial.Serial('/dev/ttyUSB0', 115200)
        s.write([0x00])
        s.close()
        # Una vez cambiado el estado del cargador, espero 3 segundos para que
        # el sistema operativo detecte que cambió el estado del cargador
        # y Apio no devuelva ventanas de error
        time.sleep(3)
    else:
        # En caso contrario es False y no hay puerto ni nada conectado. Por
        # lo tanto no puedo hacer nada
        pass
    return