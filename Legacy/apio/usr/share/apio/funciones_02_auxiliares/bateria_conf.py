#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo devuelve un dato del archivo de configuración
# que usa el programa para saber ciertos datos sobre la
# gestión que tiene que hacer


###############################################################
############### FUNCIÓN ciclos() ##############################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el estado del cargador.
# Esa línea se marca con @1 en la línea inmediatamente
# superior. Se leen todas las líneas hasta la que tiene valor @1
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos.

def ciclos():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@01\n':
            dato_encontrado = True
    ciclos = int(linea_conf)
    configuracion.close()
    return ciclos


###############################################################
############### FUNCIÓN actualiza_ciclos() ####################
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el estado del cargador (@01). Cambia el elemento
# siguiente de la lista con el valor que recibió la función
# y sobre escribe todo el archivo con el contenido actualizado
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @01 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_ciclos(nuevo_numero_ciclos):
# Convierto el dato en string
    nuevo_numero_ciclos = str(nuevo_numero_ciclos)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_numero_ciclos = nuevo_numero_ciclos + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@01\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_numero_ciclos
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
############### FUNCIÓN estado_ciclo() ########################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el estado del ciclo.
# Esa línea se marca con @2 en la línea inmediatamente
# superior. Se leen todas las líneas hasta la que tiene valor @2
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero (0 ó 1) que representa el estado del ciclo.

def estado_ciclo():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@02\n':
            dato_encontrado = True
    estado_ciclo = int(linea_conf)
    configuracion.close()
    return estado_ciclo


###############################################################
############### FUNCIÓN actualiza_estado_ciclo() ##############
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el estado del ciclo (@02). Cambia el elemento
# siguiente de la lista con el valor que recibió la función
# y sobre escribe todo el archivo con el contenido actualizado
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @02 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_estado_ciclo(nuevo_estado_ciclo):
# Convierto el dato en string
    nuevo_estado_ciclo = str(nuevo_estado_ciclo)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_estado_ciclo = nuevo_estado_ciclo + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@02\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_estado_ciclo
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
############### FUNCIÓN descarga_profunda() ###################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica si estamos en descarga
# profunda. Esa línea se marca con @3 en la línea inmediatamente
# superior. Se leen todas las líneas hasta la que tiene valor @3
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero (0 ó 1) que representa el estado del ciclo.

def descarga_profunda():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@03\n':
            dato_encontrado = True
    descarga_profunda = int(linea_conf)
    configuracion.close()
    return descarga_profunda


###############################################################
############### FUNCIÓN actualiza_descarga_profunda() #########
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica si estamos en descarga profunda (@03). Cambia el
# elemento siguiente de la lista con el valor que recibió la
# función y sobre escribe todo el archivo con el contenido
# actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @03 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_descarga_profunda(nuevo_estado_descarga_profunda):
# Convierto el dato en string
    nuevo_estado_descarga_profunda = str(nuevo_estado_descarga_profunda)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_estado_descarga_profunda = nuevo_estado_descarga_profunda + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@03\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_estado_descarga_profunda
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
############### FUNCIÓN ejecuciones_programa() ################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el número de veces que se
# ejecutó el programa en el ciclo en curso de carga/descarga
# Esa línea se marca con @4 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @4
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos de programa.

def ejecuciones_programa():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@04\n':
            dato_encontrado = True
    ejecuciones_programa = int(linea_conf)
    configuracion.close()
    return ejecuciones_programa


###############################################################
############### FUNCIÓN actualiza_ejecuciones_programa() ######
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el número de ciclos de programa que se llevan
# ejecutados (@04). Cambia el elemento siguiente de la lista
# con el valor que recibió la función y sobre escribe todo el
# archivo con el contenido actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @04 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_ejecuciones_programa(nuevo_numero_ejecuciones_programa):
# Convierto el dato en string
    nuevo_numero_ejecuciones_programa = str(nuevo_numero_ejecuciones_programa)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_numero_ejecuciones_programa = nuevo_numero_ejecuciones_programa + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@04\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_numero_ejecuciones_programa
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
################## FUNCIÓN nivel_maximo() #####################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el nivel máximo de carga
# en % que se usa como tope en el ciclo de carga normal.
# Esa línea se marca con @5 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @5
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos de programa.

def nivel_maximo():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@05\n':
            dato_encontrado = True
    nivel_maximo = int(linea_conf)
    configuracion.close()
    return nivel_maximo


###############################################################
################## FUNCIÓN actualiza_nivel_maximo() ###########
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el nivel máximo de carga (@05). Cambia el elemento
# siguiente de la lista con el valor que recibió la función y
# sobre-escribe todo el archivo con el contenido actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @05 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_nivel_maximo(nuevo_nivel_maximo):
# Convierto el dato en string
    nuevo_nivel_maximo = str(nuevo_nivel_maximo)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_nivel_maximo = nuevo_nivel_maximo + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@05\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_nivel_maximo
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
################## FUNCIÓN nivel_minimo() #####################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el nivel mínimo de carga
# en % que se usa como tope en el ciclo de carga normal.
# Esa línea se marca con @6 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @6
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos de programa.

def nivel_minimo():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@06\n':
            dato_encontrado = True
    nivel_minimo = int(linea_conf)
    configuracion.close()
    return nivel_minimo


###############################################################
################## FUNCIÓN actualiza_nivel_minimo() ###########
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el nivel mínimo de carga (@06). Cambia el elemento
# siguiente de la lista con el valor que recibió la función y
# sobre-escribe todo el archivo con el contenido actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @06 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_nivel_minimo(nuevo_nivel_minimo):
# Convierto el dato en string
    nuevo_nivel_minimo = str(nuevo_nivel_minimo)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_nivel_minimo = nuevo_nivel_minimo + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@06\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_nivel_minimo
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
################## FUNCIÓN nivel_minimo_minimo() ##############
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el nivel mínimo de carga
# en % que se usa como tope en el ciclo de carga profunda.
# Esa línea se marca con @7 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @7
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos de programa.

def nivel_minimo_minimo():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@07\n':
            dato_encontrado = True
    nivel_minimo_minimo = int(linea_conf)
    configuracion.close()
    return nivel_minimo_minimo


###############################################################
########### FUNCIÓN actualiza_nivel_minimo_minimo() ###########
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica el nivel mínimo de descarga profunda (@07). Cambia
# el elemento siguiente de la lista con el valor que recibió la
# función y sobre-escribe todo el archivo con el contenido
# actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @07 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_nivel_minimo_minimo(nuevo_nivel_minimo_minimo):
# Convierto el dato en string
    nuevo_nivel_minimo_minimo = str(nuevo_nivel_minimo_minimo)
# Añado retorno de carro al nuevo valor del cargador
    nuevo_nivel_minimo_minimo = nuevo_nivel_minimo_minimo + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@07\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nuevo_nivel_minimo_minimo
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
############ FUNCIÓN frecuencia_descarga_profunda() ###########
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica la frecuencia de descarga
# profunda (en ciclos de carga/descarga normal).
# Esa línea se marca con @8 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @8
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# entero que representa el número de ciclos de programa.

def frecuencia_descarga_profunda():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@08\n':
            dato_encontrado = True
    frecuencia_descarga_profunda = int(linea_conf)
    configuracion.close()
    return frecuencia_descarga_profunda


###############################################################
####### FUNCIÓN actualiza_frecuencia_descarga_profunda() ######
###############################################################
# Abre el archivo de configuración bateria.conf y carga cada
# línea del archivo a una lista de nombre "lista_lineas"
# Busca, en la lista que acabamos de crear, la línea que
# identifica la frecuencia de descarga profunda (@08). Cambia
# el elemento siguiente de la lista con el valor que recibió la
# función y sobre-escribe todo el archivo con el contenido
# actualizado.
# NOTA: como el número de elementos de una lista empieza por
# cero, al localizar el elemento siguiente a @08 no es necesario
# incrementar num_linea en una unidad.
# NOTA: esta función espera recibir un string, luego hay que
# convertir el dato en string, por si acaso.

def actualiza_frecuencia_descarga_profunda(nueva_frecuencia_descarga_profunda):
# Convierto el dato en string
    nueva_frecuencia_descarga_profunda = str(nueva_frecuencia_descarga_profunda)
# Añado retorno de carro al nuevo valor del cargador
    nueva_frecuencia_descarga_profunda = \
    nueva_frecuencia_descarga_profunda + "\n"
# Carga el archivo de configuración línea a línea en lista_lineas
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    lista_lineas = configuracion.readlines()
    configuracion.close()
# Inicializa un contador de líneas y busca el elemento a actualizar
    num_linea = 0
    for contenido_linea in lista_lineas:
        num_linea = num_linea + 1
        if contenido_linea == '@08\n':
            break
# Actualiza el elemento en lista_lineas
    lista_lineas[num_linea] = nueva_frecuencia_descarga_profunda
# Sobre escribo el archivo de configuración con la información actualizada
    configuracion = open('/var/lib/apio/bateria.conf', 'w')
    configuracion.writelines(lista_lineas)
    configuracion.close()
    return


###############################################################
#################### FUNCIÓN linea_cron() #####################
###############################################################
# Abre el archivo bateria.conf que contiene los datos de
# configuración que estoy buscando. Inicializa una variable para
# determinar cuándo encontrará el dato e inicia la secuencia
# de lectura de las líneas del archivo bateria.conf hasta
# que localice la línea que identifica el código que hay que
# cargar en el CRON.
# Esa línea se marca con @9 en la línea inmediatamente superior.
# Se leen todas las líneas hasta la que tiene valor @9
# y en ese momento se lee una línea más (la que tiene el dato
# buscado) y se sale del bucle de búsqueda. Se devuelve un
# string que representa el código del CRON.

def linea_cron():
    configuracion = open('/var/lib/apio/bateria.conf', 'r')
    dato_encontrado = False
    for linea_conf in configuracion.readlines():
        if dato_encontrado is True:
            break
        elif linea_conf == '@09\n':
            dato_encontrado = True
    linea_cron = str(linea_conf)
    configuracion.close()
    return linea_cron