#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo tiene dos funciones: una es para escribir
# la línea en el crontab del usuario para activar el programa
# y la otra es para comentar la línea, es decir, para desactivar
# el programa.
# Hay una tercera función, complementaria de las dos anteriores
# para detectar si en el crontab ya existe la línea, antes de
# hacer otra cosa (para no escribirla dos veces)

import subprocess
from crontab import CronTab

from funciones_02_auxiliares.bateria_conf import linea_cron


###############################################################
############### FUNCIÓN existe_job() ##########################
###############################################################
# Busca la palabra 'clave' en los jobs del crontab del usuario
# y devuelve 0 o 1 si no existe / existe.
# La palabra 'clave' es la que me permite determinar si existe
# o no un job relativo al programa apio.
# Por otro lado, en caso de existir el job, determina si está
# habilitado o no y devuelve 0 o 1 si no está habilitado / está
# habilitado.
# Por último, devuelve la suma de existe + habilitado. La
# interpretación del resultado es: 0 = no existe; 1 = existe pero
# no está habilitado; 2 = existe y está habilitado
# Esta función es como la anterior 'existe_linea()', pero
# más completa.

def existe_job():

    # Inicializo variables: "existe" me dice si el job de apio
    # ya existe en el crontab del usuario. "habilitado" me dice si el
    # job de apio, de existir, si está habilitado o no. "clave" es la
    # variable con la que busco a "apio" en el crontab. "clave" lo formo
    # a partir del texto que obtengo con la función linea_cron(), pero
    # sólo tomando un trozo de la cadena de texto completa.
    existe = 0
    habilitado = 0
    # Clave para buscar el código del programa en el CRON
    texto_clave = linea_cron()
    clave = texto_clave[len(texto_clave) - 60:len(texto_clave) - 1]

    # Averiguo quien es el usuario y leo su crontab
    usuario = subprocess.check_output(['whoami'])
    usuario = str(usuario, 'utf-8')
    usuario = usuario[:-1]
    cron_usuario = CronTab(user=usuario)

    # Recorro job a job todo el cron buscando el job de apio
    for job in cron_usuario:
        # Debo convertir el job a texto para poder escanear el contenido
        texto = str(job)
        if clave in texto:
            existe = 1
            if texto[0] == '#':
                habilitado = 0
            else:
                habilitado = 1
        else:
            pass
    return (existe + habilitado)
    # Devuelvo el estado del job después de ejecutada la función


###############################################################
############### FUNCIÓN arranca_apio(onoff) ###################
###############################################################
# Esta función recibe la orden de arrancar o parar el programa
# "apio". El programa "apio" se lanza desde el cron, luego el
# arranque o paro de "apio" se realiza escribiendo en el crontab

def arranca_apio(onoff):

    # El parámetro 'onoff' me dice lo que tengo que hacer (0 parar el job
    # y 1 arrancar el job). Me aseguro que sea un entero.
    onoff = int(onoff)
    # Inicio variables:
    # 'estadojob' me dice si existe el job y si está habilitado.
    # 'usuario' es el usuario que está ejecutando apio.
    # 'clave' es la palabra que me permite identificar el job apio.
    # 'comando' es el comando que ejecuta apio.
    estadojob = existe_job()
    usuario = subprocess.check_output(['whoami'])
    usuario = str(usuario, 'utf-8')
    usuario = usuario[:-1]
    # Clave para buscar el código del programa en el CRON
    texto_clave = linea_cron()
    clave = texto_clave[len(texto_clave) - 60:len(texto_clave) - 1]
    # Texto con el código de CRON para lanzar Apio
    comando = linea_cron()

    # El algoritmo de la función se basa en analizar los casos en los
    # que hay que hacer algo, e ignorar los casos en los que no hay
    # que hacer nada. Los casos en los que hay que hacer algo son:
    # 1) Que nos pidan desactivar "apio" y "apio" existe en el crontab
    # y está habilitado (si existe pero está deshabilitado no habría
    # que hacer nada). En este caso hay que localizar y deshabilitar
    # "apio" en el crontab.
    # 2) Que nos pidan activar "apio" y "apio" no existe en el crontab.
    # En este caso hay que escribir el job de "apio" en el crontab.
    # 3) Que nos pidan activar "apio" y "apio" existe en el crontab,
    # pero está deshabilitado (si existe y está habilitado no hay que
    # hacer nada). En este caso, hay que localizar y habilitar el job
    # "apio" en el crontab.

    # Primer caso
    if onoff == 0 and estadojob == 2:
        # Leo el crontab del usuario:
        cron_usuario = CronTab(user=usuario)
        # Busco el job que tiene a apio. Recorro job a job buscando la
        # palabra clave
        for job_apio in cron_usuario:
            # Para buscar la palabra clave necesito convertir el job en string
            texto = str(job_apio)
            # Si el job tiene la palabra clave, termino.
            if clave in texto:
                break
            else:
                pass
        # En este punto tengo cargado el job en 'job_apio'. Lo deshabilito:
        job_apio.enable(False)
        cron_usuario.write()

    # Segundo caso
    elif onoff == 1 and estadojob == 0:
        # Abro el crontab del usuario
        cron_usuario = CronTab(user=usuario)
        # Creo el nuevo job
        job_apio = cron_usuario.new(comando)
        job_apio.minute.every(5)
        cron_usuario.write()

    # Tercer caso
    elif onoff == 1 and estadojob == 1:
        # Leo el crontab del usuario
        cron_usuario = CronTab(user=usuario)
        # Busco el job que tiene a apio. Recorro job a job buscando la
        # palabra clave
        for job_apio in cron_usuario:
            # Para buscar la palabra clave necesito convertir el job en string
            texto = str(job_apio)
            # Si el job tiene la palabra clave, termino.
            if clave in texto:
                break
            else:
                pass
        # En este punto tengo cargado el job en 'job_apio'. Lo habilito:
        job_apio.enable()
        cron_usuario.write()

    # Fin de la función
    else:
        pass
    return


###############################################################
############### FUNCIÓN onoff_apio() ##########################
###############################################################
# Cuando esta función se invoca, comprueba si "apio" está activo
# en el crontab y le cambia es estado. Es decir, si apio está
# activo, lo desactiva y viceversa. En otras palabras, es un
# conmutador del estado de "apio".
# El programa "apio" se lanza desde el cron, luego el arranque
# o paro de "apio" se realiza escribiendo en el crontab

def onoff_apio():

    # Inicio variables:
    # 'estadojob' me dice si existe el job y si está habilitado.
    # 'usuario' es el usuario que está ejecutando apio.
    # 'clave' es la palabra que me permite identificar el job apio.
    # 'comando' es el comando que ejecuta apio.
    estadojob = existe_job()
    usuario = subprocess.check_output(['whoami'])
    usuario = str(usuario, 'utf-8')
    usuario = usuario[:-1]
    # Clave para buscar el código del programa en el CRON
    texto_clave = linea_cron()
    clave = texto_clave[len(texto_clave) - 60:len(texto_clave) - 1]
    # Texto con el código de CRON para lanzar Apio
    comando = linea_cron()

# El algoritmo de la función se basa en analizar cuál es el estado
# de "apio" y tomar decisión sobre lo que hay que hacer. El estado
# de "apio" lo conocemos gracias a la función "existe_job()" que
# devuelve 0 = si el job de "apio" no existe; 1 = si el job "apio" existe
# pero no está habilitado y 2 = si el job "apio" existe y está habilitado
# En los casos 0 y 1 hay que activar el job, pero de formas distintas
# (en el caso 0 hay que crear el job y en el caso 1 hay que activiarlo).
# En el caso 2 hay que desactivar el job. Por lo tanto, el algoritmo
# valora tres casos:
#     0) Que el job no exista en el crontab: En ese caso hay que escribir
#     el job "apio" en el crontab y dejarlo activado.
#     1) Que el job exista pero no está habilitado: En ese caso hay que
#     localizar y habilitar el job en el crontab.
#     2) Que el job exista y está habilitado: En ese caso hay que
#     localizar y deshabilitar el job en el crontab.

    # Caso 0
    if estadojob == 0:
        # Abro el crontab del usuario
        cron_usuario = CronTab(user=usuario)
        # Creo el nuevo job
        job_apio = cron_usuario.new(comando)
        job_apio.minute.every(5)
        cron_usuario.write()

    # Caso 1
    elif estadojob == 1:
        # Leo el crontab del usuario
        cron_usuario = CronTab(user=usuario)
        # Busco el job que tiene a apio. Recorro job a job buscando la
        # palabra clave
        for job_apio in cron_usuario:
            # Para buscar la palabra clave necesito convertir el job en string
            texto = str(job_apio)
            # Si el job tiene la palabra clave, termino.
            if clave in texto:
                break
            else:
                pass
        # En este punto tengo cargado el job en 'job_apio'. Lo habilito:
        job_apio.enable()
        cron_usuario.write()

    # Caso 2
    elif estadojob == 2:
        # Leo el crontab del usuario:
        cron_usuario = CronTab(user=usuario)
        # Busco el job que tiene a apio. Recorro job a job buscando la
        # palabra clave
        for job_apio in cron_usuario:
            # Para buscar la palabra clave necesito convertir el job en string
            texto = str(job_apio)
            # Si el job tiene la palabra clave, termino.
            if clave in texto:
                break
            else:
                pass
        # En este punto tengo cargado el job en 'job_apio'. Lo deshabilito:
        job_apio.enable(False)
        cron_usuario.write()

    # Fin de la función
    else:
        pass
    return