#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo utiliza acpi para varias funciones que muestran
# información del estado de la batería
# Importo el módulo commands (estándar) para ejecutar el
# comando acpi, que me da información de la batería
import subprocess


###############################################################
#################### FUNCIÓN presente() #######################
###############################################################
# Ejecuta el comando acpi -b para ver si existe una batería conectada.
# acpi -b no devuelve nada de texto si la batería no está conectada,
# esta es la manera en que la función detecta la presencia de la
# batería. Si la batería está presente devuelve 1, si no, devuelve 0


def presente():
    texto_acpi = subprocess.check_output(['acpi', '-b'])
    texto_acpi = str(texto_acpi, 'utf-8')
    if texto_acpi:
        bateria_presente = 1
    else:
        bateria_presente = 0
    return bateria_presente


###############################################################
############### FUNCIÓN carga() ###############################
###############################################################
# Ejecuta el comando acpi para tomar el dato del nivel de carga
# El nivel de carga termina en "%", por ejemplo "25%"
# Recorre todo el string de texto buscando el símbolo "%"
# Cuando llega al caracter "%", el caracter inmediatamente
# anterior son las unidades
# El segundo caracter serán las decenas, pero si existen
# Si el nivel es <10 el segundo caracter es un espacio
# En ese caso decenas = 0
# Con el mismo razonamiento se determinan las centenas
# Por último, calcula en nivel de carga que es lo que devuelve
# la función


def carga():
    texto_acpi = subprocess.check_output(['acpi'])
    texto_acpi = str(texto_acpi, 'utf-8')
    a = 0
    b = texto_acpi[a]
    while b != "%":
        a = a + 1
        b = texto_acpi[a]
    unidades = int(texto_acpi[a - 1])
    if texto_acpi[a - 2] != " ":
        decenas = int(texto_acpi[a - 2])
        if texto_acpi[a - 3] != " ":
            centenas = int(texto_acpi[a - 3])
        else:
            centenas = 0
    else:
        decenas = 0
        centenas = 0
    carga = (centenas * 100) + (decenas * 10) + unidades
    return carga


###############################################################
############### FUNCIÓN enchufado() ###########################
###############################################################
# Ejecuta el comando acpi -a para ver el estado del adaptador
# El estado del adaptador se muestra como "on-line" u "off-line"
# por lo que busca ambas secuencias de texto con el método find
# que es un método que se puede usar con los string. Si el texto
# buscado existe, devuelve un entero con la posición del primer
# caracter buscado. Si no existe, devuelve -1. Sabiendo esto:
# Si existe 'on-line' -> el cargador está enchufado -> devuelve 1
# Si existe 'off-line' -> el cargador está desenchufado -> devuelve 0
# En cualquier otro caso, devuelve 2


def enchufada():
    texto_acpi = subprocess.check_output(['acpi', '-a'])
    texto_acpi = str(texto_acpi, 'utf-8')
    if texto_acpi.find('on-line') != -1:
        enchufada = 1
    elif texto_acpi.find('off-line') != -1:
        enchufada = 0
    else:
        enchufada = 2
    return enchufada


###############################################################
############### FUNCIÓN temperatura() #########################
###############################################################
# Ejecuta el comando acpi -t para ver la temperatura en grados
# celsius y la carga a un string
# Busca en el string los símbolos "d" y ",", porque acpi -t
# muestra la temperatura de forma tal que "[...] ok, 65.0 degrees C"
# Captura el dato entre ambos símbolos y lo convierte en float
# Devuelve el float, que es la temperatura en Celsius
# Antes de hacer nada de eso, comprueba que el string con la
# información de acpi -t no esté vacío, ya que si la batería no
# tuviese sensor de temperatura (equipos antiguos o gama baja)
# el comando acpi -t no da error pero no devuelve texto alguno
# por pantalla


def temperatura():
    texto_acpi = subprocess.check_output(['acpi', '-t'])
    texto_acpi = str(texto_acpi, 'utf-8')
    if texto_acpi == "":
        temperatura = "Sin dato"
    else:
        a = 0
        b = texto_acpi[a]
        while b != "d":
            a = a + 1
            b = texto_acpi[a]
        fin = a - 1
        while b != ",":
            a = a - 1
            b = texto_acpi[a]
        inicio = a + 2
        temperatura = float(texto_acpi[inicio:fin])
    return temperatura


###############################################################
############### FUNCIÓN temperaturaf() #########################
###############################################################
# Ejecuta el comando acpi -t para ver la temperatura en grados
# fahrenheit y la carga a un string
# Busca en el string los símbolos "d" y ",", porque acpi -tf
# muestra la temperatura de forma tal que "[...] ok, 129.2 degrees F"
# Captura el dato entre ambos símbolos y lo convierte en float
# Devuelve el float, que es la temperatura en Fahrenheit


def temperaturaf():
    texto_acpi = subprocess.check_output(['acpi', '-tf'])
    texto_acpi = str(texto_acpi, 'utf-8')
    a = 0
    b = texto_acpi[a]
    while b != "d":
        a = a + 1
        b = texto_acpi[a]
    fin = a - 1
    while b != ",":
        a = a - 1
        b = texto_acpi[a]
    inicio = a + 2
    temperaturaf = float(texto_acpi[inicio:fin])
    return temperaturaf


###############################################################
############### FUNCIÓN temperaturak() #########################
###############################################################
# Ejecuta el comando acpi -t para ver la temperatura en grados
# kelvin y la carga a un string
# Busca en el string los símbolos "v" y ",", porque acpi -tk
# muestra la temperatura de forma tal que "[...] ok, 322.1 kelvin"
# Captura el dato entre ambos símbolos y lo convierte en float
# Devuelve el float, que es la temperatura en Kelvin


def temperaturak():
    texto_acpi = subprocess.check_output(['acpi', '-tk'])
    texto_acpi = str(texto_acpi, 'utf-8')
    a = 0
    b = texto_acpi[a]
    while b != "v":
        a = a + 1
        b = texto_acpi[a]
    fin = a - 4
    while b != ",":
        a = a - 1
        b = texto_acpi[a]
    inicio = a + 2
    temperaturak = float(texto_acpi[inicio:fin])
    return temperaturak