#! /usr/bin/python3
# -*- coding: utf-8 -*-
# Este módulo guarda datos en bateria.data en formato
# csv, para después poder analizar con alguna hoja de
# cálculo. Guarda información tabulada del nivel de
# carga, el estado del cargador y fecha y hora de la
# muestra
# Necesita el módulo time para capturar fecha y hora
# Necesita el módulo tkFileDialog para exportar los datos
from csv import *
import time
from tkinter import filedialog
from funciones_02_auxiliares.bateria_conf import nivel_minimo, \
    nivel_minimo_minimo


###############################################################
############### FUNCIÓN nuevo_registro() ######################
###############################################################
# Recibe los siguientes datos en el siguiente orden:
# nivel de carga, temperatura de la batería y número de veces
# que se ejecutó el programa durante el ciclo de carga/descarga.
# Con estos datos, compone una tupla a la que añade fecha y hora y
# formatea como datos separados por ";" y cierra la línea con \n
# Por último los añade como un nuevo registro al final del
# archivo bateria.data
# NOTA: los datos que se suben al archivo bateria.data deben
# ser string, luego hay que convertirlos en string (str) antes
# de crear el registro

def nuevo_registro(carga, temperatura, numero_ejecuciones_programa):
    fecha = time.strftime("%d/%m/%Y")
    hora = time.strftime("%H:%M:%S")
    carga = str(carga)
    temperatura = str(temperatura)
    numero_ejecuciones_programa = str(numero_ejecuciones_programa)
# Compone y formatea en nuevo registro
    registro_nuevo = (fecha, ';', hora, ';', carga, ';',
    temperatura, ';', numero_ejecuciones_programa, '\n')
# Carga el nuevo registro en bateria.data
    registro = open('/var/lib/apio/bateria.data', 'a')
    registro.writelines(registro_nuevo)
    registro.close()
    return


###############################################################
############### FUNCIÓN exportar() ############################
###############################################################
# Toma el archivo bateria.data y lo exporta donde el usuario
# quiera pero como archivo *csv

def exportar():
# Primero abre una ventana de diálogo (para eso necesité el módulo
# "tkFileDialog") y cargo en la variable "archivo_exportado" el nombre
# del archivo final.
# Luego abro el archivo "bateria.data" y el archivo final y voy leyendo
# línea a línea "bateria.data" y escribiendo línea a línea en el
# archivo destino.
    archivo_exportado = filedialog.asksaveasfilename(initialdir="/home",
        defaultextension=".csv",
        filetypes=[('CSV', ".csv"), ('TXT', ".txt")])
    if not archivo_exportado:
        pass
    else:
        origen = open('/var/lib/apio/bateria.data', 'r')
        destino = open(archivo_exportado, 'w')
        for linea in origen:
            destino.writelines(linea)
        origen.close()
        destino.close()
    return


###############################################################
############### FUNCIÓN evolucion_autonomia() #################
###############################################################
# Toma los valores de bateria.data relativos al número de veces
# que se ejecuta apio en cada ciclo de carga y descarga, pero sólo
# se queda con los que corresponden a los periodos de descarga de
# la batería y los almacena en una lista. Por lo tanto, la lista
# contiene una sucesión de números que representan la "autonomía"
# de la batería y cómo evoluciona esta "autonomía" a lo largo del
# tiempo.

def evolucion_autonomia():
    # Abro el archivo de datos en modo lectura
    datos = open('/var/lib/apio/bateria.data', 'r')
    # Convierto los datos que acabo de leer en csv separados por ";"
    datos_csv = reader(datos, delimiter=';')
    # Creo una lista de datos vacía
    lista_datos = []
    # Leo registro por registro el contenido del csv. Para ello tengo
    # que cargar el módulo csv
    for fila in datos_csv:
        # Ignoro la primera fila, que es la cabecera de datos del csv
        if fila[0] == "FECHA":
            pass
        # En el campo 2 está el nivel de carga. Compruebo que es
        # un valor parecido al valor mínimo con el que está
        # configurado apio. Necesito la función nivel_minimo() del
        # módulo de funciones bateria_conf. Chequeo que el valor esté
        # en más-menos 3 respecto al nivel mínimo de carga
        elif int(fila[2]) > nivel_minimo() - 3 \
        and int(fila[2]) < nivel_minimo() + 3:
            # Para los casos válidos, cargo el valor del número de
            # ejecuciones de programa en la lista. Ese número está
            # en el campo 4 de cada registro del csv.
            lista_datos.append(int(fila[4]))
        else:
            pass
    datos.close()
    return(lista_datos)


###############################################################
############### FUNCIÓN regresion() ###########################
###############################################################
# Recibe valores de una lista y calcula la regresión lineal,
# es decir, la línea de tendencia que siguen los puntos que
# determina la lista. Devuelve los valores "m" y "n" de la
# ecuación de la recta que determina la tendencia: y=mx+n
# El cálculo está sacado de la wikipedia y otras webs que
# desglosan el cálculo

def regresion(evolucion_autonomia):
    # Separo las coordenadas en dos listas, una para los valores
    # de x (abcisas) y otra para los valores de y (ordendadas)
    ordenadas = []
    abcisas = []
    x = 0
    for dato in evolucion_autonomia:
        abcisas.append(x)
        ordenadas.append(dato)
        x = x + 1

    # Comienzo el cálculo de "m" y "n"
    # Factor "a"
    indice = 0
    sumaproducto = 0
    while indice < len(ordenadas):
        sumaproducto = sumaproducto + (ordenadas[indice] * abcisas[indice])
        indice = indice + 1
    a = len(ordenadas) * sumaproducto
    # Factor "b"
    sumaordenadas = 0
    sumaabcisas = 0
    for dato in ordenadas:
        sumaordenadas = sumaordenadas + dato
    for dato in abcisas:
        sumaabcisas = sumaabcisas + dato
    b = sumaordenadas * sumaabcisas
    # Factor "c"
    sumacuadrados = 0
    for x in abcisas:
        sumacuadrados = sumacuadrados + (x * x)
    c = len(abcisas) * sumacuadrados
    # Factor "d"
    d = sumaabcisas * sumaabcisas
    # PENDIENTE "m"
    m = (a - b) / (c - d)
    # Factor "e"
    e = sumaordenadas
    # Factor "f"
    f = m * sumaabcisas
    # CORTE CON Y "n"
    n = (e - f) / len(ordenadas)
    return(m, n)


###############################################################
############### FUNCIÓN autonomia_inicial() ###################
###############################################################
# Toma el número de ciclos de la primera descarga profunda y
# lo considera la autonomía inicial de la batería

def autonomia_inicial():
    # Abro el archivo de datos en modo lectura
    datos = open('/var/lib/apio/bateria.data', 'r')
    # Convierto los datos que acabo de leer en csv separados por ";"
    datos_csv = reader(datos, delimiter=';')
    # Leo registro por registro el contenido del csv. Para ello tengo
    # que cargar el módulo csv
    for fila in datos_csv:
        # Ignoro la primera fila, que es la cabecera de datos del csv
        if fila[0] == "FECHA":
            pass
        # En el campo 2 está el nivel de carga. Compruebo que es
        # un valor parecido al valor de descarga profunda con el que está
        # configurado apio. Necesito la función nivel_minimo_minimo() del
        # módulo de funciones bateria_conf. Chequeo que el valor esté
        # en más 3 respecto al nivel de descarga profunda
        elif int(fila[2]) <= nivel_minimo_minimo():
            # Si localizo el caso, cargo el valor del número de
            # ejecuciones de programa en la lista. Ese número está
            # en el campo 4 de cada registro del csv. Y termino
            autonomia = (int(fila[4]))
            break
        else:
            pass
    datos.close()
    return(autonomia)
