#! /usr/bin/python3
# -*- coding: utf-8 -*-

#########################################################################
#                                                                       #
#   ALGORITMO PRINCIPAL DEL PROGRAMA DE CARGA DE BATERÍA DEL PORTÁTIL   #
#                                                                       #
#########################################################################
# Este programa se basa en lo siguiente: Se trata de gestionar ciclos   #
# de carga y descarga de la batería. Para ello se define una variable   #
# principal que se llama "estado_ciclo" y que indica si la batería      #
# está en un ciclo de carga o de descarga. Esta variable es el director #
# de orquesta del programa. Por ello, es necesario guardarla en un      #
# archivo de configuración (bateria.conf) y actualizarla cada vez       #
# que el programa se ejecuta. Y a eso se dedica el programa, a cargar   #
# el estado del ciclo del archivo de configuración y verificar si toca  #
# actualizarlo. En paralelo, se hacen otras gestiones tendentes a que   #
# el programa de un mejor servicio (usar el hardware para el control    #
# del cargador, llevar un histórico del uso de la batería en un archivo #
# de datos, etc.)                                                       #
#########################################################################

# Módulos que importa el programa
# Módulos estándar o propios de Python
from tkinter import messagebox
import sys

# Módulos propios. Contienen funciones
import funciones_01_principales.gestion_descarga_profunda_con_controlador \
    as descarga_profunda_con
import funciones_01_principales.gestion_carga_con_controlador \
    as carga_normal_con
import funciones_01_principales.gestion_descarga_con_controlador \
    as descarga_normal_con
import funciones_01_principales.gestion_descarga_profunda_sin_controlador \
    as descarga_profunda_sin
import funciones_01_principales.gestion_carga_sin_controlador \
    as carga_normal_sin
import funciones_01_principales.gestion_descarga_sin_controlador \
    as descarga_normal_sin
import funciones_02_auxiliares.bateria_acpi as bateria
import funciones_02_auxiliares.bateria_conf as configuracion
import funciones_02_auxiliares.bateria_data as datos
import funciones_02_auxiliares.control_cargador as control_cargador


# Las variables principales para la gestión de la batería
# son las relativas al nivel de carga de la batería.
# Defino tres, una para el nivel máximo de carga, otra
# para el nivel mínimo de carga normal y la tercera para
# el nivel de descarga profunda.
# Por último, defino una variable para cada cuántas
# descargas normales tocaría una descarga profunda.
# Todos los datos los obtengo del archivo de configuración
carga_max = configuracion.nivel_maximo()
carga_min = configuracion.nivel_minimo()
carga_min_min = configuracion.nivel_minimo_minimo()
ciclos_descarga_profunda = configuracion.frecuencia_descarga_profunda()


# En primer lugar, se cargan los datos que definen cómo nos
# habíamos quedado en la anterior ejecución del programa.
# El significado de las variables es:
# "descarga_profunda" igual a "1", estamos en descarga
# profunda; igual a "0", no estamos en descarga profunda
# (estamos en ciclos de carga/descarga normales).
# "estado_ciclo" igual a "1", estamos en un ciclo de carga;
# igual a "0", estamos en un ciclo de descarga normal.
# "numero_ciclos" es el número de ciclos completos de la
# batería. Por ciclo completo se entiende un ciclo completo
# de carga o un ciclo completo de descarga
# "numero_ejecuciones_programa" es el número de veces que se
# ejecutó este programa durante el ciclo en curso
descarga_profunda = int(configuracion.descarga_profunda())
estado_ciclo = int(configuracion.estado_ciclo())
numero_ciclos = int(configuracion.ciclos())
numero_ejecuciones_programa = int(configuracion.ejecuciones_programa())

# El programa empieza valorando la información que obtiene del
# archivo de configuración, y que refleja la situación en que
# quedó la batería la última vez que se ejecutó el programa.
# Distingue dos casos: que se disponga, o no, del hardware para
# controlar el cargador. Si se dispone del hardware (en adelante
# "control del cargador") se puede invocar a tres funciones,
# según estemos en un ciclo de carga, de descarga o de descarga
# profunda. Estas tres funciones valoran la situación de carga
# actual y actúan sobre el control del cargador si fuese necesario.
# Si no se dispone del control del cargador, se invocan otras tres
# funciones análogas a las anteriores pero que le piden al usuario,
# por medio de ventanas emergentes, que enchufe o desenchufe el
# cargador a mano.
# En cualquier caso, todas estas funciones devuelven el mismo
# resultado: si toca o no toca cambiar el estado de ciclo. Esa
# información se almacena en un bit que se llama "cambio_ciclo".
# En resumen, el programa empieza valorando cuatro casos.
# Pero antes que nada, se comprueba que la batería exista, que el
# ordenador tenga batería:

# Antes que nada: ¿hay batería? Consulto la función "presente"
# del módulo "bateria_acpi". Esta función devuelve 1 cuando hay
# batería. Si no hay batería, "rompemos" la ejecución del programa.
# Para romper la ejecución del programa uso sys.exit()
if bateria.presente() == 1:
    pass
else:
    messagebox.showerror("Apio informa",
    "No detecta una batería conectada al ordenador. Mejor apaga apio.")
    sys.exit()

# Primer caso: Estamos en descarga profunda y el control del cargador
# está conectado.
if descarga_profunda == 1 and control_cargador.conectado() == 1:
    cambio_ciclo = descarga_profunda_con. \
    gestion_descarga_profunda_con_controlador(carga_min_min)

# Segundo caso: Estamos en ciclo de carga normal y el control del
# cargador está conectado.
elif estado_ciclo == 1 and control_cargador.conectado() == 1:
    cambio_ciclo = carga_normal_con. \
    gestion_carga_con_controlador(carga_max)

# Tercer caso: Estamos en ciclo de descarga normal y el control del
# cargador está conectado.
elif estado_ciclo == 0 and control_cargador.conectado() == 1:
    cambio_ciclo = descarga_normal_con. \
    gestion_descarga_con_controlador(carga_min)

# Cuarto caso: Estamos en descarga profunda y el control del cargador
# no está conectado
elif descarga_profunda == 1 and control_cargador.conectado() == 0:
    cambio_ciclo = descarga_profunda_sin. \
    gestion_descarga_profunda_sin_controlador(carga_min_min)

# Quinto caso: Estamos en ciclo de carga normal y el control del
# cargador no está conectado.
elif estado_ciclo == 1 and control_cargador.conectado() == 0:
    cambio_ciclo = carga_normal_sin. \
    gestion_carga_sin_controlador(carga_max)

# Sexto caso: Estamos en ciclo de descarga normal y el control del
# cargador no está conectado.
elif estado_ciclo == 0 and control_cargador.conectado() == 0:
    cambio_ciclo = descarga_normal_sin. \
    gestion_descarga_sin_controlador(carga_min)

# Si no estamos en ninguno de estos cuatro casos, no hacemos nada
# Este supuesto no tiene sentido, por lo que se lanza un mensaje
# de aviso al usuario:
else:
    cambio_ciclo = 0
    messagebox.showerror("Apio informa",
    "Apio ha detectado un estado extraño de funcionamiento. Consulta la ayuda")

# En este punto, el programa ha hecho todo lo posible por dejar la batería
# en la mejor situación respecto al cargador. Además, el programa ha valorado
# si corresponde realizar un cambio de ciclo o no. Ahora toca actuar en lo
# que se refiere al cambio de ciclo.
# Básicamente, cuando se produce un cambio de ciclo hay que actualizar la
# información de los archivos de configuración y de datos. Hay cinco puntos
# que actualizar si procede:
# 1) El número de ejecuciones de programa.
# 2) El número de ciclos (completos) de carga/descarga.
# 3) El bit de estado de ciclo.
# 4) El bit de descarga profunda.
# 5) El registro en el archivo de datos.
# Los 4 primeros se guardan en el archivo de configuración y el quinto se
# guarda en el archivo de datos

# Primero: Qué hacer si toca cambiar de ciclo
if cambio_ciclo == 1:
    # 1) El programa se ejecutó, así que se incrementa el número de ejecuciones
    # Pero, como toca un cambio de ciclo, en el archivo de configuración hay
    # que resetear el contador de ejecuciones de programa
    numero_ejecuciones_programa = numero_ejecuciones_programa + 1
    configuracion.actualiza_ejecuciones_programa(0)
    # 2) Si llegamos a un cambio de ciclo, toca incrementar en uno el número de
    # ciclos completos y actualizar el archivo de configuración
    numero_ciclos = numero_ciclos + 1
    configuracion.actualiza_ciclos(numero_ciclos)
    # 3) Cuando cambia el ciclo, hay que actualizar el bit de estado de ciclo.
    # Si veníamos de carga hay que pasar a descarga y vicecersa. Y se actualiza
    # el cambio en el archivo de configuración
    if estado_ciclo == 0:
        estado_ciclo = 1
    elif estado_ciclo == 1:
        estado_ciclo = 0
    else:
        pass
    configuracion.actualiza_estado_ciclo(estado_ciclo)
    # 4) Si cambia el ciclo y viniésemos de una descarga profunda,
    # significaría que la descarga profunda se habría acabado; Pero si no
    # vieniésemos de una descarga profunda, habría que verificar si en el
    # próximo ciclo toca la descarga profunda. En este segundo caso, como
    # no queda bien que la descarga profunda sea justo después de una
    # descarga normal, valoro si toca descarga profunda sólo en el caso que
    # toque comenzar un ciclo de desgarga. Los ciclos de descarga pueden
    # seguir la secuencia de los pares (0, 2, 4, 6...) o de los impares (1,
    # 3, 5...) por lo que tengo que verificar el ciclo n y el ciclo n-1.
    # Hecho el análisis, hay que actualizar el bit de descarga profunda en
    # el archivo de configuración
    if descarga_profunda == 1:
        descarga_profunda = 0
    elif descarga_profunda == 0 and estado_ciclo == 0:
        if (numero_ciclos % ciclos_descarga_profunda) == 0:
            descarga_profunda = 1
        elif ((numero_ciclos - 1) % ciclos_descarga_profunda) == 0:
            descarga_profunda = 1
        else:
            descarga_profunda = 0
    else:
        pass
    configuracion.actualiza_descarga_profunda(descarga_profunda)
    # 5) Por último, si cambia el ciclo, hay que registrar valores en el
    # archivo de datos de seguimiento de la batería
    datos.nuevo_registro(bateria.carga(), bateria.temperatura(),
    numero_ejecuciones_programa)

# Segundo: Qué hacer si NO toca cambiar de ciclo
elif cambio_ciclo == 0:
    # 1) El programa se ejecutó, así que se incrementa el número de
    # ejecuciones. Y registramos el nuevo valor en el archivo de
    # configuración
    numero_ejecuciones_programa = numero_ejecuciones_programa + 1
    configuracion.actualiza_ejecuciones_programa(numero_ejecuciones_programa)
    # 2) El número de ciclos no cambia
    pass
    # 3) El bit de estado de ciclo no cambia
    pass
    # 4) El bit de descarga profunda no cambia
    pass
    # 5) No hay que añadir ningún registro nuevo al archivo de datos
    pass

# Cierro el bucle
else:
    pass

# Fin del programa

# Notas:
# El programa sólo contempla que el usuario le hace caso
# cuando opera manualmente sobre el cargador o cuando el control
# del cargador falla. Si el usuario ignora las indicaciones
# del programa, el programa no toma ninguna medida adicional.
# No obstante, el algoritmo no "rompe" si el usuario no interactúa,
# el programa sigue operativo y actúa razonablemente.
# Este software no tiene poderes de super vaca, pero es libre.
# Ningún ñu ni ningún pingüino fue dañado durante la programación
# de este software.