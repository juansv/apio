# apio-2.0

Apio software increases your laptop battery lifetime. Apio works by keeping your battery in a safe duty cycle which include calibration cycles from time to time.

If you have a wifi plug with Tasmota software in, Apio is able to switch on an off the plug, so you don't have to worry about connecting or disconnecting your laptop AC adapter.

But if you don't have one of those Tasmota compatible plugs, don't also have to worry, because Apio tells you when you have to connect or disconnect the AC adapter.

You can read about technical basis of apio [in this file](./doc/apio-2.0/technical_things.md)

### Installing "apio"

#### Dependencies

Install the python3 interpreter

`sudo apt-get install python3`

Install the third party modules `apio` needs

`sudo apt-get install python3-pip`

`python3 -m pip install python-crontab`

`python3 -m pip install notify2`

`python3 -m pip install requests`

`python3 -m pip install xdg`

`python3 -m pip install yaml`

These modules could be also available by your distro package manager.

Please, pay attention at [`python-crontab`](https://pypi.org/project/python-crontab/) module. If `apio` gives the error `unexpected keyword argument 'user'` it means you have installed `crontab` module instead `python-crontab`.

#### "apio" software

Clone the repository...

`sudo apt-get install git` (just only if you don't have git installed)

`git clone https://gitlab.com/juansv/apio.git`

...and run the installer script

`cd apio/`

`sudo sh apio-2.0_install.sh`

The installer is made for Debian GNU/linux distro. If your system has a different filesystem hierarchy, change lines 17 to 23 of `apio-2.0_install.sh` (look at the comment `DESTINATION DIRECTORIES`).

### Use of "apio"

Once you have just installed `apio`, you should:

1. Run `apio autoconfig`
2. If there is any **error** message, fix it.
3. Repeat steps 1 and 2 until you get no error messages.
4. If you have a Tasmota device, set up it in apio by running `apio config -ip <dirección_ip_dispositivo>` or `apio config -url <url_dispositivo>`. It depends on your device and how it is configured in your LAN.
5. Run `apio config -v` to check whatever pending issue.
6. Now and optionally, you can tune the duty and calibration cycles the way you like. Read the man or run `apio config -h`
7. Finally, do not forget to run `apio config -on` for schedulling `apio` in your user's `cron`.

Each user account in the GNU/linux system should repeat the previous steps to have `apio` running in their sessions. The second and on will be faster because the most of the configuration is common for all users.

### Technical

`apio` is written in python. You will need the python3 interpreter installed. Moreover, `apio` uses the following third party modules (not standard) you also have to install: `python-crontab`, `notify2`, `requests`, `xdg` and `yaml`. All this things were explained at [dependencies](#dependencies) chapter. Because of those third party modules `apio` needs Python >= 3.7

`apio` uses cron to schedule a job. As a result, you have to run `apio config -on` to start the job. And each user should do.

`apio` should be configured to work. But `apio` is able to self configure. It was explained at [use of apio](#use-of-apio) chapter. Config file is placed at `/var/lib/apio/apio.yaml`.

`apio` also save information about each duty cycle performance. This data base lets the owner to value how the battery ageing is. Data base file is placed at `/var/lib/apio/apio.csv`. This file could be exported by typing `apio config -e /target_path`.

`apio` can drive automatically over wifi plugs which use `Tasmota` software. That way, you simply plug the AC adapter on the wifi plug and let `apio` does everything.

`apio` rules the wifi plug through the `Tasmota` web interface. No `MQTT` network is needed.

First you have to set up your `Tasmota` plug into your LAN. Please, follow the manufacturer instructions.

Then the `Tasmota` device LAN address should be told to `apio`. To do that run:  
`apio config -ip <dirección_ip_dispositivo>`  
`apio config -url <url_dispositivo>`

It is not necessary to run both commands. Only one is needed to set up the `Tasmota` device in `apio`. It depends on how you have registered the device on your LAN.

#### About Tasmota

`Tasmota` is a software you can install over [many home automation devices](https://templates.blakadder.com/index.html). There are even a few manufacturers which sell their products with `Tasmota` [installed](https://templates.blakadder.com/preflashed.html).

`Tasmota` is not 100% free software due to wifi card firmware it needs. However, all the rest is free software. More info:

<https://tasmota.github.io/docs/>

#### About not Tasmota devices

`apio` drives the wifi plug by sending orders trough http REST API. This is very common in home automation. Therefore, it is very probably that `apio` works over plenty different wifi plugs, not only the `Tasmota` driven ones. `apio` author claims it works over `Tasmota` driven devices.

### Versions

#### Legacy (1.0 y 1.1)

`apio` born in 2018 (version 1.0) as free hardware and software project. The hardware side of the project was an electronic switch controlled by USB port. In that way, `apio` turned on and off the AC adapter. Version 1.0 had a simple GUI to manage `apio`.

Few time later version 1.1 was delivered. It was in 2018 too. It had a new feature called `apio-conf` to manage `apio` in the terminal.

In July 2019 `apio` documentation was widen with another electronic circuit for the USB switch. This circuit used a triac instead a relay, which made the USB switch faster and smaller. No changes were made on software, so version still was 1.1

#### 2.0.0

`apio` project was always burdened because of the hardware. There was no commercial device compatible with `apio` software, so an `apio` user should make their own USB switch.

Wifi plugs are commercial, what makes everything easier. This fact pushed `apio` to version 2.0. In order to keep the underlying free knowledge spirit, `apio` version 2.0 was developed to be compatible with wifi plugs driven by `Tasmota` software.

These are the main features of version 2.0:

- The code was completely re-written.
- It is made as text-based software, so terminal is needed. Version 2.0 is born without any GUI.
- It has a self configure tool, to make easy to use.
- Config file becomes a yaml file.
- Data base file is still an csv file, but data base fields changed.
- Cron job frequency is not always the same. Now, it depends on battery charge. It reduces the risk of automatic shutdown when calibration cycle is running.
- `apio` messages are sent as desktop notifications.
- USB switch control is no longer supported.

### Author and license

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.

