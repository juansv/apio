#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
#
# Este módulo gestiona un único job del cron, sin preocuparse del resto de
# jobs. Necesita python >= 3.5
# Info:
# https://code.tutsplus.com/es/tutorials/managing-cron-jobs-using-python--cms-28231
# https://pypi.org/project/python-crontab/
#
# This module manages one single cron job, independent of the rest of jobs
# in cron. It is needed python >= 3.5
# Info:
# https://code.tutsplus.com/es/tutorials/managing-cron-jobs-using-python--cms-28231
# https://pypi.org/project/python-crontab/
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# El módulo crontab necesita python >= 3.5
# crontab module needs python >= 3.5
from crontab import CronTab
# La clase recibe una tarea a ejecutar (jobcommand) y un "alias" para la
# tarea (jobID). Si en el cron no existe ninguna tarea con el alias,
# crea la tarea con la info recibida por jobcommand. Pero si ya existiese
# una tarea con ese alias, ignora jocommand, es decir, no modifica el cron
# en lo que se refiere a jobcommand o job ID. Sólo aplicará las modificaciones
# que definan los otros métodos de la clase (cambiar el tiempo, habilitar o
# deshabilitar, etc.). Por lo tanto, para cambiar el jobcommand registrado
# en el cron, primero hay que borrar la tarea y luego volver a crearla.
# Para borrar la tarea hay un método, para crearla se trataría de volver a
# instanciar la clase.
class OneJobCron:
    def __init__(self, jobID, jobcommand):
        self.usercron = CronTab(user=True)
        self.jobID = str(jobID)
        self.jobcommand = str(jobcommand)
        # Antes de crear el job, hay que verificar su existencia previa
        # en el cron
        exists = False
        for job in self.usercron:
            if job.comment == self.jobID:
                exists = True
                break
        if not exists:
            userjob = self.usercron.new(command=self.jobcommand, comment=self.jobID)
            userjob.minute.every(5)
            self.usercron.write()

    def set_every(self, minutes):
        minutes = int(minutes)
        for job in self.usercron:
            if job.comment == self.jobID:
                job.minute.every(minutes)
                if job.is_valid(): self.usercron.write()
                else: raise Exception("invalid cron sentence")
                break
        return()

    def toggle_enabling(self):
        for job in self.usercron:
            if job.comment == self.jobID:
                if job.is_enabled():
                    job.enable(False)
                else:
                    job.enable()
                if job.is_valid(): self.usercron.write()
                else: raise Exception("invalid cron sentence")
                break
        return()

    def set_enable(self):
        for job in self.usercron:
            if job.comment == self.jobID:
                job.enable()
                if job.is_valid(): self.usercron.write()
                else: raise Exception("invalid cron sentence")
                break
        return()

    def set_disable(self):
        for job in self.usercron:
            if job.comment == self.jobID:
                job.enable(False)
                if job.is_valid(): self.usercron.write()
                else: raise Exception("invalid cron sentence")
                break
        return()

    def job_enabled(self):
        status = False
        for job in self.usercron:
            if job.comment == self.jobID:
                status = job.is_enabled()
                break
        return(status)

    def delete_job(self):
        for job in self.usercron:
            if job.comment == self.jobID:
                self.usercron.remove(job)
                self.usercron.write()
                break
        return()

    def job_validity(self):
        validity = False
        for job in self.usercron:
            if job.comment == self.jobID:
                validity = job.is_valid()
                break
        return(validity)
