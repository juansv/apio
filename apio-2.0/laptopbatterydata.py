#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
#
# Este módulo está hecho leer los datos de la batería de un portátil en
# /sys/class/power_supply/BATX, donde "X" es el número que la batería tiene
# asignado. Devuelve los datos en un formato de variable coherente y
# manejable por un programa python, es decir, devuelve float, int, str, etc.
# según la naturaleza del dato.
# Para más info:
# https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power
# https://www.kernel.org/doc/html/latest/power/power_supply_class.html
#
# This module is made for laptop battery data reading. It reads on
# /sys/class/power_supply/BATX where "X" is the number assigned by the
# system. It returns all data in python coherent variables, I mean, it
# returns float, int, str, etc. depending on data nature.
# For further info:
# https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power
# https://www.kernel.org/doc/html/latest/power/power_supply_class.html
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

from pathlib import Path

# La clase se nombra con class seguido de un nombre en CamelCase
# El primer método de la clase debe ser __init__ seguido siempre
# de la variable self, que es necesario para luego invocar el objeto que
# estoy creando. Este primer método sirve para hacer la instancia
# de la clase.
class LaptopBatteryData:
    # La instancia sólo prepara las variables para usar en el resto de
    # métodos.
    # Necesita el nombre que recibe la batería en /sys/class/power_supply/
    # que normalmente es BAT0, BAT1, etc. Pero, a priori, ese dato no es
    # igual en todos los portátiles, por lo que es necesario indicarlo.
    # También se le puede entregar, opcionalmente, el submúltiplo de las
    # unidades de medida (mili, micro) que se usará con los valores numéricos.
    # Por defecto devuelve los datos sin submúltiplo, que es la opción None
    # También acepta "m" para "mili-" y "u" para "micro-".
    #
    # The instance of this class only prepares all variables necessaries for
    # each method.
    # It needs the name the battery has on /sys/class/power_supply/ which
    # normally is as BAT0, BAT1, etc. It is different in each laptop.
    # Optionally, the submultiple used for measures could be given. None means
    # no submultiple and methods affected return measures in the International
    # System. It is accepted "m" for "mili-" and "u" for "micro-" and nothing
    # else. None is the default.
    def __init__(self, batteryname, submultiple=None):
        # Primero se prepara el factor para los submúltiplos.
        # Si se entrega un factor distinto de None, "m" o "u", devuelve
        # una excepción.
        # Por defecto los valores registrados en /sys/class/power_supply/BATX
        # están en "micro-", luego para None se divide por un millón,
        # para "mili" de divide por mil y para "micro-" se deja como está.
        #
        # First, the variable "factor" is defined.
        # It is only accepted None, "m" or "u", otherwise an exception is
        # raised.
        # The default submultiple in /sys/class/power_supply/BATX is "micro-"
        # for all technical values. So, if None is given, it is necessary to
        # divide by one million. If "m" is given is necessary to divide by
        # one thousand and "u" means factor equals one.
        if submultiple not in [None, "m", "u"]:
            raise Exception("submultiple not right, only \"m\" or \"u\" accepted")
        if submultiple is None: self.factor = 1/1000000
        if submultiple == "m": self.factor = 1/1000
        if submultiple == "u": self.factor = 1

        # Construyo la ruta al directorio de la batería y verifico que existe
        # It is necessary create the path to battery and check the path exists
        if batteryname.startswith("/sys/class/power_supply/"):
            batteryname = batteryname.lstrip("/sys/class/power_supply/")
        self.commonpath = Path("/sys/class/power_supply")
        self.batterypath = Path(self.commonpath / batteryname)
        if self.batterypath.exists(): pass
        else: raise Exception("battery name does not exist")

        # Defino una propiedad (variable) por cada archivo de datos
        # disponible en el directorio /sys/class/power_supply/BATX
        #
        # Now, it creates one property (variable) for each  file in
        # /sys/class/power_supply/BATX

        ######################################################################
        # Propiedades con datos generales de la batería (solo lectura)
        # Properties about general battery data (read-only)
        self.manufacturer_file = Path(self.batterypath / "manufacturer")
        self.serialnumber_file = Path(self.batterypath / "serial_number")
        self.modelname_file = Path(self.batterypath / "model_name")
        self.batterytype_file = Path(self.batterypath / "type")
        self.batterytech_file = Path(self.batterypath / "technology")

        ######################################################################
        # Propiedades con valores de diseño (solo lectura)
        # Properties with design values (read-only)
        self.chargefulldesign_file = Path(self.batterypath / "charge_full_design")
        self.chargeemptydesign_file = Path(self.batterypath / "charge_empty_design")
        self.voltagemaxdesign_file = Path(self.batterypath / "voltage_max_design")
        self.voltagemindesign_file = Path(self.batterypath / "voltage_min_design")

        ######################################################################
        # Propiedades con los valores límite de la batería (solo lectura)
        # Properties with battery limit values (read-only)
        self.tempmax_file = Path(self.batterypath / "temp_max")
        self.tempmin_file = Path(self.batterypath / "temp_min")
        self.voltagemax_file = Path(self.batterypath / "voltage_max")
        self.voltagemin_file = Path(self.batterypath / "voltage_min")
        self.currentmax_file = Path(self.batterypath / "current_max")
        self.chargelimitmax_file = Path(self.batterypath / "charge_control_limit_max")
        self.chargetermcurrent_file = Path(self.batterypath / "charge_term_current")

        ######################################################################
        # Propiedades con la configuración de la batería (letura y escritura)
        # Properties with battery settings (read/write)
        self.chargetype_file = Path(self.batterypath / "charge_type")
        self.chargebehav_file = Path(self.batterypath / "charge_behaviour")
        self.tempalertmax_file = Path(self.batterypath / "temp_alert_max")
        self.tempalertmin_file = Path(self.batterypath / "temp_alert_min")
        self.capalertmax_file = Path(self.batterypath / "capacity_alert_max")
        self.capalertmin_file = Path(self.batterypath / "capacity_alert_min")
        self.chargelimit_file = Path(self.batterypath / "charge_control_limit")
        self.chargestartthreshold_file = Path(self.batterypath / "charge_control_start_threshold")
        self.chargeendthreshold_file = Path(self.batterypath / "charge_control_end_threshold")

        ######################################################################
        # Propiedades con medidas y contadores (valor actual, sólo lectura)
        # Properties with measurements and counters (present value, read only)
        self.chargepercentage_file = Path(self.batterypath / "capacity")
        self.chargeAh_file = Path(self.batterypath / "charge_now")
        self.current_file = Path(self.batterypath / "current_now")
        self.voltage_file = Path(self.batterypath / "voltage_now")
        self.currentavg_file = Path(self.batterypath / "current_avg")
        self.voltageavg_file = Path(self.batterypath / "voltage_avg")
        self.temperature_file = Path(self.batterypath / "temp")
        self.prechargecurrent_file = Path(self.batterypath / "precharge_current")
        self.chargefullactual_file = Path(self.batterypath / "charge_full")
        self.chargeemptyactual_file = Path(self.batterypath / "charge_empty")
        self.caperrormargin_file = Path(self.batterypath / "capacity_error_margin")
        self.cyclecount_file = Path(self.batterypath / "cycle_count")

        ######################################################################
        # Propiedades con descripción de estado actual (solo lectura)
        # Properties with present status description (read only)
        self.present_file = Path(self.batterypath / "present")
        self.status_file = Path(self.batterypath / "status")
        self.capacitylevel_file = Path(self.batterypath / "capacity_level")
        self.health_file = Path(self.batterypath / "health")

# Defino un método para extraer la información de cada propiedad
# Los métodos llevan el mismo orden que las propiedades de arriba
# Cada método tiene la información disponible en:
# <https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>
# que ilustra el dato de la batería contenido en él.
#
# Now, it creates one method for each properties to extract the info they have
# Methods are sorted in the same order than the properties above
# Each method has the info available in:
# <https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>
# This info explain the battery data linked to the method.

##############################################################################
# Métodos que devuelven los datos generales de la batería
# Methods that give back general battery data

# What:       /sys/class/power_supply/<supply_name>/manufacturer
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the name of the device manufacturer.
#         Access: Read
#         Valid values: Represented as string
    def manufacturer(self):
        try:
            with open(self.manufacturer_file, "r") as temp_file:
                manufacturer_name = temp_file.read()
            manufacturer_name = str(manufacturer_name[:-1])
            return(manufacturer_name)
        except(FileNotFoundError):
            raise FileNotFoundError("manufacturer property file not found")

# What:       /sys/class/power_supply/<supply_name>/serial_number
# Date:       January 2008
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the serial number of the device.
#         Access: Read
#         Valid values: Represented as string
    def serialnumber(self):
        try:
            with open(self.serialnumber_file, "r") as temp_file:
                serial_number = temp_file.read()
            serial_number = str(serial_number[:-1])
            return(serial_number)
        except(FileNotFoundError):
            raise FileNotFoundError("serial number property file not found")

# What:       /sys/class/power_supply/<supply_name>/model_name
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the name of the device model.
#         Access: Read
#         Valid values: Represented as string
    def modelname(self):
        try:
            with open(self.modelname_file, "r") as temp_file:
                model_name = temp_file.read()
            model_name = str(model_name[:-1])
            return(model_name)
        except(FileNotFoundError):
            raise FileNotFoundError("model name property file not found")

# What:       /sys/class/power_supply/<supply_name>/type
# Date:       May 2010
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Describes the main type of the supply.
#         Access: Read
#         Valid values: "Battery", "UPS", "Mains", "USB", "Wireless"
    def batterytype(self):
        try:
            with open(self.batterytype_file, "r") as temp_file:
                battery_type = temp_file.read()
            battery_type = str(battery_type[:-1])
            return(battery_type)
        except(FileNotFoundError):
            raise FileNotFoundError("battery type property file not found")

# What:       /sys/class/power_supply/<supply_name>/technology
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Describes the battery technology supported by the supply.
#         Access: Read
#         Valid values: "Unknown", "NiMH", "Li-ion", "Li-poly", "LiFe", "NiCd", "LiMn"
    def batterytech(self):
        try:
            with open(self.batterytech_file, "r") as temp_file:
                battery_tech = temp_file.read()
            battery_tech = str(battery_tech[:-1])
            return(battery_tech)
        except(FileNotFoundError):
            raise FileNotFoundError("technology property file not found")

##############################################################################
# Métodos que devuelven valores de diseño
# Methods that give back design values

# There is no info in
# <https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>
# Acording to https://www.kernel.org/doc/html/latest/power/power_supply_class.html
# all properties called "property_DESIGN" are design figures for maximal and
# minimal value as applicable.
    def chargefulldesign(self):
        try:
            with open(self.chargefulldesign_file, "r") as temp_file:
                charge_full_design = temp_file.read()
            charge_full_design = int(charge_full_design) * self.factor
            return(charge_full_design)
        except(FileNotFoundError):
            raise FileNotFoundError("full charge design property file not found")
    def chargeemptydesign(self):
        try:
            with open(self.chargeemptydesign_file, "r") as temp_file:
                charge_empty_design = temp_file.read()
            charge_empty_design = int(charge_empty_design) * self.factor
            return(charge_empty_design)
        except(FileNotFoundError):
            raise FileNotFoundError("empty charge design property file not found")
    def voltagemaxdesign(self):
        try:
            with open(self.voltagemaxdesign_file, "r") as temp_file:
                voltage_max_design = temp_file.read()
            voltage_max_design = int(voltage_max_design) * self.factor
            return(voltage_max_design)
        except(FileNotFoundError):
            raise FileNotFoundError("maximun voltage design property file not found")
    def voltagemindesign(self):
        try:
            with open(self.voltagemindesign_file, "r") as temp_file:
                voltage_min_design = temp_file.read()
            voltage_min_design = int(voltage_min_design) * self.factor
            return(voltage_min_design)
        except(FileNotFoundError):
            raise FileNotFoundError("minimun voltage design property file not found")

##########################################################################
# Métodos que devuelve los valores límite de la batería
# Methods that give back battery limiting values

# What:       /sys/class/power_supply/<supply_name>/temp_max
# Date:       July 2014
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the maximum allowed TBAT battery temperature for
#           charging.
#         USB:
#           Reports the maximum allowed supply temperature for operation.
#         Access: Read
#         Valid values: Represented in 1/10 Degrees Celsius
    def tempmax(self):
        try:
            with open(self.tempmax_file, "r") as temp_file:
                tempmax = temp_file.read()
            tempmax = int(tempmax)
            return(tempmax)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum allowed temperature property file not found")

# What:       /sys/class/power_supply/<supply_name>/temp_min
# Date:       July 2014
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the minimum allowed TBAT battery temperature for
#           charging.
#         USB:
#           Reports the minimum allowed supply temperature for operation.
#         Access: Read
#         Valid values: Represented in 1/10 Degrees Celsius
    def tempmin(self):
        try:
            with open(self.tempmin_file, "r") as temp_file:
                tempmin = temp_file.read()
            tempmin = int(tempmin)
            return(tempmin)
        except(FileNotFoundError):
            raise FileNotFoundError("minimum allowed temperature property file not found")

# What:       /sys/class/power_supply/<supply_name>/voltage_max,
# Date:       January 2008
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the maximum safe VBAT voltage permitted for the
#           battery, during charging.
#         USB:
#           Reports the maximum VBUS voltage the supply can support.
#         Access: Read
#         Valid values: Represented in microvolts
    def voltagemax(self):
        try:
            with open(self.voltagemax_file, "r") as temp_file:
                voltage_MAX = temp_file.read()
            voltage_MAX = int(voltage_MAX) * self.factor
            return(voltage_MAX)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum voltage property file not found")

# What:       /sys/class/power_supply/<supply_name>/voltage_min,
# Date:       January 2008
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the minimum safe VBAT voltage permitted for the
#           battery, during discharging.
#         USB:
#           Reports the minimum VBUS voltage the supply can support.
#         Access: Read
#         Valid values: Represented in microvolts
    def voltagemin(self):
        try:
            with open(self.voltagemin_file, "r") as temp_file:
                voltage_MIN = temp_file.read()
            voltage_MIN = int(voltage_MIN) * self.factor
            return(voltage_MIN)
        except(FileNotFoundError):
            raise FileNotFoundError("minimun voltage property file not found")

# What:       /sys/class/power_supply/<supply_name>/current_max
# Date:       October 2010
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the maximum IBAT current allowed into the battery.
#         USB:
#           Reports the maximum IBUS current the supply can support.
#         Access: Read
#         Valid values: Represented in microamps
    def currentmax(self):
        try:
            with open(self.currentmax_file, "r") as temp_file:
                current_MAX = temp_file.read()
            current_MAX = int(current_MAX) * self.factor
            return(current_MAX)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum current property file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_control_limit_max
# Date:       Oct 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Maximum legal value for the charge_control_limit property.
#         Access: Read
#         Valid values: Represented in microamps
    def chargelimitmax(self):
        try:
            with open(self.chargelimitmax_file, "r") as temp_file:
                charge_limit_MAX = temp_file.read()
            charge_limit_MAX = int(charge_limit_MAX) * self.factor
            return(charge_limit_MAX)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum charge control limit file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_term_current
# Date:       July 2014
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the charging current value which is used to determine
#         when the battery is considered full and charging should end.
#         Access: Read
#         Valid values: Represented in microamps
    def chargeendcurrent(self):
        try:
            with open(self.chargetermcurrent_file, "r") as temp_file:
                charge_term = temp_file.read()
            charge_term = int(charge_term) * self.factor
            return(charge_term)
        except(FileNotFoundError):
            raise FileNotFoundError("charging end current property file not found")

#############################################################
# Métodos que devuelven la configuración de la batería
# Methods that give back battery settings

# What:       /sys/class/power_supply/<supply_name>/charge_type
# Date:       July 2009
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Represents the type of charging currently being applied to the
#         battery. "Trickle", "Fast", and "Standard" all mean different
#         charging speeds. "Adaptive" means that the charger uses some
#         algorithm to adjust the charge rate dynamically, without
#         any user configuration required. "Custom" means that the charger
#         uses the charge_control_* properties as configuration for some
#         different algorithm. "Long Life" means the charger reduces its
#         charging rate in order to prolong the battery health. "Bypass"
#         means the charger bypasses the charging path around the
#         integrated converter allowing for a "smart" wall adaptor to
#         perform the power conversion externally.
#         Access: Read, Write
#         Valid values: "Unknown", "N/A", "Trickle", "Fast", "Standard",
#                     "Adaptive", "Custom", "Long Life", "Bypass"
    def chargetype(self):
        try:
            with open(self.chargetype_file, "r") as temp_file:
                charge_type = temp_file.read()
            charge_type = str(charge_type[:-1])
            return(charge_type)
        except(FileNotFoundError):
            raise FileNotFoundError("charge type property file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_behaviour
# Date:       November 2021
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Represents the charging behaviour.
#         Access: Read, Write
#         Valid values:
#             auto:            Charge normally, respect thresholds
#             inhibit-charge:  Do not charge while AC is attached
#             force-discharge: Force discharge while AC is attached
    def chargebehaviour(self):
        try:
            with open(self.chargebehav_file, "r") as temp_file:
                charge_behav = temp_file.read()
            charge_behav = str(charge_behav[:-1])
            return(charge_behav)
        except(FileNotFoundError):
            raise FileNotFoundError("charge behaviour property file not found")

# What:       /sys/class/power_supply/<supply_name>/temp_alert_max
# Date:       July 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Maximum TBAT temperature trip-wire value where the supply will
#           notify user-space of the event.
#         USB:
#           Maximum supply temperature trip-wire value where the supply
#           will notify user-space of the event.
#         This is normally used for the charging scenario where
#         user-space needs to know if the temperature has crossed an
#         upper threshold so it can take appropriate action (e.g. warning
#         user that the temperature is critically high, and charging has
#         stopped).
#         Access: Read
#         Valid values: Represented in 1/10 Degrees Celsius
    def tempmaxalert(self):
        try:
            with open(self.tempalertmax_file, "r") as temp_file:
                tempalertmax = temp_file.read()
            tempalertmax = int(tempalertmax)
            return(tempalertmax)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum temperature alert property file not found")

# What:       /sys/class/power_supply/<supply_name>/temp_alert_min
# Date:       July 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Minimum TBAT temperature trip-wire value where the supply will
#           notify user-space of the event.
#         USB:
#           Minimum supply temperature trip-wire value where the supply
#           will notify user-space of the event.
#         This is normally used for the charging scenario where user-space
#         needs to know if the temperature has crossed a lower threshold
#         so it can take appropriate action (e.g. warning user that
#         temperature level is high, and charging current has been
#         reduced accordingly to remedy the situation).
#         Access: Read
#         Valid values: Represented in 1/10 Degrees Celsius
    def tempminalert(self):
        try:
            with open(self.tempalertmin_file, "r") as temp_file:
                tempalertmin = temp_file.read()
            tempalertmin = int(tempalertmin)
            return(tempalertmin)
        except(FileNotFoundError):
            raise FileNotFoundError("minimum temperature alert property file not found")

# What:       /sys/class/power_supply/<supply_name>/capacity_alert_max
# Date:       July 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Maximum battery capacity trip-wire value where the supply will
#         notify user-space of the event. This is normally used for the
#         battery discharging scenario where user-space needs to know the
#         battery has dropped to an upper level so it can take
#         appropriate action (e.g. warning user that battery level is
#         low).
#         Access: Read, Write
#         Valid values: 0 - 100 (percent)
    def capmaxalert(self):
        try:
            with open(self.capalertmax_file, "r") as temp_file:
                capalertmax = temp_file.read()
            capalertmax = int(capalertmax)
            return(capalertmax)
        except(FileNotFoundError):
            raise FileNotFoundError("maximum capacity alert property file not found")

# What:       /sys/class/power_supply/<supply_name>/capacity_alert_min
# Date:       July 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Minimum battery capacity trip-wire value where the supply will
#         notify user-space of the event. This is normally used for the
#         battery discharging scenario where user-space needs to know the
#         battery has dropped to a lower level so it can take
#         appropriate action (e.g. warning user that battery level is
#         critically low).
#         Access: Read, Write
#         Valid values: 0 - 100 (percent)
    def capminalert(self):
        try:
            with open(self.capalertmin_file, "r") as temp_file:
                capalertmin = temp_file.read()
            capalertmin = int(capalertmin)
            return(capalertmin)
        except(FileNotFoundError):
            raise FileNotFoundError("minimum capacity alert property file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_control_limit
# Date:       Oct 2012
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Maximum allowable charging current. Used for charge rate
#         throttling for thermal cooling or improving battery health.
#         Access: Read, Write
#         Valid values: Represented in microamps
    def chargelimit(self):
        try:
            with open(self.chargelimit_file, "r") as temp_file:
                charge_limit = temp_file.read()
            charge_limit = int(charge_limit) * self.factor
            return(charge_limit)
        except(FileNotFoundError):
            raise FileNotFoundError("charge control limit property file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_control_start_threshold
# Date:       April 2019
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Represents a battery percentage level, below which charging will
#         begin.
#         Access: Read, Write
#         Valid values: 0 - 100 (percent)
    def chargestart(self):
        try:
            with open(self.chargestartthreshold_file, "r") as temp_file:
                charge_start = temp_file.read()
            charge_start = int(charge_start)
            return(charge_start)
        except(FileNotFoundError):
            raise FileNotFoundError("charging start threshold property file not found")

# What:       /sys/class/power_supply/<supply_name>/charge_control_end_threshold
# Date:       April 2019
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Represents a battery percentage level, above which charging will
#         stop. Not all hardware is capable of setting this to an arbitrary
#         percentage. Drivers will round written values to the nearest
#         supported value. Reading back the value will show the actual
#         threshold set by the driver.
#         Access: Read, Write
#         Valid values: 0 - 100 (percent)
    def chargeend(self):
        try:
            with open(self.chargeendthreshold_file, "r") as temp_file:
                charge_end = temp_file.read()
            charge_end = int(charge_end)
            return(charge_end)
        except(FileNotFoundError):
            raise FileNotFoundError("charging end threshold property file not found")

#############################################################
# Métodos que devuelven el valor actual de medidas y contadores
# Methods that give back present values of measurements and counters

# What:       /sys/class/power_supply/<supply_name>/capacity
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Fine grain representation of battery capacity.
#         Access: Read
#         Valid values: 0 - 100 (percent)
    def chargepercentage(self):
        try:
            with open(self.chargepercentage_file, "r") as temp_file:
                charge_percentage = temp_file.read()
            charge_percentage = int(charge_percentage)
            return(charge_percentage)
        except(FileNotFoundError):
            raise FileNotFoundError("% charge property file not found")

# There is no info in
# <https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>
# nor
# <https://www.kernel.org/doc/html/latest/power/power_supply_class.html>
# about "charge_now" file. But it seems it give back the present charge
# value in Ah.
    def chargeAh(self):
        try:
            with open(self.chargeAh_file, "r") as temp_file:
                charge_Ah = temp_file.read()
            charge_Ah = int(charge_Ah) * self.factor
            return(charge_Ah)
        except(FileNotFoundError):
            raise FileNotFoundError("charge property file not found")

# What:       /sys/class/power_supply/<supply_name>/current_now
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports an instant, single IBAT current reading for the
#           battery. This value is not averaged/smoothed.
#           Access: Read
#         USB:
#           Reports the IBUS current supplied now. This value is generally
#           read-only reporting, unless the 'online' state of the supply
#           is set to be programmable, in which case this value can be set
#           within the reported min/max range.
#           Access: Read, Write
#         Valid values: Represented in microamps. Negative values are
#         used for discharging batteries, positive values for charging
#         batteries and for USB IBUS current.
    def current(self):
        try:
            with open(self.current_file, "r") as temp_file:
                current_A = temp_file.read()
            current_A = int(current_A) * self.factor
            return(current_A)
        except(FileNotFoundError):
            raise FileNotFoundError("current property file not found")

# What:       /sys/class/power_supply/<supply_name>/voltage_now,
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports an instant, single VBAT voltage reading for the
#           battery. This value is not averaged/smoothed.
#           Access: Read
#         USB:
#           Reports the VBUS voltage supplied now. This value is generally
#           read-only reporting, unless the 'online' state of the supply
#           is set to be programmable, in which case this value can be set
#           within the reported min/max range.
#           Access: Read, Write
#         Valid values: Represented in microvolts
    def voltage(self):
        try:
            with open(self.voltage_file, "r") as temp_file:
                voltage_V = temp_file.read()
            voltage_V = int(voltage_V) * self.factor
            return(voltage_V)
        except(FileNotFoundError):
            raise FileNotFoundError("voltage property file not found")

# What:       /sys/class/power_supply/<supply_name>/current_avg
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports an average IBAT current reading for the battery, over
#           a fixed period. Normally devices will provide a fixed interval
#           in which they average readings to smooth out the reported
#           value.
#         USB:
#           Reports an average IBUS current reading over a fixed period.
#           Normally devices will provide a fixed interval in which they
#           average readings to smooth out the reported value.
#         Access: Read
#         Valid values: Represented in microamps. Negative values are
#         used for discharging batteries, positive values for charging
#         batteries and for USB IBUS current.
    def currentavg(self):
        try:
            with open(self.currentavg_file, "r") as temp_file:
                current_AVG = temp_file.read()
            current_AVG = int(current_AVG) * self.factor
            return(current_AVG)
        except(FileNotFoundError):
            raise FileNotFoundError("average current property file not found")

# What:       /sys/class/power_supply/<supply_name>/voltage_avg,
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports an average VBAT voltage reading for the battery, over a
#         fixed period. Normally devices will provide a fixed interval in
#         which they average readings to smooth out the reported value.
#         Access: Read
#         Valid values: Represented in microvolts
    def voltageavg(self):
        try:
            with open(self.voltageavg_file, "r") as temp_file:
                voltage_AVG = temp_file.read()
            voltage_AVG = int(voltage_AVG) * self.factor
            return(voltage_AVG)
        except(FileNotFoundError):
            raise FileNotFoundError("average voltage property file not found")

# What:       /sys/class/power_supply/<supply_name>/temp
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery:
#           Reports the current TBAT battery temperature reading.
#         USB:
#           Reports the current supply temperature reading. This would
#           normally be the internal temperature of the device itself
#           (e.g TJUNC temperature of an IC)
#         Access: Read
#         Valid values: Represented in 1/10 Degrees Celsius
    def temperature(self):
        try:
            with open(self.temperature_file, "r") as temp_file:
                temperature_C = temp_file.read()
            temperature_C = int(temperature_C)
            return(temperature_C)
        except(FileNotFoundError):
            raise FileNotFoundError("temperature property file not found")

# What:       /sys/class/power_supply/<supply_name>/precharge_current
# Date:       June 2017
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the charging current applied during pre-charging phase
#         for a battery charge cycle.
#         Access: Read
#         Valid values: Represented in microamps
    def prechargecurrent(self):
        try:
            with open(self.prechargecurrent_file, "r") as temp_file:
                precharge_current = temp_file.read()
            precharge_current = int(precharge_current) * self.factor
            return(precharge_current)
        except(FileNotFoundError):
            raise FileNotFoundError("precharge current property file not found")

# There is no info in
# <https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>
# Acording to https://www.kernel.org/doc/html/latest/power/power_supply_class.html
# CHARGE_FULL, CHARGE_EMPTY
#    These attributes means “last remembered value of charge when battery
#    became full/empty”. It also could mean “value of charge when battery
#    considered full/empty at given conditions (temperature, age)”. I.e.
#    these attributes represents real thresholds, not design values.
    def chargefullactual(self):
        try:
            with open(self.chargefullactual_file, "r") as temp_file:
                charge_full_actual = temp_file.read()
            charge_full_actual = int(charge_full_actual) * self.factor
            return(charge_full_actual)
        except(FileNotFoundError):
            raise FileNotFoundError("full charge actual property file not found")
    def chargeemptyactual(self):
        try:
            with open(self.chargeemptyactual_file, "r") as temp_file:
                charge_empty_actual = temp_file.read()
            charge_empty_actual = int(charge_empty_actual) * self.factor
            return(charge_empty_actual)
        except(FileNotFoundError):
            raise FileNotFoundError("full charge actual property file not found")

# What:       /sys/class/power_supply/<supply_name>/capacity_error_margin
# Date:       April 2019
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Battery capacity measurement becomes unreliable without
#         recalibration. This values provides the maximum error
#         margin expected to exist by the fuel gauge in percent.
#         Values close to 0% will be returned after (re-)calibration
#         has happened. Over time the error margin will increase.
#         100% means, that the capacity related values are basically
#         completely useless.
#         Access: Read
#         Valid values: 0 - 100 (percent)
    def capacityerror(self):
        try:
            with open(self.caperrormargin_file, "r") as temp_file:
                caperrormargin = temp_file.read()
            caperrormargin = int(caperrormargin)
            return(caperrormargin)
        except(FileNotFoundError):
            raise FileNotFoundError("capacity error margin property file not found")

# What:       /sys/class/power_supply/<supply_name>/cycle_count
# Date:       January 2010
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the number of full charge + discharge cycles the
#         battery has undergone.
#         Access: Read
#         Valid values:
#             Integer > 0: representing full cycles
#             Integer = 0: cycle_count info is not available
    def cyclecount(self):
        try:
            with open(self.cyclecount_file, "r") as temp_file:
                cycle_count = temp_file.read()
            cycle_count = int(cycle_count)
            return(cycle_count)
        except(FileNotFoundError):
            raise FileNotFoundError("cycle_count property file not found")

#############################################################
# Métodos que devuelven descripciones del estado actual
# Methods that give back present status descriptions

# What:       /sys/class/power_supply/<supply_name>/present
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports whether a battery is present or not in the system.
#         Access: Read
#         Valid values:
#             0: Absent
#             1: Present
    def present(self):
        try:
            with open(self.present_file, "r") as temp_file:
                present = temp_file.read()
            if int(present) == 1:
                battery_present = True
            elif int(present) == 0:
                battery_present = False
            else:
                battery_present = False
            return(battery_present)
        except(FileNotFoundError):
            raise FileNotFoundError("battery present property file not found")

# What:       /sys/class/power_supply/<supply_name>/status
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Represents the charging status of the battery. Normally this
#         is read-only reporting although for some supplies this can be
#         used to enable/disable charging to the battery.
#         Access: Read, Write
#         Valid values: "Unknown", "Charging", "Discharging", "Not charging", "Full"
    def status(self):
        try:
            with open(self.status_file, "r") as temp_file:
                battery_status = temp_file.read()
            battery_status = str(battery_status[:-1])
            return(battery_status)
        except(FileNotFoundError):
            raise FileNotFoundError("battery status property file not found")
    def charging(self):
        try:
            with open(self.status_file, "r") as temp_file:
                battery_status = temp_file.read()
            if battery_status.startswith("Charging"):
                battery_specific_status = True
            else:
                battery_specific_status = False
            return(battery_specific_status)
        except(FileNotFoundError):
            raise FileNotFoundError("battery status property file not found")
    def discharging(self):
        try:
            with open(self.status_file, "r") as temp_file:
                battery_status = temp_file.read()
            if battery_status.startswith("Discharging"):
                battery_specific_status = True
            else:
                battery_specific_status = False
            return(battery_specific_status)
        except(FileNotFoundError):
            raise FileNotFoundError("battery status property file not found")
    def notcharging(self):
        try:
            with open(self.status_file, "r") as temp_file:
                battery_status = temp_file.read()
            if battery_status.startswith("Not charging"):
                battery_specific_status = True
            else:
                battery_specific_status = False
            return(battery_specific_status)
        except(FileNotFoundError):
            raise FileNotFoundError("battery status property file not found")
    def full(self):
        try:
            with open(self.status_file, "r") as temp_file:
                battery_status = temp_file.read()
            if battery_status.startswith("Full"):
                battery_specific_status = True
            else:
                battery_specific_status = False
            return(battery_specific_status)
        except(FileNotFoundError):
            raise FileNotFoundError("battery status property file not found")

# What:       /sys/class/power_supply/<supply_name>/capacity_level
# Date:       June 2009
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Coarse representation of battery capacity.
#         Access: Read
#         Valid values: "Unknown", "Critical", "Low", "Normal", "High", "Full"
    def capacitylevel(self):
        try:
            with open(self.capacitylevel_file, "r") as temp_file:
                capacity_level = temp_file.read()
            capacity_level = str(capacity_level[:-1])
            return(capacity_level)
        except(FileNotFoundError):
            raise FileNotFoundError("capacity level property file not found")

# What:       /sys/class/power_supply/<supply_name>/health
# Date:       May 2007
# Contact:    linux-pm@vger.kernel.org
# Description:
#         Reports the health of the battery or battery side of charger
#         functionality.
#         Access: Read
#         Valid values: "Unknown", "Good", "Overheat", "Dead", "Over voltage",
#                     "Unspecified failure", "Cold", "Watchdog timer expire",
#                     "Safety timer expire", "Over current",
#                     "Calibration required", "Warm", "Cool", "Hot",
#                     "No battery"
    def health(self):
        try:
            with open(self.health_file, "r") as temp_file:
                battery_health = temp_file.read()
            battery_health = str(battery_health[:-1])
            return(battery_health)
        except(FileNotFoundError):
            raise FileNotFoundError("health property file not found")
