#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
#
# Este módulo está hecho para gestionar de forma simple un archivo csv no
# no muy complejo, una pequeña base de datos.
#
# Module made for easing csv file management. The csv file should be simple,
# not a complex data base.
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# Los módulos csv y time forman parte de las librerías estándar
# desde, al menos, python >= 3.5
# Modules csv and time take part of the standard library since, at least,
# python >= 3.5
import csv
from pathlib import Path
import time

# La clase se nombra con class seguido de un nombre en CamelCase

class BasicCsvFile:
    # El primer método de la clase debe ser __init__ seguido siempre
    # de la variable self, que es necesario para luego invocar el objeto que
    # estoy creando. Este primer método sirve para hacer la instancia
    # de la clase.
    # La instancia prepara el archivo, lo crea si no existe y le carga los
    # encabezados de los campos. Hay que indicarle la dirección del archivo,
    # el separador de campos y la lista de campos. También hay que indicarle
    # si queremos que, cada vez que se añada un registro, incluya la
    # fecha+hora, en cuyo caso crea un campo al inicio, llamado "TIME"
    #
    # It prepares csv file: it creates if does not exist and insert the first
    # row with the field names. It needs the csv file path, the char used
    # as delimiter and finally, all field names within a list. If we want
    # to add the date+time each time we add a row, we have to say that and
    # it will add an initial field called "TIME"
    #
    def __init__(self, filepath, fieldlist, fieldchar, addtime=True):
        # Preparo la ruta al archivo
        # It creates the file path
        self.filepath = Path(filepath)
        # Extraigo el path al directorio que contiene el archivo
        # It pulls out the directory path
        self.filedir = self.filepath.parent
        # Amplio la lista si se quiere salvar cada registro con fecha+hora
        # It adds the very first field "Date_time" (default option)
        if addtime:
            fieldlist.insert(0, "Date_time")
        # creo tres propiedades (variables) para la lista de campos, una
        # con la lista de campos, otra con su longitud y otra con el signo
        # delimitador
        # It makes three properties needed for fields: the list of fields,
        # the number of fields and the delimiter char
        self.fieldlist = list(fieldlist)
        self.checklenth = len(self.fieldlist)
        self.fieldchar = str(fieldchar)
        # Creo el directorio si no existe
        # It creates the directory, if it does not exist
        if not self.filedir.exists():
            Path.mkdir(self.filedir, parents=True)
        # Creo el archivo si no existe
        # It creates the csv file, if it does not exist
        if not self.filepath.exists():
            Path.touch(self.filepath)
            # Creo el encabezado de datos
            # First row with field names
            self.addheaders()

# Métodos: escribir (añadir) una linea, exportar el csv a otro directorio,
# comprobar si el csv tiene encabezados y añadir encabezados al csv (aunque
# tenga datos)
# Methods: Writing a new data row, export the csv file to another point,
# check if csv file has a very first line of headers and adding a very first
# line with headers (althoug the csv file has data)

    # Añade una línea al csv
    # It adds a row
    def addrow(self, datalist, addtime=True):
        if addtime:
            datalist.insert(0, time.strftime("%Y/%m/%d %H:%M:%S"))
        if len(datalist) != self.checklenth:
            raise Exception("data does not fit with csv fields")
        with open(self.filepath, "a", newline='') as file:
            datawriter = csv.writer(file, delimiter=self.fieldchar)
            datawriter.writerow(datalist)
        return()

    # Lee el contenido de un registro indicado de forma relativa. Los valores
    # negativos indican que se lea de abajo hacia arriba
    # Reads one register in a relative way. Negative values mean file would
    # be read from end to beginning.
    def readrelative(self, linegap):
        try:
            linegap = int(linegap)
        except:
            raise Exception("invalid value, it should be an integer")
        with open(self.filepath, "r") as file:
            totallines = len(file.readlines())
        if abs(linegap) > totallines:
            raise Exception("too many relative lines")
        if linegap < 0:
            targetline = totallines + linegap
        else:
            targetline = linegap
        # Los objetos csv.reader no permiten el método reversed para
        # recorrer del final al principio.
        # Recorremos todo el archivo hasta la línea buscada desde el principio
        with open(self.filepath, "r") as file:
            datareaded = csv.reader(file, delimiter=self.fieldchar)
            for linenumber, line in enumerate(datareaded):
                if linenumber == targetline:
                    break
        return(line)

    # Inserta una línea, la sobreescribe o la borra. El número de línea se
    # indica de forma relativa. Valores negativos cuenta de atrás adelante
    # Insert one line, overwrite it or delete it. Line number should be give
    # in a relative way. Negative values mean file would be read from bottom.
    def writerelative(self, linegap, action, datalist=[], addtime=True):
        if action not in ["insert", "delete", "overwrite"]:
            raise Exception("invalid action, type insert, delete or overwrite")
        try:
            linegap = int(linegap)
        except:
            raise Exception("invalid value, it should be an integer")
        with open(self.filepath, "r") as file:
            totallines = len(file.readlines())
        if abs(linegap) > totallines:
            raise Exception("too many relative lines")
        if addtime:
            datalist.insert(0, time.strftime("%Y/%m/%d %H:%M:%S"))
        if len(datalist) != self.checklenth:
            raise Exception("data does not fit with csv fields")
        if linegap < 0:
            targetline = totallines + linegap
        else:
            targetline = linegap
        newcontent = []
        with open(self.filepath, "r") as file:
            datareaded = csv.reader(file, delimiter=self.fieldchar)
            for linenumber, line in enumerate(datareaded):
                if linenumber == targetline:
                    if action == "insert":
                        newcontent.append(datalist)
                        newcontent.append(line)
                    elif action == "overwrite":
                        newcontent.append(datalist)
                    elif action == "delete":
                        pass
                else:
                    newcontent.append(line)
        with open(self.filepath, "w", newline='') as file:
            datawriter = csv.writer(file, delimiter=self.fieldchar)
            for line in newcontent:
                datawriter.writerow(line)
        return()

    # Exportar datos
    # It exports csv file
    def csvexport(self, pathtargetfile):
        targetfile = Path(pathtargetfile)
        targetdir = targetfile.parent
        if not targetdir.exists():
            raise Exception("target directory does not exist")
        if not targetfile.exists():
            Path.touch(targetfile)
        fromfile = open(self.filepath, "r")
        tofile = open(targetfile, "w")
        for line in fromfile:
            tofile.writelines(line)
        fromfile.close()
        tofile.close()
        return()

    # Comprobar si tiene encabezados. Devuelve True o False
    # Check if it has headers. Gives back True or False
    def csvhasheaders(self):
        with open(self.filepath, "r") as file:
            # La clase Sniffer del módulo csv tiene un método has_header el
            # que, entregando una pequeña muestra del archivo csv en cuestión
            # en este caso file.read(1024), es capaz de determinar si la
            # primera fila es de encabezados. Devuelve True o False.
            csv_has_header = csv.Sniffer().has_header(file.read(1024))
        return(csv_has_header)

    # Cargar encabezados
    # Add a first row of headers
    def addheaders(self):
        with open(self.filepath, "r") as file:
            content_in_lines = file.readlines()
        with open(self.filepath, "w", newline='') as file:
            headwriter = csv.DictWriter(file, delimiter=self.fieldchar, fieldnames=self.fieldlist)
            headwriter.writeheader()
            file.writelines(content_in_lines)
        return()
