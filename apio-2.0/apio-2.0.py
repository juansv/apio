#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
# El software Apio aumenta la vida útil de la batería de tu portátil. Apio
# trabaja manteniendo su batería en un ciclo de trabajo seguro que incluye
# ciclos de calibración de vez en cuando.
# Si tienes un enchufe wifi con el software Tasmota, Apio es capaz de activar
# y desactivar el enchufe, por lo que no tienes que preocuparte de conectar
# o desconectar el adaptador de CA de tu portátil.
# Pero si no tienes uno de esos enchufes compatibles con Tasmota, tampoco
# tienes que preocuparte, porque Apio te indica cuándo tienes que conectar o
# desconectar el adaptador de CA.
#
# Apio software increases your laptop battery lifetime. Apio works by keeping
# your battery in a safe duty cycle which include calibration cycles from
# time to time.
# If you have a wifi plug with Tasmota software in, Apio is able to switch
# on an off the plug, so you don't have to worry about connecting or
# disconnecting your laptop AC adapter.
# But if you don't have one of those Tasmota compatible plugs, don't also
# have to worry, because Apio tells you when you have to connect or
# disconnect the AC adapter.
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# Módulos estándar
import argparse
import getpass
import gettext
import grp
import os
from pathlib import Path
import time

# Módulos de terceros
import notify2

# Módulos propios de la aplicación
from auxyamlfiles import AuxYamlFiles
from basiccsvfiles import BasicCsvFile
from laptopbatterydata import LaptopBatteryData
from onejobcron import OneJobCron
from tasmotaplugrestapi import TasmotaPlug

# Configuración de las traducciones
gettext.textdomain("apio-2.0")
gettext.bindtextdomain("apio-2.0", "/usr/share/locale")
# Alias para las traducciones.
_ = gettext.gettext

##############################################################################
# LAS FUNCIONES
##############################################################################

def getarguments():
    # Inicio la lista de argumentos
    # Primero creo la instancia
    arglist = argparse.ArgumentParser(description=_("Enlarge your laptop battery lifetime"))
    # Creo tres grupos de argumentos, lo que divide el script en tres
    # el ejecutable para configurar apio, el ejecutable de cron y el ejecutable
    # que autoconfigura el sistema
    subcommand = arglist.add_subparsers(help=_("Apio sub-commands"))
    apioconfig = subcommand.add_parser("config", description=_("Apio configuration"))
    apiocron = subcommand.add_parser("cron", description=_("cron driver"))
    apioauto = subcommand.add_parser("autoconfig", description=_("Apio self-configure tool"))

    ##########################################################################
    # Argumentos del subcomando config

    # Argumento -sh, es opcional, es entero
    apioconfig.add_argument("-sh", "--sethigh", type=int,
                         help=_("Higher charge limit"))

    # Argumento -sl, es opcional, es entero
    apioconfig.add_argument("-sl", "--setlow", type=int,
                         help=_("Lower charge limit"))

    # Argumento -dl, es opcional, es entero
    apioconfig.add_argument("-se", "--setempty", type=int,
                         help=_("Empty battery value"))

    # Argumento -dc, es opcional, es entero
    apioconfig.add_argument("-sc", "--setcalibration", type=int,
                         help=_("Duty cycles between calibrations"))

    # Argumento -e, es opcional, es pathlib.Path
    apioconfig.add_argument("-e", "--export", type=Path,
                          help=_("Exports apio.csv to the given path"))

    # Argumento -bat, es opcional, es pathlib.Path
    apioconfig.add_argument("-bat", "--setbattery", type=Path,
                          help=_("Kernel filesystem battery path /sys/class/power_supply/BATX"))

    # Argumento -ip, es opcional, es str
    apioconfig.add_argument("-ip", "--setip", type=str,
                          help=_("Tasmota device LAN IP address"))

    # Argumento -url, es opcional, es str
    apioconfig.add_argument("-url", "--seturl", type=str,
                          help=_("url of Tasmota device"))

    # Argumento -v, es opcional, es bool
    apioconfig.add_argument("-v", "--verbose", action="store_true", default=False,
                         help=_("Shows Apio status"))

    # Argumento -t, es opcional, es bool
    apioconfig.add_argument("-t", "--toggle", action="store_true", default=False,
                         help=_("Toggle on/off the Tasmota device"))

    # Las opciones de arrancar/parar apio son incompatibles entre si
    startstop = apioconfig.add_mutually_exclusive_group()

    # Argumento -on, es opcional, por defecto False (no se ejecuta)
    startstop.add_argument("-on", "--start", action="store_true", default=False,
                           help=_("Turn on the Apio cron job"))

    # Argumento -off, es opcional, por defecto Falso (no desactiva)
    startstop.add_argument("-off", "--stop", action="store_true", default=False,
                           help=_("Turn off the Apio cron job"))

    ##########################################################################
    # Argumentos del subcomando cron

    # Caso de un argumento posicional. Son de tipo obligatorio. Para no
    # hacerlo obligatorio, se añaden las opciones nargs y default. Para
    # argumentos opcionales, sólo hace falta default, pero para
    # posicionales, tiene que venir acompañado de nargs="?", que significa
    # "en caso de no haber argumento, usa default"
    apiocron.add_argument("runapio", type=bool, nargs="?", default=True,
                          help=_("Runs Apio as cron does"))

    ##########################################################################
    # Argumentos del subcomando auto

    # Caso de un argumento posicional. Son de tipo obligatorio. Para no
    # hacerlo obligatorio, se añaden las opciones nargs y default. Para
    # argumentos opcionales, sólo hace falta default, pero para
    # posicionales, tiene que venir acompañado de nargs="?", que significa
    # "en caso de no haber argumento, usa default"
    apioauto.add_argument("automatic", type=bool, nargs="?", default=True,
                          help=_("Apio self configuration"))

    ##########################################################################
    # Convertir los argumentos en variables del programa
    # primero creamos el objeto namespace con los argumentos
    args_obj = arglist.parse_args()
    # segundo convertimos el objeto en un diccionario
    argument_dict = vars(args_obj)
    # tercero cargo el valor de cada clave del dict en su variable
    sethigh = argument_dict.get("sethigh")
    setlow = argument_dict.get("setlow")
    setempty = argument_dict.get("setempty")
    setcalibration = argument_dict.get("setcalibration")
    start = argument_dict.get("start")
    stop = argument_dict.get("stop")
    verbose = argument_dict.get("verbose")
    toggle = argument_dict.get("toggle")
    export = argument_dict.get("export")
    setbattery = argument_dict.get("setbattery")
    setip = argument_dict.get("setip")
    seturl = argument_dict.get("seturl")
    # Estas claves corresponde con los subcomandos cron y auto
    cron = argument_dict.get("runapio")
    auto = argument_dict.get("automatic")
    return(auto, cron, export, setbattery, setip, seturl, sethigh, setlow, setempty, setcalibration, start, stop, toggle, verbose)

def preset():
    # Esta función genera un string formateado como un archivo YAML con los
    # datos básicos del archivo de configuración, para que se realice una
    # precarga de estos datos en dicho archivo. Es decir, esta función
    # permite dejar listo el archivo de configuración cuando se crea. Por
    # lo tanto, esta función se invoca al crear el archivo de configuración.
    content = """
---
title: "Configuration file for Apio"
counters_status:
  cycle_counter: 0
  time_counter: 0
  next_calibration: 50
cycle_status:
  charging_cycle: True
  discharging_cycle: False
  calibrate_cycle: False
charge_settings:
  high_charge_level: 80
  low_charge_level: 40
  empty_charge_level: 10
  calibration_cycles: 50
cron_settings:
  cron_task: "apio cron 2>&1 | logger -t apiocron"
system_settings:
  battery_path:
  plug_ip:
  plug_url:
  red_icon: "/usr/share/pixmaps/apio_rojo.png"
  green_icon: "/usr/share/pixmaps/apio_blanco.png"
...
      """
    return(content)

def checkpath(path_str):
    path_str = Path(path_str)
    error_matrix = {}
    # ¿Existe?
    if path_str.exists():
        error_matrix.update({"exist_ok": True})
    else:
        error_matrix.update({"exist_ok": False})
    # ¿Pertenece al grupo "apio"?
    if path_str.group() == "apio":
        error_matrix.update({"group_ok": True})
    else:
        error_matrix.update({"group_ok": False})
    # ¿El grupo tiene permiso de escritura?
    # El método os.stat(file).st_mode devuelve el permiso de file (admite dir)
    # Lo convierto a binario y me quedo con la posición 5 empezando por detrás
    # que corresponde con el bit de write del grupo:
    # own-grp-oth
    # 987-654-321
    # rwx-rwx-rwx
    # Si es "1" el grupo tiene permiso de escritura
    if bin(os.stat(path_str).st_mode)[-5:-4] == "1":
        error_matrix.update({"permission_ok": True})
    else:
        error_matrix.update({"permission_ok": False})
    return(error_matrix)

##############################################################################
# EL PROGRAMA
##############################################################################

def main():
    ##########################################################################
    # PARTE COMÚN: VARIABLES COMUNES, INSTANCIAR OBJETOS, PREPARACIÓN ARCHIVOS
    ##########################################################################

    ##########################################################################
    # LECTURA DE ARGUMENTOS Y PREPARACIÓN DE VARIABLES DE ARGUMENTO
    auto, cron, export, setbattery, setip, seturl, sethigh, setlow, setempty, setcalibration, start, stop, toggle, verbose = getarguments()

    ##########################################################################
    # PREPARAR EL ARCHIVO DE CONFIGURACIÓN
    configfile_ready = False
    configfile_completed = False
    APP = "apio"
    CONFIG = "apio.yaml"
    DIR = Path("/var/lib")
    # Intento crear el objeto configfile
    try:
        configfile = AuxYamlFiles(APP, CONFIG, configfile=True)
        configfile.globalauxfile(DIR)
        configfile_ready = True
    except:
        configfile_ready = False
    # Compruebo si configfile tiene datos. Utilizo el "title" como prueba
    try:
        configfile.readone(["title"])
        configfile_completed = True
    except:
        # Si me devuelve alguna excepción, entonces no existe "title".
        # En ese caso, cargo por primera vez el contenido del archivo
        # de configuración.
        # Primero genero un string con toda esta información formateada
        # en JSON/YAML, cosa que hago con la función "precarga" que devuelve
        # dicho string.
        configfile_completed = False

    ##########################################################################
    # PREPARAR EL ARCHIVO DE DATOS
    datafile_ready = False
    filepath = "/var/lib/apio/apio.csv"
    fieldlist = ["Cycle_counter", "Minute_counter", "Charge_at_turning_point"]
    delimiter = ";"
    try:
        datafile = BasicCsvFile(filepath, fieldlist, delimiter, addtime=True)
        datafile_ready = True
    except:
        datafile_ready = False

    ##########################################################################
    # LECTURA DE LAS VARIABLES ALMACENADAS EN EL ARCHIVO DE CONFIGURACIÓN
    if configfile_ready and configfile_completed:
        # Contadores
        cycle_counter = configfile.readone(["counters_status", "cycle_counter"])
        time_counter = configfile.readone(["counters_status", "time_counter"])
        next_calibration = configfile.readone(["counters_status", "next_calibration"])
        # Estados de ciclo
        charging_cycle = configfile.readone(["cycle_status", "charging_cycle"])
        discharging_cycle = configfile.readone(["cycle_status", "discharging_cycle"])
        calibrate_cycle = configfile.readone(["cycle_status", "calibrate_cycle"])
        # Configuraciones de operación
        high_charge_level = configfile.readone(["charge_settings", "high_charge_level"])
        low_charge_level = configfile.readone(["charge_settings", "low_charge_level"])
        empty_charge_level = configfile.readone(["charge_settings", "empty_charge_level"])
        calibration_cycles = configfile.readone(["charge_settings", "calibration_cycles"])
        # Configuraciones de sistema
        red_icon = configfile.readone(["system_settings", "red_icon"])
        green_icon = configfile.readone(["system_settings", "green_icon"])

    ##########################################################################
    # LECTURA DEL NIVEL DE CARGA ACTUAL Y ESTADO BATERÍA
    # Si se produce error de batería, asigno cero al valor de carga,
    # para que la variable no quede sin asignación.
    battery_ERROR = False
    if configfile_ready and configfile_completed:
        BATTERY = configfile.readone(["system_settings", "battery_path"])
        if BATTERY != None:
            battery_ERROR = False
            # Detección de errores
            try:
                mybattery = LaptopBatteryData(BATTERY)
                actual_charge = mybattery.chargepercentage()
            except:
                battery_ERROR = True
                actual_charge = 0
        else:
            battery_ERROR = True
            actual_charge = 0

    ##########################################################################
    # PREPARAR LA TAREA DEL CRON
    if configfile_ready and configfile_completed:
        cron_task = configfile.readone(["cron_settings", "cron_task"])
        TASK = cron_task
        # La clase OneJobCron crea el job con los datos de TASK y le asigna
        # un identificador con los datos de APP. Pero, una vez que el job
        # se escribió en el cron, para saber si el job está en el cron recurre
        # solo al identificador, es decir, no reescribe la tarea, por lo que
        # puedo usar la tarea breve que hay escrita en el configfile sin temor
        # a que se sobreescriba el job en el cron.
        cronjob = OneJobCron(APP, TASK)
        # Si la tarea está activa en el cron, configuro su frecuencia según
        # el nivel de carga.
        # Por defecto, para no dejar la variable sin asignarle valor, la
        # frecuencia se ajusta a 1 minuto.
        job_frequency = 1
        # Pero si el cron está activo, se ajusta a la frecuencia correcta.
        if cronjob.job_enabled():
            if actual_charge >= high_charge_level - 5:
                job_frequency = 2
            elif actual_charge in range(20, high_charge_level - 5):
                job_frequency = 5
            elif actual_charge in range(15, 20):
                job_frequency = 2
            else:
                job_frequency = 1
            cronjob.set_every(job_frequency)

    ##########################################################################
    # PREPARAR EL INTERRUPTOR TASMOTA Y DETERMINAR MODO DE FUNCIONAMIENTO
    automatic_mode = False
    if configfile_ready and configfile_completed:
        # Compruebo si hay alguna dirección para el enchufe WIFI tasmota
        for plug_address in ["plug_ip", "plug_url"]:
            WIFIPLUG = configfile.readone(["system_settings", plug_address])
            if WIFIPLUG != None:
                break
        # Si el enchufe WIFI tasmota tiene alguna dirección, creo el objeto para
        # el enchufe y le pregunto si está activo. Si todo va bien, configuro
        # la variable de "modo automático".
        if WIFIPLUG != None:
            try:
                myplug = TasmotaPlug(WIFIPLUG)
                if myplug.ready():
                    automatic_mode = True
            except:
                automatic_mode = False

    ##########################################################################
    # PREPARAR EL ÁREA DE NOTIFICACIONES
    # El área de notificaciones se prepara con independencia de que esté
    # preparado el archivo de configuración, ya que no lo necesita. El área
    # de notificaciones depende del archivo de configuración cuando apio
    # se ejecuta desde el cron.
    notify2.init(_("Apio notifications"))
    # Se puede indicar aquí el path
    # al icono del programa, en lugar de None.
    notice = notify2.Notification(None)
    # Esto le asigna una prioridad a la notificación. Hay tres niveles de
    # prioridad LOW, NORMAL, CRITICAL.
    notice.set_urgency(notify2.URGENCY_NORMAL)

    ##########################################################################
    # SALIDA DE LA PARTE COMÚN
    apio_ERROR = not configfile_ready or not configfile_completed or not datafile_ready or battery_ERROR
    # No mostramos la salida de la parte común si estamos ejecutando apio autoconfig
    if apio_ERROR and not auto:
        print(_("Apio can not start because is not well configured"))
        print(_("Please, run apio autoconfig once"))
        print(_("If you have done it, try apio config -v and follow the instructions"))
    else:
        pass

    ##########################################################################
    # SUBCOMANDO AUTO
    ##########################################################################

    # ETAPAS DE AUTOCONFIGURACIÓN
    # 1. Comprobar que existe el grupo apio y que el usuario es miembro
    # 2. Comprobar que existe el directorio /var/lib/apio y sus permisos
    # 3. Comprobar el grupo de los archivos de configuración y datos
    # 4. Rellenar el archivo de configuración (si no lo está)
    # 5. Completar el battery_path
    # 6. Completar la variable DBUS_SESSION_BUS_ADDRESS
    # 7. Registrar el job en el cron
    # 8. Archivo de datos
    # 9. Salida de la autoconfiguración

    ##########################################################################
    # 1. Comprobar que existe el grupo apio y que el usuario es miembro
    if auto:
        print("\033[01m")
        print(_("Group and user checking..."), "\033[0m")
        try:
            apiogroup_data = grp.getgrnam("apio")
            apiogroup_members = apiogroup_data.gr_mem # El atributo gr_mem es una lista con los miembros
            apiogroup_userbelong = False
            for user in apiogroup_members:
                if user == getpass.getuser():
                    apiogroup_userbelong = True
                    break
            if apiogroup_userbelong:
                print(_("Apio group exists and user is added"))
            else:
                print("\033[31m", "\033[01m", _("ERROR: apio group exists but user is not added!"), "\033[0m")
        except:
            print("\033[31m", "\033[01m", _("ERROR: apio group does not exist!"), "\033[0m")

    ##########################################################################
    # 2. Comprobar que existe el directorio /var/lib/apio y sus permisos
    if (auto and not configfile_ready) or (auto and not datafile_ready):
        # Si no se pudieron crear los objetos de los archivos de configuración
        # ni datos, probablemente el problema esté con el directorio que los
        # contiene: /var/lib/apio.
        # Verifico si existe el directorio, si pertenece al grupo apio y
        # si el grupo apio tiene permisos de escritura
        apio_dir = Path(DIR / APP)
        # matriz de errores del directorio /var/lib/apio
        apio_dir_error_mtx = checkpath(apio_dir)
        print("\033[01m")
        print(apio_dir, _(" checking..."), "\033[0m")
        if not apio_dir_error_mtx.get("exist_ok"):
            print("\033[31m", "\033[01m", _("ERROR: directory does not exist!"), "\033[0m")
        else:
            print(_("Directory exists"))
        if not apio_dir_error_mtx.get("group_ok"):
            print("\033[31m", "\033[01m", _("ERROR: directory does not belong to apio group!"), "\033[0m")
        else:
            print(_("Directory belongs to apio group"))
        if not apio_dir_error_mtx.get("permission_ok"):
            print("\033[31m", "\033[01m", _("ERROR: Directory does not have write permissions on group!"), "\033[0m")
        else:
            print(_("Directory has write permissions on group"))

    ##########################################################################
    # 3. Comprobar el grupo de los archivos de configuración y datos
    # Parte del archivo de configuración
    if auto and configfile_ready:
        apio_config_path = Path(DIR / APP / CONFIG)
        apio_dir_error_mtx = checkpath(apio_config_path)
        print("\033[01m")
        print(apio_config_path, _(" checking..."), "\033[0m")
        if not apio_dir_error_mtx.get("exist_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not exist!"), "\033[0m")
        else:
            print(_("File exists"))
        if not apio_dir_error_mtx.get("group_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not belong to apio group!"), "\033[0m")
        else:
            print(_("File belongs to apio group"))
        if not apio_dir_error_mtx.get("permission_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not have write permissions on group!"), "\033[0m")
        else:
            print(_("File has write permissions on group"))
    # Parte del archivo de datos
    if auto and datafile_ready:
        apio_data_path = Path("/var/lib/apio/apio.csv")
        apio_dir_error_mtx = checkpath(apio_data_path)
        print("\033[01m")
        print(apio_data_path, _(" checking..."), "\033[0m")
        if not apio_dir_error_mtx.get("exist_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not exist!"), "\033[0m")
        else:
            print(_("File exists"))
        if not apio_dir_error_mtx.get("group_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not belong to apio group!"), "\033[0m")
        else:
            print(_("File belongs to apio group"))
        if not apio_dir_error_mtx.get("permission_ok"):
            print("\033[31m", "\033[01m", _("ERROR: File does not have write permissions on group!"), "\033[0m")
        else:
            print(_("File has write permissions on group"))

    ##########################################################################
    # 4. Rellenar el archivo de configuración (si no lo está) con lo básico
    if auto and configfile_ready and not configfile_completed:
        print("\033[01m")
        print(_("Basic configuration, checking..."), "\033[0m")
        try:
            filecontent = preset()
            configfile.writeall(filecontent)
            print(_("/var/lib/apio/apio.yaml basic configuration done"))
        except:
            print(_("/var/lib/apio/apio.yaml not accessible"))

    ##########################################################################
    # 5. Completar el battery_path
    if auto and configfile_ready:
        print("\033[01m")
        print(_("Battery information, checking..."), "\033[0m")
        try:
            for index in range(10):
                testing_path = "/sys/class/power_supply/BAT" + str(index)
                if Path(testing_path).exists():
                    configfile.writeone(["system_settings", "battery_path"], testing_path, new=False)
                    print(_("Battery path: "), testing_path)
                    print(_("It is recorded in config file"))
                    break
            if configfile.readone(["system_settings", "battery_path"]) == None:
                print(_("directory /sys/class/power_supply/BAT# not found"))
                print(_("please, edit config file and fill battery_path field"))
        except:
            print(_("/var/lib/apio/apio.yaml not accessible"))

    ##########################################################################
    # 6. Completar la variable DBUS_SESSION_BUS_ADDRESS
    if auto and configfile_ready:
        print("\033[01m")
        print(_("Notification bus variable, checking..."), "\033[0m")
        try:
            # Determinar el usuario
            user_name = "cron_" + str(getpass.getuser())
            dbus_variable = os.environ.get("DBUS_SESSION_BUS_ADDRESS")
            # La variable DBUS_SESSION_BUS_ADDRESS a veces toma una clave
            # de encriptació que se añade a la variable tras una coma.
            # Me quedo sólo con la primera parte de la variable
            parts = dbus_variable.split(",")
            dbus_variable = parts[0]
            # Si ya existe el usuario, lo reescribo (new=False)
            try:
                configfile.readone(["cron_settings", user_name])
                configfile.writeone(["cron_settings", user_name], dbus_variable, new=False)
            # Si no existe, lo registro por primera vez (new=True)
            except:
                configfile.writeone(["cron_settings", user_name], dbus_variable, new=True)
            if configfile.readone(["cron_settings", user_name]) == None:
                print(_("not able to find DBUS_SESSION_BUS_ADDRESS variable"))
                print(_("Apio would not show notifications on desktop"))
            else:
                print(_("User Notification bus variable: "), dbus_variable)
                print(_("It is recorded in config file"))
        except:
            print(_("/var/lib/apio/apio.yaml not accessible"))

    ##########################################################################
    # 7. Registrar el job en el cron
    if auto and configfile_ready:
        print("\033[01m")
        print(_("Apio cron job, checking..."), "\033[0m")
        try:
            cron_task = configfile.readone(["cron_settings", "cron_task"])
            # Hay que crear el objeto, da igual con qué tarea, por si el job ya
            # estuviese cargado en el cron
            cronjob = OneJobCron(APP, cron_task)
            # Ahora se puede borrar el job y volver a cargarlo con la info de
            # la variable DBUS_SESSION_BUS_ADDRESS
            cronjob.delete_job()
            # Creamos el job completo, con la variable y la tarea. Si no hubiese
            # varible, creo el job solo con la tarea.
            user_dbus = configfile.readone(["cron_settings", user_name])
            if user_dbus != None:
                TASK = "export DBUS_SESSION_BUS_ADDRESS=" + user_dbus + " && " + cron_task
            else:
                TASK = cron_task
            # Podemos volver a usar el mismo nombre para el objeto, no genera
            # conflicto. Lo configuro para ejecutar cada 5 minutos, pero lo
            # deshabilito.
            cronjob = OneJobCron(APP, TASK)
            cronjob.set_every(5)
            cronjob.set_disable()
            print(_("Apio job scheduled but disabled"))
            print(_("Run apio config -on when you want Apio get started"))
        except:
            print(_("/var/lib/apio/apio.yaml not accessible"))

    ##########################################################################
    # 8. Archivo de datos
    if auto and datafile_ready:
        print("\033[01m")
        print(_("Data base, checking..."), "\033[0m")
        try:
            # Añado encabezados, si no los tuviese
            if not datafile.csvhasheaders():
                datafile.addheaders()
            # Exporto a /dev/null para probar si es posible leer el archivo
            datafile.csvexport("/dev/null")
            print(_("/var/lib/apio/apio.csv file ready"))
        except:
            print(_("/var/lib/apio/apio.csv not accessible"))

    ##########################################################################
    # 9. Salida de la autoconfiguración
    if auto:
        print("\033[01m")
        print(_("Self configuration process finished"), "\033[0m")
        print(_("If any error has been detected, please, fix it"))
        print(_("and, then, run apio autoconfig again"))
        print(_("Finally, get status info by running apio config -v"))
        print(_("Once all errors disapear:"))
        print(_("- Set up Apio as you like it. Read apio config -h"))
        print(_("- Start Apio by running apio config -on"))

    ##########################################################################
    # SUBCOMANDO CRON
    ##########################################################################

    # LISTA DE ESCENARIOS
    # 1. La batería presenta un problema
    # 2. La batería está en ciclo de carga. Contemplar modo manual y automático
    # 3. La batería está en ciclo de descarga. Contemplar modo manual y automático
    # 4. La batería está en ciclo de calibración + descarga
    # 5. La batería está en ciclo de calibración + carga
    # 6. Actualización de datos y registro en csv
    # 7. Escritura en el archivo de configuración

    ##########################################################################
    # 1. LA BATERÍA PRESENTA UN PROBLEMA
    # Si battery_ERROR es True entonces apio_ERROR es True también. Para
    # determinar el error de batería debo consultar apio_ERROR en afirmativo
    if apio_ERROR and battery_ERROR:
        # Primero, avisar al usuario
        notice.update(_("Apio warning"),
                      message=_("Something happens to your laptop battery!"),
                      icon=red_icon)
        notice.show()
        # Activo el cron para avisar cada poco del error
        cronjob.set_every(1)

    ##########################################################################
    # INICIAR VARIABLES PREVIAS A SUBCOMANDO CRON
    # Detectoras de fin de ciclo
    end_of_charging_cycle = False
    end_of_discharging_cycle = False
    end_of_calibrate_cycle_down = False
    end_of_calibrate_cycle_up = False

    ##########################################################################
    # 2. LA BATERÍA ESTÁ EN CICLO DE CARGA
    if not apio_ERROR and cron and charging_cycle and not calibrate_cycle:
        # Caso 1: el ciclo de carga ha terminado. Verifico que queda
        # desconectado. Si todo sale bien actualizo el estado de ciclo.
        if actual_charge >= high_charge_level:
            if automatic_mode:
                myplug.turnOFF()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if mybattery.charging(): # compruebo si sigue cargando y aviso
                notice.update(_("Apio says"),
                              message=_("Battery charged, please unplug AC adapter"),
                              icon=green_icon)
                notice.show()
            end_of_charging_cycle = True
        #
        # Caso 2: el ciclo de carga no ha terminado. Verifico que queda
        # conectado.
        elif actual_charge < high_charge_level:
            if automatic_mode:
                myplug.turnON()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if not mybattery.charging():
                notice.update(_("Apio says"),
                              message=_("Now is time for charging, please plug AC adapter"),
                              icon=green_icon)
                notice.show()
        #
        # No hay más casos
        else:
            pass

    ##########################################################################
    # 3. LA BATERÍA ESTÁ EN CICLO DE DESCARGA
    if not apio_ERROR and cron and discharging_cycle and not calibrate_cycle:
        # Caso 1: el ciclo de descarga ha terminado. Verifico que queda
        # conectado. Si todo está bien, actualizo estado de ciclo.
        if actual_charge <= low_charge_level:
            if automatic_mode:
                myplug.turnON()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if not mybattery.charging(): # compruebo si no está cargando y aviso
                notice.update(_("Apio says"),
                              message=_("Battery low, please plug AC adapter"),
                              icon=green_icon)
                notice.show()
            end_of_discharging_cycle = True
        #
        # Caso 2: el ciclo de descarga no ha terminado. Verifico que queda
        # desconectado.
        elif actual_charge > low_charge_level:
            if automatic_mode:
                myplug.turnOFF()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if mybattery.charging():
                notice.update(_("Apio says"),
                              message=_("Now is time for discharging, please unplug AC adapter"),
                              icon=green_icon)
                notice.show()
        #
        # No hay más casos
        else:
            pass

    ##########################################################################
    # 4. LA BATERÍA ESTÁ EN CICLO DE CALIBRACIÓN + DESCARGA
    if not apio_ERROR and cron and calibrate_cycle and discharging_cycle:
        # Caso 1: el ciclo de descarga para calibración ha terminado. Verifico
        # que queda conectado. Si todo está bien, actualizo el estado de ciclo.
        if actual_charge <= empty_charge_level:
            if automatic_mode:
                myplug.turnON()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if not mybattery.charging(): # compruebo si no está cargando y aviso
                notice.update(_("Apio says"),
                              message=_("Battery empty, please plug AC adapter"),
                              icon=red_icon)
                notice.show()
            end_of_calibrate_cycle_down = True
        #
        # Caso 2: el ciclo de descarga para calibración no ha terminado.
        # Verifico que queda desconectado.
        elif actual_charge > empty_charge_level:
            if automatic_mode:
                myplug.turnOFF()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if mybattery.charging():
                notice.update(_("Apio says"),
                              message=_("Now is time for one calibration cycle, please unplug AC adapter"),
                              icon=green_icon)
                notice.show()
        #
        # No hay más casos
        else:
            pass

    ##########################################################################
    # 5. LA BATERÍA ESTÁ EN CICLO DE CALIBRACIÓN + CARGA
    if not apio_ERROR and cron and calibrate_cycle and charging_cycle:
        # Caso 1: el ciclo de carga para calibración ha terminado. Verifico
        # que queda desconectado. Si todo está bien, actualizo el estado de ciclo.
        if mybattery.full() or actual_charge == 100 or (mybattery.status() == "Unknown" and actual_charge >= high_charge_level):
            if automatic_mode:
                myplug.turnOFF()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if mybattery.status() in ["Unknown", "Full", "Charging"]: # compruebo si está cargando y aviso
                notice.update(_("Apio says"),
                              message=_("Battery full, please unplug AC adapter"),
                              icon=red_icon)
                notice.show()
            end_of_calibrate_cycle_up = True
        #
        # Caso 2: el ciclo de carga para calibración no ha terminado.
        # Verifico que queda conectado.
        elif actual_charge < 100:
            if automatic_mode:
                myplug.turnON()
                time.sleep(1) # espero un segundo a que reaccione el enchufe
            if not mybattery.charging():
                notice.update(_("Apio says"),
                              message=_("Now is time for one calibration cycle, please plug AC adapter"),
                              icon=green_icon)
                notice.show()
        #
        # No hay más casos
        else:
            pass

    ##########################################################################
    # 6. ACTUALIZACIÓN DE DATOS Y REGISTRO EN CSV

    # Si ocurrió algún "end_of_whatever_cycle" sin fallo de batería => se
    # terminó un ciclo => registro en el csv, contador de ciclos + 1 y reseteo
    # del contador de tiempo
    end_of_sth = end_of_charging_cycle or end_of_discharging_cycle or end_of_calibrate_cycle_up or end_of_calibrate_cycle_down
    if not apio_ERROR and cron and end_of_sth:
        # ["Cycle_number", "Cycle_length", "End_charge"]
        cycle_counter = cycle_counter + 1
        datafile.addrow([cycle_counter, time_counter, actual_charge], addtime=True)

    # Si el cargador no corresponde con el estado de carga o descarga,
    # actualiza el último registro del csv, hasta que haya correspondencia
    if not apio_ERROR and cron:
        # Si se lanzó apio desde cron sin fallo de batería => sumar al 
        # contador tiempo la frecuencia del job en cron
        time_counter = time_counter + job_frequency
        # Echo un vistazo a los dos últimos registros del csv
        csv_last_cycle = int(datafile.readrelative(-1)[1])
        csv_last_charge = int(datafile.readrelative(-1)[3])
        # Entró en ciclo de descarga pero el cargador sigue enchufado
        if discharging_cycle and mybattery.status() in ["Unknown", "Full", "Charging"]:
            # Si dentro del mismo ciclo, la carga sigue aumentando, actualizo
            # el último registro
            if csv_last_cycle == cycle_counter and csv_last_charge <= actual_charge:
                datafile.writerelative(-1, "overwrite", [cycle_counter, time_counter, actual_charge], addtime=True)
        # Entró en ciclo de carga pero el cargador no está enchufado
        elif charging_cycle and not mybattery.charging():
            # Si dentro del mismo ciclo, la carga sigue disminuyendo,
            # actualizo el último registro
            if csv_last_cycle == cycle_counter and csv_last_charge >= actual_charge:
                datafile.writerelative(-1, "overwrite", [cycle_counter, time_counter, actual_charge], addtime=True)
        else:
            pass

    # Si ocurrió el "end_of_calibrate_cycle_down" sin fallo de batería => comienza
    # ciclo de carga pero sigo en ciclo de calibración
    if end_of_calibrate_cycle_down:
        calibrate_cycle = True
        discharging_cycle = False
        charging_cycle = True

    # Si ocurrió el "end_of_calibrate_cycle_up" sin fallo de batería => comienza
    # ciclo de descarga & seteo el siguiente ciclo de calibración
    if end_of_calibrate_cycle_up:
        calibrate_cycle = False
        discharging_cycle = True
        charging_cycle = False
        next_calibration = next_calibration + calibration_cycles

    # Si ocurrió el "end_of_discharging_cycle" sin fallo de batería => comienza
    # ciclo de carga
    if end_of_discharging_cycle:
        calibrate_cycle = False
        discharging_cycle = False
        charging_cycle = True

    # Si ocurrió el "end_of_charging_cycle" sin fallo de batería => comienza
    # ciclo de descarga y/o de calibración (la calibración es siempre
    # depués de un ciclo de carga)
    if end_of_charging_cycle:
        charging_cycle = False
        discharging_cycle = True
        if cycle_counter >= next_calibration:
            calibrate_cycle = True
        else:
            calibrate_cycle = False

    ##########################################################################
    # 7. ESCRIBIR EN EL ARCHIVO DE CONFIGURACIÓN
    if not apio_ERROR and cron:
        # Contadores
        configfile.writeone(["counters_status", "cycle_counter"], cycle_counter)
        configfile.writeone(["counters_status", "time_counter"], time_counter)
        configfile.writeone(["counters_status", "next_calibration"], next_calibration)
        # Estado de ciclo
        configfile.writeone(["cycle_status", "charging_cycle"], charging_cycle)
        configfile.writeone(["cycle_status", "discharging_cycle"], discharging_cycle)
        configfile.writeone(["cycle_status", "calibrate_cycle"], calibrate_cycle)

    ##########################################################################
    # SUBCOMANDO CONFIG
    ##########################################################################

    ##########################################################################
    # ACTUALIZACIÓN DEL ARCHIVO DE CONFIGURACIÓN
    # charge_settings
    if not apio_ERROR and sethigh:
        if sethigh <= low_charge_level:
            print(_("You have typed a wrong charge value"))
        else:
            configfile.writeone(["charge_settings", "high_charge_level"], sethigh)
    if not apio_ERROR and setlow:
        if setlow >= high_charge_level:
            print(_("You have typed a wrong charge value"))
        else:
            configfile.writeone(["charge_settings", "low_charge_level"], setlow)
    if not apio_ERROR and setempty:
        if setempty not in range (10, 21):
            print(_("For calibration process, charge should be between 10% and 20%"))
        elif setempty >= low_charge_level:
            print(_("You have typed a wrong charge value"))
        else:
            configfile.writeone(["charge_settings", "empty_charge_level"], setempty)
    if not apio_ERROR and setcalibration:
        configfile.writeone(["charge_settings", "calibration_cycles"], setcalibration)
    # system_settings
    if not apio_ERROR and setbattery:
        if setbattery.exists():
            # en un yaml no puedo almacenar la clase Path, lo paso a str
            configfile.writeone(["system_settings", "battery_path"], str(setbattery))
        else:
            print(_("The path "), setbattery, _(" does not exist"))
    if not apio_ERROR and setip:
        if setip.startswith("http://"):
            configfile.writeone(["system_settings", "plug_ip"], setip)
        else:
            setip = "http://" + setip
            configfile.writeone(["system_settings", "plug_ip"], setip)
    if not apio_ERROR and seturl:
        if seturl.startswith("http://"):
            configfile.writeone(["system_settings", "plug_url"], seturl)
        else:
            seturl = "http://" + seturl
            configfile.writeone(["system_settings", "plug_url"], seturl)

    ##########################################################################
    # EXPORTAR CSV
    if not apio_ERROR and export:
        # El método csvexport no crea la ruta de directorios al archivo
        # destino, pero sí el archivo de destino. Por lo tanto, primero hay
        # que verificar la ruta y luego llamar al método
        if not export.parent.exists():
            Path.mkdir(export.parent, parents=True)
        datafile.csvexport(export)

    ##########################################################################
    # ACTUALIZACIÓN DEL CRON
    if not apio_ERROR and start:
        cronjob.set_enable()
    if not apio_ERROR and stop:
        cronjob.set_disable()

    ##########################################################################
    # CAMBIO DE ESTADO DEL ENCHUFE
    if not apio_ERROR and toggle:
        myplug.commute()

    ##########################################################################
    # INFORMAR POR SALIDA ESTÁNDAR

    # EVENTOS SOBRE LOS QUE INFORMAR
    # 1. Si existe o no el archivo de configuración, el de datos y pertecen
    # al grupo apio
    # 2. Si la batería presenta un problema (ruta ok)
    # 3. Si la variable dbus existe
    # 4. Si cron está en marcha
    # 5. Si existe dispositivo Tasmota y está ready
    # 6. Información del modo (auto o man), estado de ciclo y niveles de carga

    if verbose:
    # El código ANSI "\033[01m" pone negritas, "\033[31m" colorea de rojo
    # y "\033[0m" quita ambos formatos

    ##########################################################################
    # 1. Si existe o no el archivo de configuración, el de datos y pertecen
    # al grupo apio
        print("\033[01m")
        print(_("Apio necessary files:"), "\033[0m")
        if not configfile_ready or not datafile_ready:
            print("\033[31m", "\033[01m", _("ERROR: Apio files not accessibles"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))
        else:
            print(_("- Apio files are accessibles"))

    ##########################################################################
    # 2. Si la batería presenta un problema (ruta ok)
        print("\033[01m")
        print(_("Battery configuration:"), "\033[0m")
        # Batería configurada en apio.yaml
        try:
            check_battery = configfile.readone(["system_settings", "battery_path"])
            check_battery = Path(check_battery)
            if check_battery.exists():
                print(_("- Battery set up in config file"))
                print(_("- Current charge"), actual_charge, "%")
        except:
            print("\033[31m", "\033[01m", _("ERROR: Can not read Apio configuration"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))

    ##########################################################################
    # 3. Si la variable dbus existe user_name
        print("\033[01m")
        print(_("Desktop notifications config:"), "\033[0m")
        try:
            user_name = "cron_" + str(getpass.getuser())
            check_dbus = configfile.readone(["cron_settings", user_name])
            if check_dbus != None:
                print(_("- Apio is able to show desktop notifications"))
                print(_("- A trial notification has been sent"))
                notice.update(_("Apio trial"),
                              message=_("This is a trial message sent by apio config -v"),
                              icon=green_icon)
                notice.show()
        except:
            print("\033[31m", "\033[01m", _("ERROR: Can not read Apio configuration"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))

    ##########################################################################
    # 4. Si cron está en marcha
        print("\033[01m")
        print(_("Cron job of Apio:"), "\033[0m")
        try:
            if cronjob.job_enabled():
                print(_("- Apio job is scheduled and ready"))
            else:
                print(_("- Apio job is not scheduled"))
        except:
            print("\033[31m", "\033[01m", _("ERROR: Can not read in crontab"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))

    if verbose and configfile_ready:
    # Los eventos 5 y 6 no tienen sentido si no existe el archivo de configuración

    ##########################################################################
    # 5. Si existe dispositivo Tasmota y está ready
        print("\033[01m")
        print(_("Tasmota device configuration:"), "\033[0m")
        try:
            if WIFIPLUG != None:
                print(_("- There is a Tasmota device set up in: "), WIFIPLUG)
                if automatic_mode and myplug.ready():
                    print(_("- Tasmota device reacts properly to Apio"))
                else:
                    print(_("- Tasmota device does not react, is it switched on?"))
            else:
                print(_("- There is not any Tasmota device set up"))
                print(_("- Apio could not work in automatic mode"))
                print(_("To sep up a Tasmota device, run:"))
                print(_("apio config -ip <ip address>"))
                print(_("apio config -url <url>"))
                print(_("depending on your LAN or your Tasmota device"))
        except:
            print("\033[31m", "\033[01m", _("ERROR: Can not read Apio configuration"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))

    ##########################################################################
    # 6. Información del modo (auto o man), estado de ciclo y niveles de carga
        # Modo de funcionamiento
        print("\033[01m")
        print(_("Operation mode:"), "\033[0m")
        try:
            if automatic_mode:
                print(_("- Apio in automatic mode"))
            else:
                print(_("- Apio in manual mode"))
            if not battery_ERROR:
                if mybattery.charging():
                    print(_("- AC adapter is plugged"))
                else:
                    print(_("- AC adapter is not plugged"))
            # Estado de ciclo
            print("\033[01m")
            print(_("Current cycle:"), "\033[0m")
            if calibrate_cycle:
                print(_("- Calibration cycle"))
            if charging_cycle:
                print(_("- Charging cycle"))
            if discharging_cycle:
                print(_("- Discharging cycle"))
            # Valores de funcionamiento
            print("\033[01m")
            print(_("Current set up:"), "\033[0m")
            print(_("- Higher charge limit"), high_charge_level, "%")
            print(_("- Lower charge limit"), low_charge_level, "%")
            print(_("- Empty battery value"), empty_charge_level, "%")
            print(_("- Duty cycles between calibrations"), calibration_cycles, _("cycles"))
        except:
            print("\033[31m", "\033[01m", _("ERROR: Can not read Apio configuration"), "\033[0m")
            print(_("Please, run apio autoconfig and follow the instructions"))

##############################################################################
# EJECUCIÓN DEL PROGRAMA
##############################################################################
if __name__ == "__main__":
    main()