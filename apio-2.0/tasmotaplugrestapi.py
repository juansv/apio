#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
#
# Este módulo lanza instrucciones simples vía REST API contra un
# dispositivo tipo "enchufe WIFI" que use el software TASMOTA.
# Necesita python >= 3.7
# Para más información sobre REST API en python:
# https://realpython.com/api-integration-in-python/#rest-apis-and-web-services
# Para más información sobre los comandos ejecutables vía http en TASMOTA:
# https://tasmota.github.io/docs/Commands/#with-web-requests
#
# This module gives simple orders to a "plug WIFI" devices via REST API.
# But those devices should have TASMOTA software built-in.
# It needs python >= 3.7
# For further info about REST API in python:
# https://realpython.com/api-integration-in-python/#rest-apis-and-web-services
# For further info about how to run commands via http in TASMOTA:
# https://tasmota.github.io/docs/Commands/#with-web-requests
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# El módulo requests necesita python >= 3.7
# The requests module needs python >= 3.7

import requests
import subprocess

# La clase se nombra con class seguido de un nombre en CamelCase
# El primer método de la clase debe ser __init__ seguido siempre
# de la variable self, que es necesario para luego invocar el objeto que
# estoy creando. Este primer método sirve para hacer la instancia
# de la clase.
class TasmotaPlug:
    # La instancia sólo prepara las variables para usar en el resto de
    # métodos.
    # Se necesita la dirección web del dispositivo TASMOTA. Se puede
    # entregar como dirección ip o con el nombre que usa el dispositivo
    # en la red. Pero debe empezar por "http://" y debe ser un string
    #
    # The instance of this class only prepares all variables necessaries for
    # each method.
    # It is needed the url of TASMOTA device. It could be delivered
    # as ip or as device name. Any case, it should start by "http://"
    # and should be an string variable.
    #
    def __init__(self, tasmota_plug_url):
        # Creo una variable str con la dirección url del dispositivo tasmota
        # An str variable is made with device url
        self.tasmota_url = str(tasmota_plug_url)
        # Verifico si está el dispositivo en la red
        # Check if the devide is in the LAN
        try:
            response = requests.get(self.tasmota_url)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            pass
        else:
            raise Exception("something wrong with tasmota device")
        # Creo varias propiedades (variables) para usar en los métodos.
        # Los espacios en la dirección url se escriben "%20"
        # Properties which will be used by the methods. White spaces
        # should be written as "%20".
        self.switch_on = "/cm?cmnd=power%20on"
        self.switch_off = "/cm?cmnd=power%20off"
        self.toggle = "/cm?cmnd=power%20toggle"
        self.led_follows_wifi = "/cm?cmnd=ledpower%201"
        self.led_disable = "/cm?cmnd=ledpower%200"
        self.led_follows_switch = "/cm?cmnd=ledstate%201"

    # Método para encender el enchufe WIFI tasmota
    # Method to turn on the tasmota WIFI plug
    def turnON(self):
        try:
            response = requests.get(self.tasmota_url + self.switch_on)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método para apagar el enchufe WIFI tasmota
    # Method to turn off the tasmota WIFI plug
    def turnOFF(self):
        try:
            response = requests.get(self.tasmota_url + self.switch_off)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método para conmutar el enchufe WIFI tasmota
    # Method to toggle the tasmota WIFI plug
    def commute(self):
        try:
            response = requests.get(self.tasmota_url + self.toggle)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método configura el led para que se encienda cuando hay wifi
    # Method to set up led to bright when wifi is on
    def ledfollowswifi(self):
        try:
            response = requests.get(self.tasmota_url + self.led_follows_wifi)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método apaga el led
    # Method to disable the led
    def leddisable(self):
        try:
            response = requests.get(self.tasmota_url + self.led_disable)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método configura el led para que siga el estado del enchufe
    # Method to set up led to bright when plug is turned on
    def ledfollowsswitch(self):
        try:
            response = requests.get(self.tasmota_url + self.led_follows_switch)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        if response.status_code == 200:
            return()
        else:
            raise Exception("something wrong with tasmota device")

    # Método para conocer el estado del enchufe WIFI tasmota
    # Method to know tasmota WIFI plug state
    def statuscode(self):
        try:
            response = requests.get(self.tasmota_url)
        except(OSError, ConnectionError):
            raise OSError("tasmota device not found")
            raise ConnectionError("tasmota device not found")
        return(response.status_code)

    # Método para comprobar si el enchufe WIFI tasmota está en la LAN
    # Funciona haciendo ping contra el enchufe WIFI
    # Method to check if tasmota WIFI plug is on LAN. It works making
    # ping against WIFI plug
    def ready(self):
        # Para hacer ping se necesita quitar "http://"
        # To make ping it is necessary remove "http://"
        if self.tasmota_url.startswith("http://"):
            ping_url = self.tasmota_url.lstrip("http://")
        elif self.tasmota_url.startswith("https://"):
            ping_url = self.tasmota_url.lstrip("https://")
        else:
            ping_url = self.tasmota_url
        #
        # Ejecuta el comando "~$ timeout 0.2 ping -c 1 ping_url"
        # que hace un ping de un solo paquete (-c 1) a la dirección del
        # enchufe tasmota (ping_url) pero sólo espera 0.2 seg a la
        # respuesta (timeout 0.2)
        #
        # It runs the command "~$ timeout 0.2 ping -c 1 ping_url"
        # which made ping with only one package (-c 1) against tasmota
        # plug url (ping_url) but it only waits 0.2 seconds to ping
        # finish (timeout 0.2)
        #
        try:
            subprocess.check_output(["timeout", "0.2", "ping", "-c", "1", ping_url])
            return True
        except subprocess.CalledProcessError:
            return False
