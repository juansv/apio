# apio-2.0

El software Apio aumenta la vida útil de la batería de tu portátil. Apio trabaja manteniendo su batería en un ciclo de trabajo seguro que incluye ciclos de calibración de vez en cuando.

Si tienes un enchufe wifi con el software Tasmota, Apio es capaz de activar y desactivar el enchufe, por lo que no tienes que preocuparte de conectar o desconectar el adaptador de CA de tu portátil.

Pero si no tienes uno de esos enchufes compatibles con Tasmota, tampoco tienes que preocuparte, porque Apio te indica cuándo tienes que conectar o desconectar el adaptador de CA.

El fundamento técnico de apio lo tienes [en este archivo](./doc/apio-2.0/technical_things.md)

### Instalación de "apio"

#### Dependencias

Instala el interprete de python3

`sudo apt-get install python3`

Instala los módulos de terceros necesarios para `apio`

`sudo apt-get install python3-pip`

`python3 -m pip install python-crontab`

`python3 -m pip install notify2`

`python3 -m pip install requests`

`python3 -m pip install xdg`

`python3 -m pip install yaml`

Estos módulos también pueden estar disponibles en el gestor de paquetes de tu distribución.

Presta especial atención al módulo [`python-crontab`](https://pypi.org/project/python-crontab/). Si `apio` te devuelve el error `unexpected keyword argument 'user'` es que has instalado el módulo `crontab` en lugar de `python-crontab`.

#### El software "apio"

Clona el repositorio...

`sudo apt-get install git` (sólo si no tienes git instalado)

`git clone https://gitlab.com/juansv/apio.git`

...y ejecuta el script de instalación

`cd apio/`

`sudo sh apio-2.0_install.sh`

El script de instalación está hecho para distribuciones Debian GNU/linux. Si la jerarquía de tu sistema de archivos es diferente, puedes editar el script `apio-2.0_install.sh` en las líneas 17 a 23 (es la parte del scrit encabezada con el comentario `DIRECTORIOS DE DESTINO`).

### Uso de "apio"

Debes dar los siguientes pasos después de instalar `apio`:

1. Ejecuta `apio autoconfig`
2. Si el paso anterior devuelve algún mensaje de **error**, corrígelo.
3. Vuelve a repetir los pasos anteriores hasta que no devuelva ningún error.
4. Si dispones de un dispositivo Tasmota en red, indícaselo a `apio` con el comando `apio config -ip <dirección_ip_dispositivo>` o con el comando `apio config -url <url_dispositivo>`. Esto dependerá del dispositivo Tasmota y de cómo lo tengas dado de alta en tu red local.
5. Ejecuta `apio config -v` para verificar qué puntos de la configuración están pendientes.
6. Opcionalmente, ajusta la configuación de los ciclos de carga y calibración a tu gusto. Consulta el man o ejecuta `apio config -h`
7. Por último, no te olvides de ejecutar `apio config -on` para que `apio` se instale en el `cron` de tu usuario.

Cada usuario dado de alta en el equipo debe repetir los pasos anteriores si quiere que `apio` funcione cuando inicie sesión. El proceso será mucho más rápido, ya que gran parte de la configuración de `apio` es común a todos los usuarios.

### Tecnología

El programa `apio` está escrito en python. Necesitas tener el interprete de python3 instalado. Además, utiliza los siguientes módulos de terceros (no estándar), que deberás tener instalados: `python-crontab`, `notify2`, `requests`, `xdg` y `yaml`. La instalación de estos componentes se explica en el apartado [dependencias](#dependencias). Estos módulos de terceros condicionan que la versión de Python para correr `apio` deba ser, como mínimo, la 3.7

`apio` programa una tarea en el cron del usuario, por lo que no basta con instalarlo, también hay que ejecutar `apio config -on` para que se inicie el job del cron. Y cada usuario dado de alta en el equipo debe hacer lo mismo si quiere que `apio` funcione en su sesión.

`apio` necesita una configuración básica para poder funcionar, pero `apio` es capaz de autoconfigurarse. Esto se explica en el apartado [uso de apio](#uso-de-apio). El archivo de configuración se guarda en `/var/lib/apio/apio.yaml`.

`apio` guarda una información básica de cada ciclo de trabajo de la batería. Este registro de datos permite valorar, a lo largo del tiempo, cómo está siendo el envejecimiento de la batería. Esta información se guarda en el archivo `/var/lib/apio/apio.csv`. Este archivo se puede exportar con el comando `apio config -e /ruta_destino`.

`apio` puede operar de manera automática sobre enchufes wifi que usen el software `Tasmota`. De esta forma, basta conectar el cargador del portátil en el enchufe wifi para que `apio` se encargue de todo.

El control de `apio` sobre el enchufe wifi se realiza por la interface web que integra `Tasmota`. No es necesario montar una red `MQTT`.

Primero, configura tu enchufe con software `Tasmota` en tu red doméstica. Para ello, sigue las instrucciones del fabricante.

Luego, indícale a `apio` la dirección del dispositivo `Tasmota`. Para ello ejecuta:  
`apio config -ip <dirección_ip_dispositivo>`  
`apio config -url <url_dispositivo>`

No es necesario lanzar los dos comandos, basta con indicar la dirección del dispositivo `Tasmota` de una de las dos formas. Eso dependerá de cómo hayas preferido dar de alta el enchufe en tu red local.

#### Sobre Tasmota

`Tasmota` es un software que se puede instalar en [muchos dispositivos](https://templates.blakadder.com/index.html) de domótica. Incluso algunos fabricantes lo [incluyen pre-instalado de serie](https://templates.blakadder.com/preflashed.html) en su catálogo de productos a la venta.

`Tasmota` no es 100% libre debido al firmware necesario para gestionar la tarjeta wifi que incluyen estos aparatos. No obstante, el resto del programa tiene licencia libre. Para más información sobre `Tasmota` consulta su página web:

<https://tasmota.github.io/docs/>

#### Sobre enchufes wifi que no usen Tasmota

`apio` se comunica con el enchufe wifi lanzando las órdenes vía http por una API de REST. Esta forma de comunicarse con un aparato domótico no es exclusiva, ni mucho menos, de los dispositivos que corren el software `Tasmota`. Por lo tanto, es muy probable que `apio` funcione con multitud de otros enchufes. El autor sólo da fe del funcionamiento en aparatos con `Tasmota`.

### Versiones

#### Legacy (1.0 y 1.1)

La versión 1.0 de `apio` nace en 2018 como un proyecto de software y hardware libre. La parte de hardware consiste en un interruptor electrónico controlable desde un puerto USB. De esta forma, el software apio da la orden de encender y apagar el cargador de la batería a través de un puerto USB disponible del equipo. En esta versión 1.0 `apio` sólo era controlable desde una pequeña interface gráfica.

La versión 1.1 de `apio` aparece poco después, también en 2018. Incorpora la aplicación de terminal `apio-conf` que permite manejar y configurar `apio` desde la consola de comandos.

En julio de 2019 se modifica la documentación de `apio` añadiendo un circuito alternativo para la parte de hardware libre. Este circuito sustituye el relé por un triac, lo que permite reducir el tamaño y hace que `apio` reaccione más rápido. Como quiera que el software no sufre ningún cambio, la versión sigue siendo 1.1

#### 2.0.0

El proyecto `apio` siempre tuvo el inconveniente de la parte hardware. No existe un dispositivo comercial compatible con el software, lo que obliga al usuario a fabricarse su propio interruptor USB.

La versión 2.0 de `apio` nace con la popularización de los enchufes wifi. Estos dispositivos sí que son comerciales, lo que hace todo más fácil. Con objeto de mantener el espíritu de ser un proyecto de conocimiento libre, la versión 2.0 de `apio` se desarrolló para ser compatible con enchufes wifi que usen el software `Tasmota`.

Las principales características de la versión 2.0 son:

- Se reescribe totalmente el código del programa.
- Sólo se puede manejar con línea de comandos. La versión 2.0 nace sin un GUI.
- Para facilitar el manejo, la versión 2.0 incluye una herramienta de diagnóstico y auto configuración.
- El archivo de configuración se normaliza al formato yaml.
- El archivo de datos sigue siendo un csv, pero cambian los campos de la base de datos.
- El job de `apio` en el cron no ejecuta siempre con la misma frecuencia. La frecuencia se adapta a la carga de la batería para reducir el riesgo de una desconexión automática durante los ciclos de calibración.
- Los mensajes al entorno gráfico se envían por el área de notificaciones.
- Desaparece la opción de manejo de un interruptor por un puerto USB.

### Autor y licencia (Author and license)

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>.

