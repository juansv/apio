# Spanish translations for APIO-2.0 package
# Traducciones al español para el paquete APIO-2.0.
# Copyright (C) 2022 JUAN ANTONIO SILVA VILLAR
# This file is distributed under the same license as the APIO-2.0 package.
# Juan Antonio Silva Villar <juan.silva@disroot.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: APIO-2.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-04 19:59+0100\n"
"PO-Revision-Date: 2022-12-18 19:08+0100\n"
"Last-Translator: Juan Antonio Silva Villar <juan.silva@disroot.org>\n"
"Language-Team: Spanish <es@tp.org.es>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../apio-2.0/apio-2.0.py:86
msgid "Enlarge your laptop battery lifetime"
msgstr "Alarga la vida de tu batería"

#: ../apio-2.0/apio-2.0.py:90
msgid "Apio sub-commands"
msgstr "subcomandos de Apio"

#: ../apio-2.0/apio-2.0.py:91
msgid "Apio configuration"
msgstr "Configuración de Apio"

#: ../apio-2.0/apio-2.0.py:92
msgid "cron driver"
msgstr "Ejecuta Apio"

#: ../apio-2.0/apio-2.0.py:93
msgid "Apio self-configure tool"
msgstr "Auto configuración de Apio"

#: ../apio-2.0/apio-2.0.py:100
msgid "Higher charge limit"
msgstr "Valor máximo de carga"

#: ../apio-2.0/apio-2.0.py:104
msgid "Lower charge limit"
msgstr "Valor mínimo de carga"

#: ../apio-2.0/apio-2.0.py:108
msgid "Empty battery value"
msgstr "Valor de descarga profunda"

#: ../apio-2.0/apio-2.0.py:112
msgid "Duty cycles between calibrations"
msgstr "Número de ciclos para calibración"

#: ../apio-2.0/apio-2.0.py:116
msgid "Exports apio.csv to the given path"
msgstr "Exporta el archivo apio.csv a la ruta indicada"

#: ../apio-2.0/apio-2.0.py:120
msgid "Kernel filesystem battery path /sys/class/power_supply/BATX"
msgstr "Ruta /sys/class/power_supply/BATX de la batería"

#: ../apio-2.0/apio-2.0.py:124
msgid "Tasmota device LAN IP address"
msgstr "Dirección ip del dispositivo Tasmota en la red local"

#: ../apio-2.0/apio-2.0.py:128
msgid "url of Tasmota device"
msgstr "url del dispositivo Tasmota en la red local"

#: ../apio-2.0/apio-2.0.py:132
msgid "Shows Apio status"
msgstr "Resumen del estado de apio"

#: ../apio-2.0/apio-2.0.py:136
msgid "Toggle on/off the Tasmota device"
msgstr "Conmuta el estado del dispositivo Tasmota"

#: ../apio-2.0/apio-2.0.py:143
msgid "Turn on the Apio cron job"
msgstr "Activa apio en el cron"

#: ../apio-2.0/apio-2.0.py:147
msgid "Turn off the Apio cron job"
msgstr "Desactiva apio en el cron"

#: ../apio-2.0/apio-2.0.py:158
msgid "Runs Apio as cron does"
msgstr "Apio como lo ejecuta el cron"

#: ../apio-2.0/apio-2.0.py:169
msgid "Apio self configuration"
msgstr "Apio se auto configura"

#: ../apio-2.0/apio-2.0.py:403
msgid "Apio notifications"
msgstr "Notificaciones de Apio"

#: ../apio-2.0/apio-2.0.py:416
msgid "Apio can not start because is not well configured"
msgstr "Apio no está correctamente configurado y no se puede iniciar"

#: ../apio-2.0/apio-2.0.py:417
msgid "Please, run apio autoconfig once"
msgstr "Por favor, ejecute apio autoconfig al menos una vez"

#: ../apio-2.0/apio-2.0.py:418
msgid "If you have done it, try apio config -v and follow the instructions"
msgstr "Si ya lo ha hecho, ejecute apio config -v y siga las instrucciones"

#: ../apio-2.0/apio-2.0.py:441
msgid "Group and user checking..."
msgstr "Grupo y usuario, comprobando..."

#: ../apio-2.0/apio-2.0.py:451
msgid "Apio group exists and user is added"
msgstr "El grupo apio existe y el usuario es miembro"

#: ../apio-2.0/apio-2.0.py:453
msgid "ERROR: apio group exists but user is not added!"
msgstr "ERROR: El grupo apio existe pero el usuario no es miembro!"

#: ../apio-2.0/apio-2.0.py:455
msgid "ERROR: apio group does not exist!"
msgstr "ERROR: El grupo apio no existe!"

#: ../apio-2.0/apio-2.0.py:469 ../apio-2.0/apio-2.0.py:490
#: ../apio-2.0/apio-2.0.py:508
msgid " checking..."
msgstr " comprobando..."

#: ../apio-2.0/apio-2.0.py:471
msgid "ERROR: directory does not exist!"
msgstr "ERROR: El directorio no existe!"

#: ../apio-2.0/apio-2.0.py:473
msgid "Directory exists"
msgstr "El directorio existe"

#: ../apio-2.0/apio-2.0.py:475
msgid "ERROR: directory does not belong to apio group!"
msgstr "ERROR: El directorio no pertenece al grupo apio!"

#: ../apio-2.0/apio-2.0.py:477
msgid "Directory belongs to apio group"
msgstr "El directorio pertenece al grupo apio"

#: ../apio-2.0/apio-2.0.py:479
msgid "ERROR: Directory does not have write permissions on group!"
msgstr "ERROR: El directorio no tiene permiso de escritura en el grupo!"

#: ../apio-2.0/apio-2.0.py:481
msgid "Directory has write permissions on group"
msgstr "El directorio tiene permisos de escritura en el grupo"

#: ../apio-2.0/apio-2.0.py:492 ../apio-2.0/apio-2.0.py:510
msgid "ERROR: File does not exist!"
msgstr "ERROR: El archivo no existe!"

#: ../apio-2.0/apio-2.0.py:494 ../apio-2.0/apio-2.0.py:512
msgid "File exists"
msgstr "El archivo existe"

#: ../apio-2.0/apio-2.0.py:496 ../apio-2.0/apio-2.0.py:514
msgid "ERROR: File does not belong to apio group!"
msgstr "ERROR: El archivo no pertenece al grupo apio!"

#: ../apio-2.0/apio-2.0.py:498 ../apio-2.0/apio-2.0.py:516
msgid "File belongs to apio group"
msgstr "El archivo pertenece al grupo apio"

#: ../apio-2.0/apio-2.0.py:500 ../apio-2.0/apio-2.0.py:518
msgid "ERROR: File does not have write permissions on group!"
msgstr "El archivo no tiene permiso de escritura en el grupo!"

#: ../apio-2.0/apio-2.0.py:502 ../apio-2.0/apio-2.0.py:520
msgid "File has write permissions on group"
msgstr "El archivo tiene permisos de escritura en el grupo"

#: ../apio-2.0/apio-2.0.py:526
msgid "Basic configuration, checking..."
msgstr "Configuración básica, comprobando..."

#: ../apio-2.0/apio-2.0.py:530
msgid "/var/lib/apio/apio.yaml basic configuration done"
msgstr "Completada configuración básica de /var/lib/apio/apio.yaml"

#: ../apio-2.0/apio-2.0.py:532 ../apio-2.0/apio-2.0.py:551
#: ../apio-2.0/apio-2.0.py:581 ../apio-2.0/apio-2.0.py:612
msgid "/var/lib/apio/apio.yaml not accessible"
msgstr "Imposible acceder a /var/lib/apio/apio.yaml"

#: ../apio-2.0/apio-2.0.py:538
msgid "Battery information, checking..."
msgstr "Datos de la batería, comprobando..."

#: ../apio-2.0/apio-2.0.py:544
msgid "Battery path: "
msgstr "Ruta de la batería: "

#: ../apio-2.0/apio-2.0.py:545 ../apio-2.0/apio-2.0.py:579
msgid "It is recorded in config file"
msgstr "Se registra en el archivo de configuración"

#: ../apio-2.0/apio-2.0.py:548
msgid "directory /sys/class/power_supply/BAT# not found"
msgstr ""
"no ha sido posible determinar el directorio /sys/class/power_supply/BAT#"

#: ../apio-2.0/apio-2.0.py:549
msgid "please, edit config file and fill battery_path field"
msgstr "por favor, edite el archivo de configuración y complete battery_path"

#: ../apio-2.0/apio-2.0.py:557
msgid "Notification bus variable, checking..."
msgstr "Variable del bus de mensajes, comprobando..."

#: ../apio-2.0/apio-2.0.py:575
msgid "not able to find DBUS_SESSION_BUS_ADDRESS variable"
msgstr "no ha sido posible determinar la variable DBUS_SESSION_BUS_ADDRESS"

#: ../apio-2.0/apio-2.0.py:576
msgid "Apio would not show notifications on desktop"
msgstr "Apio no podrá lanzar notificaciones al escritorio"

#: ../apio-2.0/apio-2.0.py:578
msgid "User Notification bus variable: "
msgstr "Variable del bus de mensajes para el usuario: "

#: ../apio-2.0/apio-2.0.py:587
msgid "Apio cron job, checking..."
msgstr "Estado de Apio en el cron, comprobando..."

#: ../apio-2.0/apio-2.0.py:609
msgid "Apio job scheduled but disabled"
msgstr "Apio está cargado en el cron, pero está deshabilitado"

#: ../apio-2.0/apio-2.0.py:610
msgid "Run apio config -on when you want Apio get started"
msgstr "Ejecute apio config -on cuando quiera iniciar Apio"

#: ../apio-2.0/apio-2.0.py:618
msgid "Data base, checking..."
msgstr "Archivo de datos, comprobando..."

#: ../apio-2.0/apio-2.0.py:625
msgid "/var/lib/apio/apio.csv file ready"
msgstr "El archivo /var/lib/apio/apio.csv está operativo"

#: ../apio-2.0/apio-2.0.py:627
msgid "/var/lib/apio/apio.csv not accessible"
msgstr "Imposible acceder a /var/lib/apio/apio.csv"

#: ../apio-2.0/apio-2.0.py:633
msgid "Self configuration process finished"
msgstr "Proceso de auto configuración terminado"

#: ../apio-2.0/apio-2.0.py:634
msgid "If any error has been detected, please, fix it"
msgstr "Si el proceso ha detectado algún error, por favor, corríjalo"

#: ../apio-2.0/apio-2.0.py:635
msgid "and, then, run apio autoconfig again"
msgstr "y ejecute apio autoconfig otra vez"

#: ../apio-2.0/apio-2.0.py:636
msgid "Finally, get status info by running apio config -v"
msgstr "Luego, ejecute apio config -v para localizar otros fallos"

#: ../apio-2.0/apio-2.0.py:637
msgid "Once all errors disapear:"
msgstr "Si no hubiese más errores:"

#: ../apio-2.0/apio-2.0.py:638
msgid "- Set up Apio as you like it. Read apio config -h"
msgstr "- Configure Apio a su gusto. Consulte apio config -h"

#: ../apio-2.0/apio-2.0.py:639
msgid "- Start Apio by running apio config -on"
msgstr "- Inicie Apio ejectuando apio config -on"

#: ../apio-2.0/apio-2.0.py:660
msgid "Apio warning"
msgstr "Apio informa de un error"

#: ../apio-2.0/apio-2.0.py:661
msgid "Something happens to your laptop battery!"
msgstr "¡La batería presenta un problema!"

#: ../apio-2.0/apio-2.0.py:685 ../apio-2.0/apio-2.0.py:698
#: ../apio-2.0/apio-2.0.py:717 ../apio-2.0/apio-2.0.py:730
#: ../apio-2.0/apio-2.0.py:749 ../apio-2.0/apio-2.0.py:762
#: ../apio-2.0/apio-2.0.py:781 ../apio-2.0/apio-2.0.py:794
msgid "Apio says"
msgstr "Apio informa"

#: ../apio-2.0/apio-2.0.py:686
msgid "Battery charged, please unplug AC adapter"
msgstr "La batería está cargada, por favor desenchufe el cargador"

#: ../apio-2.0/apio-2.0.py:699
msgid "Now is time for charging, please plug AC adapter"
msgstr "Batería en ciclo de carga, por favor enchufe el cargador"

#: ../apio-2.0/apio-2.0.py:718
msgid "Battery low, please plug AC adapter"
msgstr "La batería está descargada, por favor enchufe el cargador"

#: ../apio-2.0/apio-2.0.py:731
msgid "Now is time for discharging, please unplug AC adapter"
msgstr "Batería en ciclo de descarga, por favor desenchufe el cargador"

#: ../apio-2.0/apio-2.0.py:750
msgid "Battery empty, please plug AC adapter"
msgstr "La batería está descargada, por favor enchufe el cargador"

#: ../apio-2.0/apio-2.0.py:763
msgid "Now is time for one calibration cycle, please unplug AC adapter"
msgstr "Batería en ciclo de calibración, por favor desenchufe el cargador"

#: ../apio-2.0/apio-2.0.py:782
msgid "Battery full, please unplug AC adapter"
msgstr "La batería está cargada, por favor desenchufe el cargador"

#: ../apio-2.0/apio-2.0.py:795
msgid "Now is time for one calibration cycle, please plug AC adapter"
msgstr "Batería en ciclo de calibración, por favor enchufe el cargador"

#: ../apio-2.0/apio-2.0.py:893 ../apio-2.0/apio-2.0.py:898
#: ../apio-2.0/apio-2.0.py:905
msgid "You have typed a wrong charge value"
msgstr "El valor de carga indicado es incorrecto"

#: ../apio-2.0/apio-2.0.py:903
msgid "For calibration process, charge should be between 10% and 20%"
msgstr "La carga de calibración debe estar entre 10% y 20%"

#: ../apio-2.0/apio-2.0.py:916
msgid "The path "
msgstr "La ruta "

#: ../apio-2.0/apio-2.0.py:916
msgid " does not exist"
msgstr " no existe"

#: ../apio-2.0/apio-2.0.py:972
msgid "Apio necessary files:"
msgstr "Archivos necesarios para el funcionamiento de Apio:"

#: ../apio-2.0/apio-2.0.py:974
msgid "ERROR: Apio files not accessibles"
msgstr "ERROR: No se puede acceder a los archivos de Apio"

#: ../apio-2.0/apio-2.0.py:975 ../apio-2.0/apio-2.0.py:992
#: ../apio-2.0/apio-2.0.py:1010 ../apio-2.0/apio-2.0.py:1023
#: ../apio-2.0/apio-2.0.py:1048 ../apio-2.0/apio-2.0.py:1083
msgid "Please, run apio autoconfig and follow the instructions"
msgstr "Por favor, ejecute apio autoconfig y siga las instrucciones"

#: ../apio-2.0/apio-2.0.py:977
msgid "- Apio files are accessibles"
msgstr "- Los archivos están accesibles"

#: ../apio-2.0/apio-2.0.py:982
msgid "Battery configuration:"
msgstr "Configuración de la batería:"

#: ../apio-2.0/apio-2.0.py:988
msgid "- Battery set up in config file"
msgstr "- La batería está correctamente configurada en Apio"

#: ../apio-2.0/apio-2.0.py:989
msgid "- Current charge"
msgstr "- Nivel de carga actual"

#: ../apio-2.0/apio-2.0.py:991 ../apio-2.0/apio-2.0.py:1009
#: ../apio-2.0/apio-2.0.py:1047 ../apio-2.0/apio-2.0.py:1082
msgid "ERROR: Can not read Apio configuration"
msgstr "ERROR: No se puede leer la configuración de Apio"

#: ../apio-2.0/apio-2.0.py:997
msgid "Desktop notifications config:"
msgstr "Configuración de las notificaciones al escritorio:"

#: ../apio-2.0/apio-2.0.py:1002
msgid "- Apio is able to show desktop notifications"
msgstr "- Apio puede enviar notificaciones al escritorio"

#: ../apio-2.0/apio-2.0.py:1003
msgid "- A trial notification has been sent"
msgstr "- Se acaba de enviar una como verificación"

#: ../apio-2.0/apio-2.0.py:1004
msgid "Apio trial"
msgstr "Apio realiza un test"

#: ../apio-2.0/apio-2.0.py:1005
msgid "This is a trial message sent by apio config -v"
msgstr "Este es un mensaje de prueba lanzado con apio config -v"

#: ../apio-2.0/apio-2.0.py:1015
msgid "Cron job of Apio:"
msgstr "Estado de Apio en el cron:"

#: ../apio-2.0/apio-2.0.py:1018
msgid "- Apio job is scheduled and ready"
msgstr "- Apio está programado y listo en el cron"

#: ../apio-2.0/apio-2.0.py:1020
msgid "- Apio job is not scheduled"
msgstr "- Apio no está programado en el cron"

#: ../apio-2.0/apio-2.0.py:1022
msgid "ERROR: Can not read in crontab"
msgstr "ERROR: No se puede leer en el cron"

#: ../apio-2.0/apio-2.0.py:1031
msgid "Tasmota device configuration:"
msgstr "Configuración del dispositivo Tasmota:"

#: ../apio-2.0/apio-2.0.py:1034
msgid "- There is a Tasmota device set up in: "
msgstr "- Está configurado un dispositivo Tasmota en: "

#: ../apio-2.0/apio-2.0.py:1036
msgid "- Tasmota device reacts properly to Apio"
msgstr "- El dispositivo Tasmota responde correctamente"

#: ../apio-2.0/apio-2.0.py:1038
msgid "- Tasmota device does not react, is it switched on?"
msgstr "- El dispositivo Tasmota no responde ¿está encendido?"

#: ../apio-2.0/apio-2.0.py:1040
msgid "- There is not any Tasmota device set up"
msgstr "- No está configurado ningún dispositivo Tasmota"

#: ../apio-2.0/apio-2.0.py:1041
msgid "- Apio could not work in automatic mode"
msgstr "- Apio no puede funcionar en modo automático"

#: ../apio-2.0/apio-2.0.py:1042
msgid "To sep up a Tasmota device, run:"
msgstr "Para configurar un dispositivo Tasmota, ejecute:"

#: ../apio-2.0/apio-2.0.py:1043
msgid "apio config -ip <ip address>"
msgstr "apio config -ip <dirección ip>"

#: ../apio-2.0/apio-2.0.py:1044
msgid "apio config -url <url>"
msgstr "apio config -url <dirección url>"

#: ../apio-2.0/apio-2.0.py:1045
msgid "depending on your LAN or your Tasmota device"
msgstr "según la configuración de su red o del dispositivo Tasmota"

#: ../apio-2.0/apio-2.0.py:1054
msgid "Operation mode:"
msgstr "Modo de funcionamiento:"

#: ../apio-2.0/apio-2.0.py:1057
msgid "- Apio in automatic mode"
msgstr "- Apio en modo automático"

#: ../apio-2.0/apio-2.0.py:1059
msgid "- Apio in manual mode"
msgstr "- Apio en modo manual"

#: ../apio-2.0/apio-2.0.py:1062
msgid "- AC adapter is plugged"
msgstr "- El cargador está enchufado"

#: ../apio-2.0/apio-2.0.py:1064
msgid "- AC adapter is not plugged"
msgstr "- El cargador no está enchufado"

#: ../apio-2.0/apio-2.0.py:1067
msgid "Current cycle:"
msgstr "Ciclo actual:"

#: ../apio-2.0/apio-2.0.py:1069
msgid "- Calibration cycle"
msgstr "- En ciclo de calibración"

#: ../apio-2.0/apio-2.0.py:1071
msgid "- Charging cycle"
msgstr "- En ciclo de carga"

#: ../apio-2.0/apio-2.0.py:1073
msgid "- Discharging cycle"
msgstr "- En ciclo de descarga"

#: ../apio-2.0/apio-2.0.py:1076
msgid "Current set up:"
msgstr "Configuración actual:"

#: ../apio-2.0/apio-2.0.py:1077
msgid "- Higher charge limit"
msgstr "- Valor máximo de carga"

#: ../apio-2.0/apio-2.0.py:1078
msgid "- Lower charge limit"
msgstr "- Valor mínimo de carga"

#: ../apio-2.0/apio-2.0.py:1079
msgid "- Empty battery value"
msgstr "- Valor de carga para calibración"

#: ../apio-2.0/apio-2.0.py:1080
msgid "- Duty cycles between calibrations"
msgstr "- Número de ciclos entre calibraciones"

#: ../apio-2.0/apio-2.0.py:1080
msgid "cycles"
msgstr "ciclos"
