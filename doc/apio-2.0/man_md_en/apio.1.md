% APIO(1) apio 2.0.0
% Juan Antonio Silva Villar
% Diciembre 2022

# NAME
apio - increases your laptop battery lifetime

# SYNOPSIS
**apio** {autoconfig | config [-sh *number*][-sl *number*][-se *number*][-sc *number*][-e *path*][-bat *path*][-ip *ip*][-url *url*][-v][-t][-on | -off] | cron}

# DESCRIPTION
**apio** works by keeping your battery in a safe duty cycle which include calibration cycles from time to time. It increases your laptop battery lifetime.

**apio** schedules a job in user cron. Each time this job runs, it assesses if it is necessary plug in or out the AC adapter. If so, sends a desktop notification.

**apio** does not use superuser's cron. It works over the cron of every user which want to use **apio**.

If you have a wifi plug with **Tasmota** software installed, it is possible to set up **apio** for switching on an off the plug, so you don't have to worry about connecting or disconnecting your laptop AC adapter.

**apio** needs a configfile to work. It saves technical info about the laptop and the users. In order to let all users access the configfile, a new permissions group called *apio* is needed. Of course, all apio user should be in it. The configfile should also have writing permissions on *apio* group.

**apio** also stores technical data about battery duty cycles. It lets the user value how the battery ageing is. All data are saved in *csv* file which needs writing permissions over *apio* group.

**autoconfig**
: It configures **apio** by itself. It should be run at least one time for each user who want to use **apio**. It has no options, when you run **apio autoconfig**, it simply fills the configfile at */var/lib/apio/apio.yaml*. Any error is warned on the standard output to be fix by the user.

**cron**
: This sub command is run by the **apio** cron job. Run **apio cron** by any user is a nonsense.

**config**
: It is the main tool for **apio** options tuning. If you have a **Tasmota** driven wifi plug, then you should run **apio config -ip** or **apio config -url** to tell **apio** where is the wifi plug into your LAN. Chapter options is fully for **apio config** command options.

# OPTIONS
They all but **-h** are options of sub command **config**

**-h**, **--help**
: Shows a simple help message

**-sh**, **--sethigh NUMBER**
: Sets up **apio** to consider duty cycle done when battery charge raches **NUMBER** as top value. **NUMBER** is recorded into configfile */var/lib/apio/apio.yaml*. The default is 80% which is set by running **apio autoconfig**.

**-sl**, **--setlow NUMBER**
: Sets up **apio** to consider duty cycle done when battery charge raches **NUMBER** as floor value. **NUMBER** is recorded into configfile */var/lib/apio/apio.yaml*. The default is 40% which is set by running **apio autoconfig**.

**-se**, **--setempty NUMBER**
: Battery measurements have to be calibrated from time to time. It is got by letting the battery be completely discharged. Option **-se** tells **apio** that **NUMBER** is the lowest charging value. **NUMBER** is recorded into configfile */var/lib/apio/apio.yaml*. The default is 10% which is set by running **apio autoconfig**.

**-sc**, **--setcalibration NUMBER**
: Sets up **apio** so that there are **NUMBER** duty cycles between calibration cycles. **NUMBER** is recorded into configfile */var/lib/apio/apio.yaml*. The default is 50 cycles which is set by running **apio autoconfig**.

**-e**, **--export PATH**
: Exports data base file */var/lib/apio/apio.csv* to the given **PATH**.

**-bat**, **--setbattery PATH**
: Records in configfile */var/lib/apio/apio.yaml* where the battery is mounted on the virtual file system for kernel info. It is normally */sys/class/power_supply/BAT#*, where BAT# is BAT0, BAT1 or similar. This is automatically filled by running **apio autoconfig**. This option would be needed if some error happened when **apio autoconfig** or if you want to force any other path.

**-ip**, **--setip**
: Sets up the ip of the **Tasmota** driven wifi plug. The ip address will depend on your LAN. This ip can not be figured out by **apio autoconfig** and should set it up by running **apio config -ip** at least once. It is not necessary to run both **apio config ip** and **apio config -url**, but one should be run.

**-url**, **--seturl**
: Sets up the url of the **Tasmota** driven wifi plug. The url depends on each manufacturer, please read the device instructions. This url can not be figured out by **apio autoconfig** and should set it up by running **apio config -url** at least once. It is not necessary to run both **apio config ip** and **apio config -url**, but one should be run.

**-v**, **--verbose**
: Returns a summary of **apio** current configuration and working process on the standard output. If there is any error, it will be also shown.

**-t**, **--togle**
: Toggles the **Tasmota** driven wifi plug state. It is useful to check if it is well configured in **apio**.

**-on**, **--start**
: Once **apio** is configured, run **apio config -on** to start it in cron.

**-off**, **--stop**
: Disables the **apio** job in cron. The job is not removed from crontab, it simply gets comment which disables the job.

# EXAMPLES
**apio autoconfig**
: The self configuration process starts and everything is writen in */var/lib/apio/apio.yaml*. It shows on the stardard output how the process was, errors included. If you run **apio autoconfig** again it does not change right data but could correct the wrong ones.

**apio config -ip 192.168.1.15**
: Records in */var/lib/apio/apio.yaml* the ip address 192.168.1.15 as the LAN address of the wifi plug. In case of being any error (the ip is not right, the wifi plug is turned off...) **apio** still works, but couldn't do it in automatic mode.

**apio config -v**
: Info about **apio** configuration is shown on the standard output. It also shows current state features and any error detected.

**apio config -e /home/user/data.csv**
: Data base file */var/lib/apio/apio.csv* content is exported to **/home/user/data.csv**. If it does not exist, it will be created.

# FILES
*/var/lib/apio/apio.yaml*
: Configuration file of **apio**. It is a **YAML** file and it can be handy edited, but it is strongly recommended to edit by using **apio autoconfig** and **apio config**. The parent directory */var/lib/apio* should belong to *apio* group and configfile should have writing permissions over *apio* group.

*/var/lib/apio/apio.csv*
: Data base file of **apio**. It records info about each battery duty cycle. It is a **csv** file and it can be handy edited, but it is strongly recommended to export by using **apio config -e path** and edit the exported file. The parent directory */var/lib/apio* should belong to *apio* group and data base file should have writing permissions over *apio* group.

# BUGS
I don't hope so...

If **apio** gives the error **unexpected keyword argument 'user'** it means you have installed **crontab** module instead **python-crontab**.

**apio** cron job is tagged as **apiocron**, so you can check errors by running **grep apiocron /var/log/syslog**

# COPYRIGHT
Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org). License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>. This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

