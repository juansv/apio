[Castellano](#el-fundamento-tecnico-de-apio)
[English](#technical-basis-of-apio)

---

# El fundamento técnico de apio

## Porqué del nombre

Con mi primer portátil cometí el error de, cada vez que lo usaba en mi mesa, dejaba el cargador constantemente enchufado. Cuando supe que esto era una mala práctica ya era demasiado tarde: La batería estaba casi muerta.

Buscando soluciones, leí que las baterías de ion litio podían recuperar parte de su capacidad de carga si se congelaban por un tiempo. No tenía nada que perder, así que lo probé. Envolví bien la batería y la puse en el congelador, al lado de unos táper de puré de apio.

## Buenas prácticas con una batería ion litio

El experimento funcionó, la batería recuperó algo de autonomía. Esto me sorprendió bastante, porque creí que no era más que una leyenda urbana. Así que empecé a estudiar qué había de verdad en esto. Y así aprendí un montón de cosas sobre estas baterías.

Parte del deterioro de una batería viene dado por el calentamiento derivado de su uso. Si la batería se mantiene permanentemente enchufada, este calentamiento es mayor, lo que termina con una dilatación de las celdas. Al congelar la batería, se revierte la dilatación, lo que repercute en una pequeña recuperación de la misma. Pero pequeña. Si se ha hecho mal uso de la batería, la batería queda dañada para siempre.

La mejor opción es cuidar la batería. Y la mejor forma de cuidar la batería es usarla, es decir, dejar que sea la batería, y no el cargador, la que mantenga en marcha nuestro portátil. Esto nos obliga a enchufar y desenchufar el cargador y nunca dejarlo permanentemente enchufado.

Usar la batería no significa usarla en sus dos extremos, cargarla a tope y usarla hasta que se descargue totalmente. Esta es una costumbre que muchos heredamos cuando usábamos baterías con otras tecnologías (Ni-Cd, Ni-MH). Efectivamente, estas tecnologías presentan el llamado [efecto memoria](https://es.wikipedia.org/wiki/Efecto_memoria) por el cual, si la batería no se lleva regularmente hasta su descarga total, pierde parte de su capacidad.

Con las baterías de iones de litio esto no es necesario. Lo mejor es trabajar con la batería entre dos niveles de carga ni muy bajo ni muy alto. Típicamente entre el 40% y el 80%. No obstante, es bueno someterlas a un ciclo de descarga y carga completa de vez en cuando, una vez al mes, apróximadamente.

Llevar la cuenta de todo esto es laborioso, por eso hice `apio`.

---

# Technical basis of apio

## Origin of the name

I made a mistake with my first laptop. Each time I worked with it on the table, I kept the AC adapter constantly plug. When I realized that is a bad practice it was late. My battery was almost dead.

Then, I read that Li-ion could recover some life if they get freeze. I had nothing to lose, so I tried. I wrapped battery very well and put it in the fridge, side by side a tupper with celery cream. Apio is the Spanish word for celery.

## Good practices with Li-ion battery

The experiment above worked. The battery got recover a little bit. It was a surprise for me, I thought it was a urban legend. So I started to study the truth behind and I learnt a lot.

When a Li-ion battery gets ruin, one reason is linked with the increase of temperature due to their normal use. If you keep your battery constantly plugged to the AC adapter, the increase of temperature is even higher. It expands the material. When the battery get freeze, materials reduce their volume and it causes this little battery recovery. But little, very little. If you have damaged your battery, it is damaged.

It is always better taking care of battery. And the best way to take care of battery is simply using the battery. It means let the battery energies the laptop, not the AC adapter. This compels us to plug and unplug the AC adapter.

Using the battery does not mean from 100% of charge to 0% of charge. This is a habit that many of us got when we started to use other battery technologies (Ni-Cd, Ni-MH). Indeed, those technologies have the [memory effect](https://en.wikipedia.org/wiki/Memory_effect). This effect is avoided by fully charging and discharging the battery.

Li-ion batteries live longer when they work between not very high and not very low charge levels. Typically between 40% and 80%. However, it is a good practice make a fully charge and discharge from time to time. Once per month approximately.

Keep all those info in mind is hard, this is the reason of `apio`.

