% APIO(1) apio 2.0.0
% Juan Antonio Silva Villar
% Diciembre 2022

# NOMBRE
apio - aumenta la vida útil de la batería de tu portátil

# RESUMEN
**apio** {autoconfig | config [-sh *número*][-sl *número*][-se *número*][-sc *número*][-e *path*][-bat *path*][-ip *ip*][-url *url*][-v][-t][-on | -off] | cron}

# DESCRIPCIÓN
**apio** es un programa que mantiene la batería de tu portátil funcionando en un ciclo de trabajo seguro, con objeto de alargar su vida útil.

**apio** programa una tarea en el cron. Cada vez que esta tarea se ejecuta, valora si es necesario conectar o desconectar el cargador de la batería. Si fuese necesario, lanza una recomendación al área de notificaciones del escritorio.

**apio** no utiliza el cron del superusuario si no que se programa en el cron de todos los usuarios que deseen usar **apio**.

Si dispones de un enchufe wifi con software **Tasmota**, se puede configurar **apio** para que actúe sobre el enchufe. De esta forma, **apio** opera de modo totalmente automático y transparente al usuario.

**apio** necesita un archivo de configuración para funcionar. Almacena información técnica sobre el equipo y los usuarios. Para compartir el archivo de configuración es necesario crear un grupo *apio* y que todos los usuarios formen parte de él. El archivo de configuración debe tener permiso de escritura en el grupo *apio*.

**apio** también almacena datos técnicos de los ciclos de trabajo de la batería para que el usuario pueda valorar el envejecimiento de la misma. Los datos se almacenan en un archivo *csv*. Este archivo también debe tener permiso de escritura en el grupo *apio*.

**autoconfig**
: Herramienta de autoconfiguración de **apio**. Debe ser ejecutada al menos una vez por cada usuario del equipo que quiera usar **apio**. No tiene opciones asociadas, cuando se lanza **apio autoconfig** simplemente se auto completa el archivo de configuración */var/lib/apio/apio.yaml*. Si hubiese alguna dificultad, avisa por la salida estándar para que el usuario las corrija.

**cron**
: Es el subcomando que ejecuta la tarea que **apio** programa en el cron. Aunque el usuario puede ejecutar **apio cron** desde un terminal, esta operación no tiene sentido.

**config**
: Herramienta para el ajuste de las opciones de **apio**. Si se utiliza un enchufe wifi con software **Tasmota**, será imprencindible ejecutar **apio config -ip** o **apio config -url** para indicar a **apio** la dirección del enchufe en la red local. Las opciones de **apio config** se describen en el capítulo de opciones.

# OPCIONES
Excepto la opción **-h**, todas las opciones aplican solo al subcomando **config**

**-h**, **--help**
: Muestra un mensaje de ayuda básica.

**-sh**, **--sethigh NÚMERO**
: Ajusta **apio** para que el ciclo de trabajo termine cuando la carga de la batería alcance el valor máximo **NÚMERO**. Este dato se registra en el archivo de configuración */var/lib/apio/apio.yaml*. Por defecto, la ejecución de **apio autoconfig** completa este dato al 80%.

**-sl**, **--setlow NÚMERO**
: Ajusta **apio** para que el ciclo de trabajo termine cuando la carga de la batería alcance el valor mínimo **NÚMERO**. Este dato se registra en el archivo de configuración */var/lib/apio/apio.yaml*. Por defecto, la ejecución de **apio autoconfig** completa este dato al 40%.

**-se**, **--setempty NÚMERO**
: Regularmente **apio** calibra las mediciones de la batería. Para ello, la descaga totalmente. La opción **-se** indica a **apio** el valor **NÚMERO** de carga para considerar la batería totalmente descargada. Este dato se registra en el archivo de configuración */var/lib/apio/apio.yaml*. Por defecto, la ejecución de **apio autoconfig** completa este dato al 10%.

**-sc**, **--setcalibration NÚMERO**
: Indica a **apio** el **NÚMERO** de ciclos de trabajo normales entre calibraciones. Este dato se registra en el archivo de configuración Este dato se registra en el archivo de configuración */var/lib/apio/apio.yaml*. Por defecto, la ejecución de **apio autoconfig** completa este dato al 10%.. Por defecto, la ejecución de **apio autoconfig** completa este dato en 50 ciclos.

**-e**, **--export PATH**
: Exporta el archivo de datos */var/lib/apio/apio.csv* al **PATH** indicado.

**-bat**, **--setbattery PATH**
: Registra en el archivo de configuración */var/lib/apio/apio.yaml* el punto de montaje del sistema de archivos virtual que utiliza el kernel para recopilar la información de la batería. Típicamente es la ruta */sys/class/power_supply/BAT#*, donde BAT# es BAT0, BAT1, etc. La ejecución de **apio autoconfig** completa automáticamente este dato. Sólo sería necesario el uso de esta opción si se produjese algún fallo durante la ejecución de **apio autoconfig** o si se desea forzar otra ruta.

**-ip**, **--setip**
: Dirección ip del enchufe wifi con software **Tasmota** donde está conectado el cargador de la batería del portátil. Este dato dependerá de cómo haya sido dado de alta el enchufe wifi en la red local. La herramienta **apio autoconfig** no es capaz de determinar esta dirección y debe ser introducida con **apio config -ip** por alguno de los usuarios de **apio**.

**-url**, **--seturl**
: Dirección url del enchufe wifi con software **Tasmota** donde está conectado el cargador de la batería del portátil. Este dato dependerá del propio dispositivo, lea las instrucciones del fabricante. La herramienta **apio autoconfig** no es capaz de determinar esta dirección y debe ser introducida con **apio config -url** por alguno de los usuarios de **apio**.

**-v**, **--verbose**
: Devuelve un resumen de la configuración actual de **apio** y de su estado de funcionamiento. Si hubiese algún error, también informa de ello por la salida estándar.

**-t**, **--togle**
: Conmuta el estado del enchufe wifi con software **Tasmota**. Esta opción permite comprobar si la configuración del enchufe wifi en **apio** es correcta.

**-on**, **--start**
: Una vez terminada la configuración de **apio** y del enchufe wifi, **apio config -on** inicia el proceso en el cron del usuario.

**-off**, **--stop**
: Detiene el proceso programado en el cron del usuario. El registro en crontab no se borra, simplemente queda comentado para que no se ejecute.

# EJEMPLOS
**apio autoconfig**
: Inicia el proceso de autoconfiguración y registra todos los datos encontrados en el archivo */var/lib/apio/apio.yaml*. Informa por la salida estándar de los datos registrados y los errores producidos durante la autoconfiguración. Ejecuciones sucesivas de **apio autoconfig** no borran las configuraciones correctas anteriores, sólo sobre escribe las incorrectas.

**apio config -ip 192.168.1.15**
: Registra en el archivo */var/lib/apio/apio.yaml* la dirección ip 192.168.1.15 como el punto donde está el enchufe wifi en la red local. Si la dirección es incorrecta o el enchufe está "apagado", no provoca ningún error ni detiene la ejecución de **apio**; simplemente **apio** dejará de funcionar en modo automático.

**apio config -v**
: Devuelve por la salida estándar información sobre los parámetros de configuración de **apio**, el estado actual de **apio** en el equipo y los fallos detectados.

**apio config -e /home/user/data.csv**
: Exporta el contenido actual del archivo */var/lib/apio/apio.csv* al archivo **/home/user/data.csv**. Si el archivo de destino *data.csv* no existiese, lo crea.

# ARCHIVOS
*/var/lib/apio/apio.yaml*
: Archivo de configuración de **apio**. Es un archivo en formato **YAML** y puede editarse a mano, pero se recomienda usar **apio autoconfig** o alguna de las opciones de **apio config** para completarlo. El directorio */var/lib/apio/* debe pertenecer al grupo *apio* y el archivo debe tener permiso de escritura sobre el grupo *apio*.

*/var/lib/apio/apio.csv*
: Archivo para el registro de los ciclos de trabajo de la batería. Es un archivo en formato **csv** y puede editarse a mano, pero se recomienda usar **apio config -e path** para exportarlo y editar el archivo exportado. El directorio */var/lib/apio/* debe pertenecer al grupo *apio* y el archivo debe tener permiso de escritura sobre el grupo *apio*.

# ERRORES
Espero que no haya...

Si **apio** devuelve el error **unexpected keyword argument 'user'** significa que tienes instalado el módulo **crontab** en vez de **python-crontab**.

La tarea **apio** está etiquetada como **apiocron** en el cron, por lo que puedes filtrar los errores ejecutando **grep apiocron /var/log/syslog**

# COPYRIGHT
Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org). Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>. Esto es software libre: puedes cambiarlo y distribuirlo libremente. No hay garantía, en la medida que lo permita la ley.

